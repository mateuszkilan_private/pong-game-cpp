#pragma once

#include <memory>
#include "Player.h"
#include "AIPlayer.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class AIPlayerAccessor {
public:

    template<typename T> T getMemberVar(std::shared_ptr<Player>& player, std::string variableName) {
        Assert::Fail(L"Not specialized version of AIPlayerAccessor::getMemberVar was called. Variable doesn't exist");
        return T{};
    }

    template<> int getMemberVar<int>(std::shared_ptr<Player>& player, std::string variableName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (variableName == "mRacketsRelativePointBouncePlaceDuringFirstRecognition") {
            return aiPlayer->mRacketsRelativePointBouncePlaceDuringFirstRecognition;
        }
        if (variableName == "mPixelsLeftInLastContactBounceAreaBottom") {
            return static_cast<int>(aiPlayer->mPixelsLeftInLastContactBounceAreaBottom);
        }
        if (variableName == "mAmountOfDistanceWhichRacketNeedsToCover") {
            return static_cast<int>(aiPlayer->mAmountOfDistanceWhichRacketNeedsToCover);
        }

        Assert::Fail(L"AIPlayer doesn't have this variable of int type");
        return 0;
    }

    template<> BounceAreaAndAbsRangeTuple getMemberVar<BounceAreaAndAbsRangeTuple>(std::shared_ptr<Player>& player, std::string variableName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (variableName == "mChosenBounceAreaAndAbsRangeTuple") {
            return aiPlayer->mChosenBounceAreaAndAbsRangeTuple;
        }

        Assert::Fail(L"AIPlayer doesn't have this variable of BounceAreaAndAbsRangeTuple");
        return BounceAreaAndAbsRangeTuple{};
    }

    template<> std::vector<BounceAreaAndAbsRangeTuple> getMemberVar<std::vector<BounceAreaAndAbsRangeTuple>>(std::shared_ptr<Player>& player, std::string variableName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (variableName == "mTrickShotAttackAreasBottomTupleVector") {
            return aiPlayer->mTrickShotAttackAreasBottomTupleVector;
        }
        if (variableName == "mNormalShotAttackAreasBottomTupleVector") {
            return aiPlayer->mNormalShotAttackAreasBottomTupleVector;
        }

        Assert::Fail(L"AIPlayer doesn't have this variable of std::vector<BounceAreaAndAbsRangeTuple> type");
        return std::vector<BounceAreaAndAbsRangeTuple>{};
    }

    template<> float getMemberVar<float>(std::shared_ptr<Player>& player, std::string variableName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (variableName == "mTimeLeftToPlanAttack") {
            return aiPlayer->mTimeLeftToPlanAttack;
        }
        else if (variableName == "mTimeUntilCollisionWithAIGoalLine") {
            return aiPlayer->mTimeUntilCollisionWithAIGoalLine;
        }
        else if (variableName == "mPixelsLeftInLastContactBounceAreaTop") {
            return aiPlayer->mPixelsLeftInLastContactBounceAreaTop;
        }

        Assert::Fail(L"AIPlayer doesn't have this variable of float type");
        return 0.0f;
    }

    template<> AppConsts::BOUNCE_AREA getMemberVar<AppConsts::BOUNCE_AREA>(std::shared_ptr<Player>& player, std::string variableName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (variableName == "mLastContactBounceAreaTop") {
            return aiPlayer->mLastContactBounceAreaTop;
        }
        else if (variableName == "mLastContactBounceAreaBottom") {
            return aiPlayer->mLastContactBounceAreaBottom;
        }
    }

    template<> Vec2 getMemberVar<Vec2>(std::shared_ptr<Player>& player, std::string variableName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (variableName == "mGoalLineCollPos") {
            return aiPlayer->mGoalLineCollPos;
        }
        else if (variableName == "mGoalLineEffectivePucksCollRange") {
            return aiPlayer->mGoalLineEffectivePucksCollRange;
        }
        else if (variableName == "mRacketSprite->mVelocity") {
            return aiPlayer->mRacketSprite->mVelocity;
        }
        else if (variableName == "mRacketSprite->getPosition()") {
            return aiPlayer->mRacketSprite->getPosition();
        }
        else if (variableName == "mRacketsRangeAtInitialCollWithPuck") {
            return aiPlayer->mRacketsRangeAtInitialCollWithPuck;
        }
        else if (variableName == "mMaxDistanceCoverableByRacketTopAndBottom") {
            return aiPlayer->mMaxDistanceCoverableByRacketTopAndBottom;
        }
        else if (variableName == "mRacketsRangeAfterMaxDistanceCoverableByRacketBottom") {
            return aiPlayer->mRacketsRangeAfterMaxDistanceCoverableByRacketBottom;
        }
        else if (variableName == "mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom") {
            return aiPlayer->mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom;
        }

        Assert::Fail(L"AIPlayer doesn't have this variable of Vec2 type");
        return Vec2{};
    }

    template<> bool getMemberVar<bool>(std::shared_ptr<Player>& player, std::string variableName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (variableName == "mIdlingSetupDone") {
            return aiPlayer->mIdlingSetupDone;
        }
        else if (variableName == "mPositionResettingSetupDone") {
            return aiPlayer->mPositionResettingSetupDone;
        }
        else if (variableName == "mDistancingSetupDone") {
            return aiPlayer->mDistancingSetupDone;
        }
        else if (variableName == "mDefenseSetupDone") {
            return aiPlayer->mDefenseSetupDone;
        }
        else if (variableName == "mAttackSetupDone") {
            return aiPlayer->mAttackSetupDone;
        }

        Assert::Fail(L"AIPlayer doesn't have this variable of bool type");
        return false;
    }

    bool checkAIStateFuncPtr(std::shared_ptr<Player>& player, AppConsts::AI_STATES aiState) {
        using namespace AppConsts;

        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        //void (AIPlayer::* doIdlingPtr)() = &AIPlayer::doIdling;
        //void (AIPlayer::* aiStateFuncPtr)() = aiPlayer->mAIStateFuncPtr;

        switch (aiState) {
        case AI_STATES::IDLE:
            return aiPlayer->mAIStateFuncPtr == &AIPlayer::doIdling;
        case AI_STATES::DEFENSE:
            return aiPlayer->mAIStateFuncPtr == &AIPlayer::doDefending;
        case AI_STATES::DISTANCING:
            return aiPlayer->mAIStateFuncPtr == &AIPlayer::doDistancing;
        case AI_STATES::ATTACK:
            return aiPlayer->mAIStateFuncPtr == &AIPlayer::doAttacking;
        case AI_STATES::POSITION_RESET:
            return aiPlayer->mAIStateFuncPtr == &AIPlayer::doPositionResetting;
        default:
            Assert::Fail(L"AIPlayer doesn't have this kind of function state");
            break;
        }

        return false;
    };

    void callChangeAIStateAndStateFuncPtr(std::shared_ptr<Player>& player, const AppConsts::AI_STATES newAIState) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);
        aiPlayer->changeAIStateAndStateFuncPtr(newAIState);
    }

    template<typename T> T callValueReturningAIPlayerMethod(std::shared_ptr<Player>& player, const std::string methodName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (methodName == "calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel") {
            return aiPlayer->calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel();
        }
    }

    float callGetYCoordOfPucksCollPntWithRacketForAI(std::shared_ptr<Player>& player, const Vec2& racketsRange, const Vec2& pucksRange) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);
        return aiPlayer->getYCoordOfPucksCollPntWithRacketForAI(racketsRange, pucksRange);
    }

    void callVoidReturningAIPlayerMethod(std::shared_ptr<Player>& player, const std::string methodName) {
        std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast<AIPlayer>(player);

        if (methodName == "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine") {
            aiPlayer->calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine();
        }
        else if (methodName == "runAttackSetup") {
            aiPlayer->runAttackSetup();
        }
    }
};
