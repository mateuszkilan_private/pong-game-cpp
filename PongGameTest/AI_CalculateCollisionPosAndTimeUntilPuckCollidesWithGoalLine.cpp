#include "stdafx.h"

namespace HumanPlayerVsAIOnEasyTest
{
    TEST_CLASS(calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_method_test_1) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemRight;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = -180.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(320.0f, 250.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerOne);
        }
        TEST_METHOD(Make_sure_that_AI_is_defending) {
            AIPlayerAccessor aiPlayerAccessor{};
            Assert::IsTrue(GPuck->mVelocity.x < 0.0f && GPlayerOne->getState() == AI_STATES::DEFENSE);
            Assert::IsTrue(aiPlayerAccessor.checkAIStateFuncPtr(GPlayerOne, AI_STATES::DEFENSE));
        }
        TEST_METHOD(isIdlingSetupDone_should_be_false) {
            AIPlayerAccessor aiPlayerAccessor{};
            bool isIdlingSetupDone = aiPlayerAccessor.getMemberVar<bool>(GPlayerOne, "mIdlingSetupDone");
            Assert::IsFalse(isIdlingSetupDone);
        }
        TEST_METHOD(Run_calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine) {
            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerOne, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");

            auto goalLineCollPos = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerOne, "mGoalLineCollPos");

            Assert::AreEqual(23.0f, goalLineCollPos.x);
            Assert::AreEqual(250.0f, goalLineCollPos.y);
        }
    };

    TEST_CLASS(calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_method_test_2) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending_test_2) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemRight;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = 135.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(600.0f, 240.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerOne);
        }
        TEST_METHOD(Make_sure_that_AI_is_defending_test_2) {
            AIPlayerAccessor aiPlayerAccessor{};
            Assert::IsTrue(GPuck->mVelocity.x < 0.0f && GPlayerOne->getState() == AI_STATES::DEFENSE);
            Assert::IsTrue(aiPlayerAccessor.checkAIStateFuncPtr(GPlayerOne, AI_STATES::DEFENSE));
        }
        TEST_METHOD(isIdlingSetupDone_should_be_false_test_2) {
            AIPlayerAccessor aiPlayerAccessor{};
            bool isIdlingSetupDone = aiPlayerAccessor.getMemberVar<bool>(GPlayerOne, "mIdlingSetupDone");
            Assert::IsFalse(isIdlingSetupDone);
        }
        TEST_METHOD(Run_calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_test_2) {
            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerOne, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");

            auto goalLineCollPos = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerOne, "mGoalLineCollPos");

            Assert::AreEqual(23.0f, goalLineCollPos.x);
        }
    };

    TEST_CLASS(calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_method_test_3) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending_test_3) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemLeft;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = 60.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(40.0f, 240.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);
        }
        TEST_METHOD(Make_sure_that_AI_is_defending_test_3) {
            AIPlayerAccessor aiPlayerAccessor{};
            Assert::IsTrue(GPuck->mVelocity.x > 0.0f && GPlayerTwo->getState() == AI_STATES::DEFENSE);
            Assert::IsTrue(aiPlayerAccessor.checkAIStateFuncPtr(GPlayerTwo, AI_STATES::DEFENSE));
        }
        TEST_METHOD(isIdlingSetupDone_should_be_false_test_3) {
            AIPlayerAccessor aiPlayerAccessor{};
            bool isIdlingSetupDone = aiPlayerAccessor.getMemberVar<bool>(GPlayerTwo, "mIdlingSetupDone");
            Assert::IsFalse(isIdlingSetupDone);
        }
        TEST_METHOD(Run_calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_test_3) {
            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");

            auto goalLineCollPos = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mGoalLineCollPos");

            Assert::AreEqual(617.0f, goalLineCollPos.x);
        }
    };

    TEST_CLASS(calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_method_test_4) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending_test_4) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemLeft;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = 0.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(40.0f, 240.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);
        }
        TEST_METHOD(Make_sure_that_AI_is_defending_test_4) {
            AIPlayerAccessor aiPlayerAccessor{};
            Assert::IsTrue(GPuck->mVelocity.x > 0.0f && GPlayerTwo->getState() == AI_STATES::DEFENSE);
            Assert::IsTrue(aiPlayerAccessor.checkAIStateFuncPtr(GPlayerTwo, AI_STATES::DEFENSE));
        }
        TEST_METHOD(isIdlingSetupDone_should_be_false_test_4) {
            AIPlayerAccessor aiPlayerAccessor{};
            bool isIdlingSetupDone = aiPlayerAccessor.getMemberVar<bool>(GPlayerTwo, "mIdlingSetupDone");
            Assert::IsFalse(isIdlingSetupDone);
        }
        TEST_METHOD(Run_calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_test_4) {
            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");

            auto goalLineCollPos = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mGoalLineCollPos");

            Assert::AreEqual(617.0f, goalLineCollPos.x);
            Assert::AreEqual(240.0f, goalLineCollPos.y);
        }
    };

    TEST_CLASS(calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_method_test_5) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending_test_5) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemLeft;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = -45.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(40.0f, 240.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);
        }
        TEST_METHOD(Make_sure_that_AI_is_defending_test_5) {
            AIPlayerAccessor aiPlayerAccessor{};
            Assert::IsTrue(GPuck->mVelocity.x > 0.0f && GPlayerTwo->getState() == AI_STATES::DEFENSE);
            Assert::IsTrue(aiPlayerAccessor.checkAIStateFuncPtr(GPlayerTwo, AI_STATES::DEFENSE));
        }
        TEST_METHOD(isIdlingSetupDone_should_be_false_test_5) {
            AIPlayerAccessor aiPlayerAccessor{};
            bool isIdlingSetupDone = aiPlayerAccessor.getMemberVar<bool>(GPlayerTwo, "mIdlingSetupDone");
            Assert::IsFalse(isIdlingSetupDone);
        }
        TEST_METHOD(Run_calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine_test_5) {
            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");

            auto goalLineCollPos = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mGoalLineCollPos");

            Assert::AreEqual(617.0f, goalLineCollPos.x);
        }
    };
}
