#include "stdafx.h"

namespace HumanPlayerVsAIOnEasyTest
{
    TEST_CLASS(run_runAttackSetup) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemLeft;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = 0.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(40.0f, 400.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            Logger::WriteMessage(L"logging zzzzzzzzzzzzzzzzzzz ->\n");
            Logger::WriteMessage((std::to_wstring(GPuck->mVelocity.x) + L", " + std::to_wstring(GPuck->mVelocity.y)).c_str());
            Logger::WriteMessage(L"\n END logging zzzzzzzzzzzzzzzzzzz\n");

            GPlayerTwoRacket->setPosition(Vec2(632.0f, 300.0f));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);

            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");
            aiPlayerAccessor.callValueReturningAIPlayerMethod<AI_STATES>(GPlayerTwo, "calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel");
            aiPlayerAccessor.callChangeAIStateAndStateFuncPtr(GPlayerTwo, AI_STATES::ATTACK);
        }
        TEST_METHOD(Run_runAttackSetup_puck_hits_below_AIs_racket_check_runAttackSetup_member_variables) {
            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "runAttackSetup");

            auto racketsRangeAtInitialCollWithPuck = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mRacketsRangeAtInitialCollWithPuck");
            auto goalLineEffectivePucksCollRange = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mGoalLineEffectivePucksCollRange");
            auto racketsRelativePointBouncePlaceDuringFirstRecognition = aiPlayerAccessor.getMemberVar<int>(GPlayerTwo, "mRacketsRelativePointBouncePlaceDuringFirstRecognition");
            auto maxDistanceCoverableByRacketTopAndBottom = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mMaxDistanceCoverableByRacketTopAndBottom");

            Assert::AreEqual(100, racketsRelativePointBouncePlaceDuringFirstRecognition);
            Assert::AreEqual(393.0f, goalLineEffectivePucksCollRange.x);
            Assert::AreEqual(407.0f, goalLineEffectivePucksCollRange.y);
            Assert::AreEqual(393.0f - 100.0f, racketsRangeAtInitialCollWithPuck.x);
            Assert::AreEqual(393.0f, racketsRangeAtInitialCollWithPuck.y);
            Assert::AreEqual(0.0f, maxDistanceCoverableByRacketTopAndBottom.x);
            Assert::AreEqual(87.0f, maxDistanceCoverableByRacketTopAndBottom.y);
        }
        TEST_METHOD(check_bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity_set_members) {
            AIPlayerAccessor aiPlayerAccessor{};

            const auto amountOfDistanceWhichRacketNeedsToCover = aiPlayerAccessor.getMemberVar<int>(GPlayerTwo, "mAmountOfDistanceWhichRacketNeedsToCover");
            const auto& playersTwoRacketsVel = GPlayerTwo->mRacketSprite->mVelocity;

            const auto chosenBounceAreaAndAbsRangeTuple = aiPlayerAccessor.getMemberVar<BounceAreaAndAbsRangeTuple>(GPlayerTwo, "mChosenBounceAreaAndAbsRangeTuple");

            Logger::WriteMessage(L"\n======================= check final ========================\n");
            Logger::WriteMessage((L"amountOfDistanceWhichRacketNeedsToCover = " + std::to_wstring(amountOfDistanceWhichRacketNeedsToCover)).c_str());
            Logger::WriteMessage(L"\n");
            Logger::WriteMessage(std::to_wstring(playersTwoRacketsVel.x).c_str());
            Logger::WriteMessage(L"\n");
            Logger::WriteMessage(std::to_wstring(playersTwoRacketsVel.y).c_str());
            Logger::WriteMessage(L"\n======================= END check final ========================\n");
        }
    };
}
