#include "stdafx.h"
#include "CppUnitTest.h"
#include "TestHelpers.h"

#include "MenuItemSpriteWithoutMask.h"
#include "SpriteWithoutMask.h"
#include "Player.h"
#include "AppConsts.h"
#include "AppInitializers.h"
#include "GameController.h"
#include "PuckHelpers.h"
#include "MainMenuOperations.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace AppInitializers;
using namespace AppConsts;

void TestHelpers::SetDataNotSetProperlyByWindowCreatedInitializer() {
    GHalfOfPucksWidth = 8;
    GHalfOfPucksHeight = 8;

    GHalfOfRacketsHeight = 50;

    GLeftBorderOfGameArea = 15 + GHalfOfPucksWidth;
    GRightBorderOfGameArea = G_CLIENT_WIDTH - 15 - GHalfOfPucksWidth;
    GTopBorderOfGameArea = GHalfOfPucksHeight;
    GBottomBorderOfGameArea = G_CLIENT_HEIGHT - GHalfOfPucksHeight;
}
