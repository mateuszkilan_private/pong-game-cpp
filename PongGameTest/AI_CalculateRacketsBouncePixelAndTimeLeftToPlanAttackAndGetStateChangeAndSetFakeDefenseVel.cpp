#include "stdafx.h"

namespace HumanPlayerVsAIOnEasyTest
{
    TEST_CLASS(calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel_method_test_1) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemLeft;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = 0.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(40.0f, 240.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);

            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");
        }
        TEST_METHOD(Run_calculateRacketsBouncePixelAndTimeLeftToPlanAttack_puck_hits_exactly_at_rackets_position) {
            AIPlayerAccessor aiPlayerAccessor{};
            auto aiState = aiPlayerAccessor.callValueReturningAIPlayerMethod<AI_STATES>(GPlayerTwo, "calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel");

            auto goalLineEffectivePucksCollRange = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mGoalLineEffectivePucksCollRange");
            auto timeLeftToPlanAttack = aiPlayerAccessor.getMemberVar<float>(GPlayerTwo, "mTimeLeftToPlanAttack");
            auto timeUntilCollisionWithAIGoalLine = aiPlayerAccessor.getMemberVar<float>(GPlayerTwo, "mTimeUntilCollisionWithAIGoalLine");
            auto racketsRelativePointBouncePlaceDuringFirstRecognition = aiPlayerAccessor.getMemberVar<int>(GPlayerTwo, "mRacketsRelativePointBouncePlaceDuringFirstRecognition");
            auto racketVel = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mRacketSprite->mVelocity");

            Assert::AreEqual(static_cast<int>(AI_STATES::ATTACK), static_cast<int>(aiState));
            Assert::AreEqual(timeLeftToPlanAttack, timeUntilCollisionWithAIGoalLine);
            Assert::AreEqual(233.0f, goalLineEffectivePucksCollRange.x); // remember it is point not pixel
            Assert::AreEqual(247.0f, goalLineEffectivePucksCollRange.y); // remember it is point not pixel
            Assert::AreEqual(50, racketsRelativePointBouncePlaceDuringFirstRecognition);
            Assert::AreEqual(0.0f, racketVel.y);
        }
    };

    TEST_CLASS(calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel_method_test_2) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemLeft;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = 0.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(40.0f, 100.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GPlayerTwoRacket->setPosition(Vec2(8.0f, 200.0f));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);

            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");
        }
        TEST_METHOD(Run_calculateRacketsBouncePixelAndTimeLeftToPlanAttack_puck_hits_above_racket_AI_will_defend) {
            AIPlayerAccessor aiPlayerAccessor{};
            auto aiState = aiPlayerAccessor.callValueReturningAIPlayerMethod<AI_STATES>(GPlayerTwo, "calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel");

            auto goalLineEffectivePucksCollRange = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mGoalLineEffectivePucksCollRange");
            auto timeLeftToPlanAttack = aiPlayerAccessor.getMemberVar<float>(GPlayerTwo, "mTimeLeftToPlanAttack");
            auto timeUntilCollisionWithAIGoalLine = aiPlayerAccessor.getMemberVar<float>(GPlayerTwo, "mTimeUntilCollisionWithAIGoalLine");
            auto racketsRelativePointBouncePlaceDuringFirstRecognition = aiPlayerAccessor.getMemberVar<int>(GPlayerTwo, "mRacketsRelativePointBouncePlaceDuringFirstRecognition");
            auto racketVel = aiPlayerAccessor.getMemberVar<Vec2>(GPlayerTwo, "mRacketSprite->mVelocity");

            Assert::AreEqual(static_cast<int>(AI_STATES::ATTACK), static_cast<int>(aiState));
            Assert::AreEqual(93.0f, goalLineEffectivePucksCollRange.x); // remember it is point not pixel
            Assert::AreEqual(107.0f, goalLineEffectivePucksCollRange.y); // remember it is point not pixel
            Assert::AreEqual(0, racketsRelativePointBouncePlaceDuringFirstRecognition);
            Assert::AreEqual(0.0f, racketVel.y);
        }
    };

    TEST_CLASS(calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel_method_test_3) {
    public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup_AI_starts_by_defending) {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemLeft;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();

            // I want to make sure that AI will be defending and I want to set pucks position and angle explicitly
            auto pucksAngle = 0.0f;
            auto pucksVelVectorLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();

            GPuck->setPosition(Vec2(40.0f, 400.0f));
            GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));

            GPlayerTwoRacket->setPosition(Vec2(8.0f, 300.0f));

            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);

            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callVoidReturningAIPlayerMethod(GPlayerTwo, "calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine");
        }
        TEST_METHOD(Make_sure_AI_state_will_change_to_attack) {
            AIPlayerAccessor aiPlayerAccessor{};
            aiPlayerAccessor.callChangeAIStateAndStateFuncPtr(GPlayerTwo, AI_STATES::ATTACK);

            auto aiStateIsAttack = aiPlayerAccessor.checkAIStateFuncPtr(GPlayerTwo, AI_STATES::ATTACK);

            Assert::AreEqual(true, aiStateIsAttack);
        }
    };
}
