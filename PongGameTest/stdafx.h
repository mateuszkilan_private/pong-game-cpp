// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

// Headers for CppUnitTest
#include "CppUnitTest.h"

// TODO: reference additional headers your program requires here
#include <Windows.h>
#include <time.h>

// project includes
#include "resource2.h"
#include "MenuItemSpriteWithoutMask.h"
#include "SpriteWithoutMask.h"
#include "Player.h"
#include "AppConsts.h"
#include "AppInitializers.h"
#include "GameController.h"
#include "PuckHelpers.h"
#include "MainMenuOperations.h"

// test project includes
//#include "PlayerAccessor.h"
#include "AIPlayerAccessor.h"
#include "TestHelpers.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace AppInitializers;
using namespace AppConsts;
using namespace MainMenuOperations;
using namespace PuckHelpers;
using namespace TestHelpers;
