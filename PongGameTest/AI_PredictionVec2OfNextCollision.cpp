#include "stdafx.h"

namespace HumanPlayerVsAIOnEasyTest
{
    TEST_CLASS(PuckHelpers_GetPredictionVec2OfNextCollision) {
    public:
        TEST_CLASS_INITIALIZE(Basic_app_setup) {
            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();
        }

        TEST_METHOD(GetPredictionVec2OfNextCollision_100_0) {
            auto puckVel = Vec2(100.0f, 0.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(static_cast<float>(GRightBorderOfGameArea), puckPos.x);
            Assert::AreEqual(0.0f, puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_min_100_0) {
            auto puckVel = Vec2(-100.0f, 0.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(static_cast<float>(GLeftBorderOfGameArea), puckPos.x);
            Assert::AreEqual(0.0f, puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_0_100) {
            auto puckVel = Vec2(0.0f, 100.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(0.0f, puckPos.x);
            Assert::AreEqual(static_cast<float>(GBottomBorderOfGameArea), puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_0_min_100) {
            auto puckVel = Vec2(0.0f, -100.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(0.0f, puckPos.x);
            Assert::AreEqual(static_cast<float>(GTopBorderOfGameArea), puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_300_50) {
            auto puckVel = Vec2(300.0f, 50.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(static_cast<float>(GRightBorderOfGameArea), puckPos.x);
            Assert::AreEqual(-1.0f, puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_50_300) {
            auto puckVel = Vec2(50.0f, 300.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(-1.0f, puckPos.x);
            Assert::AreEqual(static_cast<float>(GBottomBorderOfGameArea), puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_400_min_50) {
            auto puckVel = Vec2(400.0f, -50.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(static_cast<float>(GRightBorderOfGameArea), puckPos.x);
            Assert::AreEqual(-1.0f, puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_50_min_400) {
            auto puckVel = Vec2(50.0f, -400.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(-1.0f, puckPos.x);
            Assert::AreEqual(static_cast<float>(GTopBorderOfGameArea), puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_min_500_60) {
            auto puckVel = Vec2(-500.0f, 60.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(static_cast<float>(GLeftBorderOfGameArea), puckPos.x);
            Assert::AreEqual(-1.0f, puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_min_50_600) {
            auto puckVel = Vec2(-50.0f, 600.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(-1.0f, puckPos.x);
            Assert::AreEqual(static_cast<float>(GBottomBorderOfGameArea), puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_min_50_min_500) {
            auto puckVel = Vec2(-50.0f, -500.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(-1.0f, puckPos.x);
            Assert::AreEqual(static_cast<float>(GTopBorderOfGameArea), puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_min_500_min_50) {
            auto puckVel = Vec2(-500.0f, -50.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(static_cast<float>(GLeftBorderOfGameArea), puckPos.x);
            Assert::AreEqual(-1.0f, puckPos.y);
        }
        TEST_METHOD(GetPredictionVec2OfNextCollision_not_moving) {
            auto puckVel = Vec2(0.0f, 0.0f);
            auto puckPos = Vec2(320.0f, 240.0f);
            GetPredictionVec2OfNextCollision(puckVel, puckPos);

            Assert::AreEqual(-1.0f, puckPos.x);
            Assert::AreEqual(-1.0f, puckPos.y);
        }
    };
}
