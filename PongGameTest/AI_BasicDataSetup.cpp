#include "stdafx.h"

namespace HumanPlayerVsAIOnEasyTest
{
	TEST_CLASS(BasicDataSetupTest) {
	public:
        TEST_CLASS_INITIALIZE(Human_vs_AI_basic_data_setup)
        {
            srand((unsigned int)time(NULL)); // seed random number generator

            MainMenuOperations::WindowCreatedInitializer(HWND());
            SetDataNotSetProperlyByWindowCreatedInitializer();

            GActiveMenuItem = GMenuItemRight;
            GSettingDifficulty = GAME_SETTINGS::DIFFICULTY_EASY;

            MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();
        }
		TEST_METHOD(Verify_active_menu_item_and_game_phase_and_setting_side) {
            Assert::IsTrue(GActiveMenuItem == nullptr);
            Assert::IsTrue(GCurrentGamePhase == GAME_PHASES::HUMAN_P1_VS_AI_GAME);
            Assert::IsTrue(GSettingSide == GAME_SETTINGS::SIDE_RIGHT);
            Assert::IsTrue(GMenuItemRestartDynGamePhase->mMainMenuOrGameTypeToActivateOnClick == GAME_PHASES::HUMAN_P1_VS_AI_GAME);
		}
        TEST_METHOD(Verify_if_player_one_is_AI_and_player_two_is_human) {
            Assert::IsTrue(GPlayerOne->isAI());
            Assert::IsTrue(GPlayerTwo->isHuman());
        }
        TEST_METHOD(Verify_if_players_rackets_are_at_default_position) {
            auto playerOneRacketsPos = GPlayerOneRacket->getPosition();
            auto playerTwoRacketsPos = GPlayerTwoRacket->getPosition();

            Assert::AreEqual(playerOneRacketsPos.x, G_RACKETS_LEFT_DEFAULT_POS.x);
            Assert::AreEqual(playerOneRacketsPos.y, G_RACKETS_LEFT_DEFAULT_POS.y);

            Assert::AreEqual(playerTwoRacketsPos.x, G_RACKETS_RIGHT_DEFAULT_POS.x);
            Assert::AreEqual(playerTwoRacketsPos.y, G_RACKETS_RIGHT_DEFAULT_POS.y);
        }
        TEST_METHOD(Is_AI_player_playing_on_the_left) {
            Assert::IsTrue(GPlayerOne->mSidePlayingOn == GAME_SETTINGS::SIDE_LEFT);
        }
        TEST_METHOD(Verify_that_pucks_pos_was_generated_successfully) {
            auto pucksPos = GPuck->getPosition();

            Assert::IsTrue(pucksPos.x == 320.0f);
            Assert::IsTrue(pucksPos.y >= 230.0f && pucksPos.y <= 250.0f);
        }
        TEST_METHOD(Verify_that_puck_is_moving) {
            auto pucksVel = GPuck->mVelocity;
            Assert::AreNotEqual(pucksVel.x, 0.0f); // pucks velocity x coord should never be equal to 0.0f

            if (IsPuckMovingLeft()) {
                Assert::IsTrue(IsPuckMovingLeft());
                Assert::IsFalse(IsPuckMovingRight());
            }
            else {
                Assert::IsTrue(IsPuckMovingRight());
                Assert::IsFalse(IsPuckMovingLeft());
            }
        }
        TEST_METHOD(AI_state_is_defend_or_distance_depending_on_pucks_direction) {
            auto& aiPlayer = GPlayerOne;
            auto pucksVel = GPuck->mVelocity;

            if (pucksVel.x < 0.0f && aiPlayer->getState() == AI_STATES::DEFENSE) {
                Logger::WriteMessage(L"TEST - AI_state_is_defend_or_distance_depending_on_pucks_direction - AI is defending");
            }
            else if (pucksVel.x > 0.0f && aiPlayer->getState() == AI_STATES::DISTANCING) {
                Logger::WriteMessage(L"TEST - AI_state_is_defend_or_distance_depending_on_pucks_direction - AI is distancing");
            }
            else {
                Assert::Fail(L"pucksVel and/or aiPlayer's state are not equal to expected values");
            }
        }
        TEST_METHOD(Puck_should_be_stopped_after_game_starts) {
            Assert::AreEqual(GStopPuckAfterGoalDeltaTime, 0.0f);
            Assert::IsTrue(GStopPuckAfterGoal);
        }
	};
}
