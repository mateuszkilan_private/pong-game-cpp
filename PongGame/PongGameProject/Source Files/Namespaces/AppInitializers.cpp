#include "../../Header Files/Classes/AIEventsDispatcher.h"
#include "../../Header Files/Namespaces/AppInitializers.h"


HWND AppInitializers::GhMainWnd = nullptr;
HINSTANCE AppInitializers::GhAppInst = nullptr;
HDC AppInitializers::GhSpriteDC = nullptr;
HMENU AppInitializers::GhMainMenu = nullptr;

int AppInitializers::GSleepTimeInMs = 0;

short int AppInitializers::GMouseCoordinateX = 0;
short int AppInitializers::GMouseCoordinateY = 0;

BackBuffer* AppInitializers::GBackBuffer = nullptr;
std::unique_ptr<AIEventsDispatcher> AppInitializers::GAIEventsDispatcher(nullptr);
AppConsts::COLLISION_SIDES AppInitializers::GRecentPucksCollisionSide(AppConsts::COLLISION_SIDES::NONE);

// VIEW BITMAPS BEGIN
// Common bitmaps
SpriteWithoutMask* AppInitializers::GBlackBackground = nullptr;
SpriteWithoutMask* AppInitializers::GMenuCopyrights = nullptr;
SpriteWithoutMask* AppInitializers::GMenuFrame = nullptr;
SpriteWithoutMask* AppInitializers::GMenuThePongGameLogo = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemBack = nullptr;
std::shared_ptr<SpriteWithoutMask> AppInitializers::GF1HowToPlayHint(nullptr);

// Main Menu View
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemNewGame = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemOptionsDynPos = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemCredits = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemExitGameDynPos = nullptr;

// New Game View
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemAIvsAI = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemP1vsAI = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemP1vsP2 = nullptr;

// AI Difficulty View
SpriteWithoutMask* AppInitializers::GMenuDifficulty = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemEasy = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemMedium = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemHard = nullptr;

// Choose Side View
SpriteWithoutMask* AppInitializers::GMenuChooseSide = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemLeft = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemRight = nullptr;

// Options View
SpriteWithoutMask* AppInitializers::GMenuGameSpeed = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemFast = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemNormal = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemSlow = nullptr;

// Credits View
SpriteWithoutMask* AppInitializers::GMenuCreditsText = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemCreditsBack = nullptr;

std::shared_ptr<SpriteWithoutMask> AppInitializers::GPlayerOneRacket(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GPlayerTwoRacket(nullptr);

std::shared_ptr<SpriteWithoutMask> AppInitializers::GPlayerOneSpeedBoost(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GPlayerTwoSpeedBoost(nullptr);

std::shared_ptr<SpriteWithoutMask> AppInitializers::GHashLine(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GPuck(nullptr);

std::shared_ptr<SpriteWithoutMask> AppInitializers::GMenuHowToPlay(nullptr);

// End Game View
std::shared_ptr<SpriteWithoutMask> AppInitializers::GAIWon{ nullptr };
std::shared_ptr<SpriteWithoutMask> AppInitializers::GP1Won{ nullptr };
std::shared_ptr<SpriteWithoutMask> AppInitializers::GP2Won{ nullptr };
std::shared_ptr<SpriteWithoutMask> AppInitializers::GColonDynPos{ nullptr };
std::shared_ptr<SpriteWithoutMask> AppInitializers::GFinalScore{ nullptr };
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemMainMenuDynPos{ nullptr };
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemPlayAgainDynGamePhase{ nullptr };

// Pause View
std::shared_ptr<SpriteWithoutMask> AppInitializers::GCurrentScore{ nullptr };
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemContinue{ nullptr };
MenuItemSpriteWithoutMask* AppInitializers::GMenuItemRestartDynGamePhase{ nullptr };

// Score numbers
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreZeroDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreOneDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreTwoDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreThreeDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreFourDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreFiveDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreSixDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreSevenDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreEightDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreNineDynPos(nullptr);
std::shared_ptr<SpriteWithoutMask> AppInitializers::GScoreTenDynPos(nullptr);
// VIEW BITMAPS END
bool AppInitializers::GShowScore(false);
float AppInitializers::GShowScoreDeltaTime{0.0f};

bool AppInitializers::GStopPuckAfterGoal(false);
float AppInitializers::GStopPuckAfterGoalDeltaTime{0.0f};

float AppInitializers::GRacketsChosenSpeedForCoordinateY{0.0f};

AppConsts::RACKET_SPEED_STATES AppInitializers::GPlayerOneRacketSpeedState(AppConsts::RACKET_SPEED_STATES::STILL);
AppConsts::RACKET_SPEED_STATES AppInitializers::GPlayerTwoRacketSpeedState(AppConsts::RACKET_SPEED_STATES::STILL);

std::shared_ptr<Player> AppInitializers::GPlayerOne(nullptr);
std::shared_ptr<Player> AppInitializers::GPlayerTwo(nullptr);

double AppInitializers::GTimeScaleSeconds = 0.0;
__int64 AppInitializers::GTimeCount = 0;
double AppInitializers::GLastTimeInSeconds = 0.0;
double AppInitializers::GCurrentTimeInSeconds = 0.0;
double AppInitializers::GDeltaTimeInSeconds = 0.0;

bool AppInitializers::GGameWasNotActive = false;

int AppInitializers::GLeftBorderOfGameArea(0.0f);
int AppInitializers::GRightBorderOfGameArea(0.0f);
int AppInitializers::GTopBorderOfGameArea(0.0f);
int AppInitializers::GBottomBorderOfGameArea(0.0f);

int AppInitializers::GHalfOfPucksWidth(0);
int AppInitializers::GHalfOfPucksHeight(0);

int AppInitializers::GHalfOfRacketsHeight(0);

float AppInitializers::GPucksSpeedBounceModifier(1.0f);

AppConsts::GAME_PHASES AppInitializers::GCurrentGamePhase = AppConsts::GAME_PHASES::INITIAL_MENU;
AppConsts::GAME_SETTINGS AppInitializers::GSettingDifficulty = AppConsts::GAME_SETTINGS::NONE;
AppConsts::GAME_SETTINGS AppInitializers::GSettingSide = AppConsts::GAME_SETTINGS::NONE;
AppConsts::GAME_SETTINGS AppInitializers::GSettingSpeed = AppConsts::GAME_SETTINGS::SPEED_FAST;
AppConsts::GAME_SETTINGS AppInitializers::GPreviousSettingSpeed = AppConsts::GAME_SETTINGS::NONE;
bool AppInitializers::GMenuHowToPlayIsVisible = false;
bool AppInitializers::GPauseMenuIsVisible = false;
AppConsts::GAME_PHASES AppInitializers::GPreviousGamePlayPhaseInfoForPauseMenu = AppConsts::GAME_PHASES::NONE;

MenuItemSpriteWithoutMask* AppInitializers::GActiveMenuItem = nullptr;
MenuItemSpriteWithoutMask* AppInitializers::GActiveSettingMenuItem = nullptr;
std::vector<MenuItemSpriteWithoutMask*> AppInitializers::GCurrentMenuItems;
