#include "../../Header Files/Namespaces/MainMenuOperations.h"
#include "../../Header Files/Namespaces/Utils.h"


void MainMenuOperations::WindowCreatedInitializer(HWND hWnd) {
	using namespace AppInitializers;
	using namespace AppConsts;

	GCurrentGamePhase = GAME_PHASES::INITIAL_MENU;
	GActiveMenuItem = nullptr;
	GhSpriteDC = CreateCompatibleDC(NULL);
	GBackBuffer = new BackBuffer(hWnd, G_CLIENT_WIDTH, G_CLIENT_HEIGHT);

    GAIEventsDispatcher = std::make_unique<AIEventsDispatcher>();

	// common bitmaps
	GBlackBackground = new SpriteWithoutMask(GhAppInst, BLACK_BACKGROUND_BM, G_CLIENT_CENTER, Vec2(0.0f, 0.0f));
	GMenuCopyrights = new SpriteWithoutMask(GhAppInst, MENU_COPYRIGHTS_BM, Vec2(320, 417), Vec2(0.0f, 0.0f));
	GMenuFrame = new SpriteWithoutMask(GhAppInst, MENU_FRAME_BM, Vec2(320, 240), Vec2(0.0f, 0.0f));
	GMenuThePongGameLogo = new SpriteWithoutMask(GhAppInst, MENU_THE_PONG_GAME_LOGO_BM, Vec2(320, 86), Vec2(0.0f, 0.0f));
	GMenuItemBack = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_BACK_BM, MENU_ITEM_BACK_ACTIVE_BM, Vec2(321, 191), GAME_PHASES::NONE); // this sprite's game_phases part will have to be changed dinamically
    GF1HowToPlayHint.reset(new SpriteWithoutMask(GhAppInst, F1_HOW_TO_PLAY_HINT, Vec2(321, 135), Vec2(0.0f, 0.0f)));

	// Gameplay view
	GHashLine.reset(new SpriteWithoutMask(GhAppInst, HASH_LINE_BM, Vec2(320, 240), Vec2(0.0f, 0.0f)));
	GPlayerOneRacket.reset(new SpriteWithoutMask(GhAppInst, RACKET_BM, G_RACKETS_LEFT_DEFAULT_POS, Vec2(0.0f, 0.0f)));
	GPlayerTwoRacket.reset(new SpriteWithoutMask(GhAppInst, RACKET_BM, G_RACKETS_RIGHT_DEFAULT_POS, Vec2(0.0f, 0.0f)));
	GPlayerOneSpeedBoost.reset(new SpriteWithoutMask(GhAppInst, SPEED_BOOST_PLUS_SIGN_BM, G_RACKETS_LEFT_DEFAULT_POS, Vec2(0.0f, 0.0f)));
	GPlayerTwoSpeedBoost.reset(new SpriteWithoutMask(GhAppInst, SPEED_BOOST_PLUS_SIGN_BM, G_RACKETS_RIGHT_DEFAULT_POS, Vec2(0.0f, 0.0f)));
	GPuck.reset(new SpriteWithoutMask(GhAppInst, BALL_BM, Vec2(80, 295), PuckHelpers::GetPucksSettingSpeedVector()));

	// Main Menu View
	GMenuItemNewGame = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_NEW_GAME_BM, MENU_ITEM_NEW_GAME_ACTIVE_BM, Vec2(321, 191), GAME_PHASES::NEW_GAME_MENU);
	GMenuItemOptionsDynPos = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_OPTIONS_BM, MENU_ITEM_OPTIONS_ACTIVE_BM, Vec2(0.0f, 0.0f), GAME_PHASES::OPTIONS_MENU);
	GMenuItemCredits = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_CREDITS_BM, MENU_ITEM_CREDITS_ACTIVE_BM, Vec2(321, 279), GAME_PHASES::CREDITS_MENU);
	GMenuItemExitGameDynPos = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_EXIT_GAME_BM, MENU_ITEM_EXIT_GAME_ACTIVE_BM, Vec2(0.0f, 0.0f), GAME_PHASES::EXIT_GAME);

	// New Game View
	GMenuItemAIvsAI = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_AI_VS_AI_BM, MENU_ITEM_AI_VS_AI_ACTIVE_BM, Vec2(321, 322), GAME_PHASES::AI_VS_AI_GAME);
	GMenuItemP1vsAI = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_P1_VS_AI_BM, MENU_ITEM_P1_VS_AI_ACTIVE_BM, Vec2(321, 235), GAME_PHASES::CHOOSE_AI_DIFFICULTY_MENU);
	GMenuItemP1vsP2 = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_P1_VS_P2_BM, MENU_ITEM_P1_VS_P2_ACTIVE_BM, Vec2(321, 279), GAME_PHASES::HUMAN_P1_VS_HUMAN_P2_GAME);

	// AI Difficulty View
	GMenuDifficulty = new SpriteWithoutMask(GhAppInst, MENU_DIFFICULTY_BM, Vec2(321, 235), Vec2(0.0f, 0.0f));
	GMenuItemEasy = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_EASY_BM, MENU_ITEM_EASY_ACTIVE_BM, Vec2(321, 279), GAME_PHASES::CHOOSE_SIDE_MENU, GAME_SETTINGS::DIFFICULTY_EASY);
	GMenuItemMedium = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_MEDIUM_BM, MENU_ITEM_MEDIUM_ACTIVE_BM, Vec2(321, 322), GAME_PHASES::CHOOSE_SIDE_MENU, GAME_SETTINGS::DIFFICULTY_MEDIUM);
	GMenuItemHard = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_HARD_BM, MENU_ITEM_HARD_ACTIVE_BM, Vec2(321, 365), GAME_PHASES::CHOOSE_SIDE_MENU, GAME_SETTINGS::DIFFICULTY_HARD);

	// Choose Side View
	GMenuChooseSide = new SpriteWithoutMask(GhAppInst, MENU_CHOOSE_SIDE_BM, Vec2(321, 235), Vec2(0.0f, 0.0f));
	GMenuItemLeft = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_LEFT_BM, MENU_ITEM_LEFT_ACTIVE_BM, Vec2(321, 279), GAME_PHASES::HUMAN_P1_VS_AI_GAME, GAME_SETTINGS::SIDE_LEFT);
	GMenuItemRight = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_RIGHT_BM, MENU_ITEM_RIGHT_ACTIVE_BM, Vec2(321, 322), GAME_PHASES::HUMAN_P1_VS_AI_GAME, GAME_SETTINGS::SIDE_RIGHT);

	// Options View
	GMenuGameSpeed = new SpriteWithoutMask(GhAppInst, MENU_GAME_SPEED_BM, Vec2(321, 235), Vec2(0.0f, 0.0f));
	GMenuItemSlow = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_SLOW_BM, MENU_ITEM_SLOW_ACTIVE_BM, Vec2(321, 279), GAME_PHASES::NONE, GAME_SETTINGS::SPEED_SLOW);
	GMenuItemNormal = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_NORMAL_BM, MENU_ITEM_NORMAL_ACTIVE_BM, Vec2(321, 322), GAME_PHASES::NONE, GAME_SETTINGS::SPEED_NORMAL);
	GMenuItemFast = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_FAST_BM, MENU_ITEM_FAST_ACTIVE_BM, Vec2(321, 365), GAME_PHASES::NONE, GAME_SETTINGS::SPEED_FAST);

	// Credits View
	GMenuCreditsText = new SpriteWithoutMask(GhAppInst, MENU_CREDITS_TEXT_BM, Vec2(328, 269), Vec2(0.0f, 0.0f));
	GMenuItemCreditsBack = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_BACK_BM, MENU_ITEM_BACK_ACTIVE_BM, Vec2(321, 175), GAME_PHASES::INITIAL_MENU);

	// HowToPLay View
	GMenuHowToPlay.reset(new SpriteWithoutMask(GhAppInst, MENU_HOW_TO_PLAY_BM, Vec2(320, 240), Vec2(0.0f, 0.0f)));

    // Score numbers
    GScoreZeroDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_ZERO_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreOneDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_ONE_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreTwoDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_TWO_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreThreeDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_THREE_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreFourDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_FOUR_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreFiveDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_FIVE_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreSixDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_SIX_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreSevenDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_SEVEN_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreEightDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_EIGHT_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreNineDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_NINE_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GScoreTenDynPos.reset(new SpriteWithoutMask(GhAppInst, SCORE_TEN_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));

    // End Game View
    GAIWon.reset(new SpriteWithoutMask(GhAppInst, AI_WON_BM, Vec2(320.0f, 173.0f), Vec2(0.0f, 0.0f)));
    GP1Won.reset(new SpriteWithoutMask(GhAppInst, P1_WON_BM, Vec2(320.0f, 173.0f), Vec2(0.0f, 0.0f)));
    GP2Won.reset(new SpriteWithoutMask(GhAppInst, P2_WON_BM, Vec2(320.0f, 173.0f), Vec2(0.0f, 0.0f)));
    GColonDynPos.reset(new SpriteWithoutMask(GhAppInst, COLON_BM, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f)));
    GFinalScore.reset(new SpriteWithoutMask(GhAppInst, FINAL_SCORE_BM, Vec2(320.0f, 72.0f), Vec2(0.0f, 0.0f)));
    GMenuItemMainMenuDynPos = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_MAIN_MENU_BM, MENU_ITEM_MAIN_MENU_ACTIVE_BM, Vec2(0.0f, 0.0f), GAME_PHASES::INITIAL_MENU);
    GMenuItemPlayAgainDynGamePhase = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_PLAY_AGAIN_BM, MENU_ITEM_PLAY_AGAIN_ACTIVE_BM, Vec2(320.0f, 235.0f), GAME_PHASES::NONE); // in here we need to set GAME_PHASES param when game starts

    // Pause View
    GCurrentScore.reset(new SpriteWithoutMask(GhAppInst, CURRENT_SCORE_BM, Vec2(319.0f, 72.0f), Vec2(0.0f, 0.0f)));
    GMenuItemContinue = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_CONTINUE_BM, MENU_ITEM_CONTINUE_ACTIVE_BM, Vec2(320.0f, 191.0f), GAME_PHASES::CONTINUE);
    GMenuItemRestartDynGamePhase = new MenuItemSpriteWithoutMask(GhAppInst, MENU_ITEM_RESTART_BM, MENU_ITEM_RESTART_ACTIVE_BM, Vec2(320.0f, 235.0f), GAME_PHASES::NONE); // in here we need to set GAME_PHASES param when game starts

    GRacketsChosenSpeedForCoordinateY = GetRacketsSettingSpeedForCoordinateY();

    GHalfOfPucksWidth = ceil((float)GPuck->width() / 2);
    GHalfOfPucksHeight = ceil((float)GPuck->height() / 2);

    GHalfOfRacketsHeight = ceil((float)GPlayerOneRacket->height() / 2);

    GLeftBorderOfGameArea = GPlayerOneRacket->width() + GHalfOfPucksWidth;
    GRightBorderOfGameArea = G_CLIENT_WIDTH - GPlayerTwoRacket->width() - GHalfOfPucksWidth;
    GTopBorderOfGameArea = GHalfOfPucksHeight;
    GBottomBorderOfGameArea = G_CLIENT_HEIGHT - GHalfOfPucksHeight;
}

const float MainMenuOperations::GetRacketsSettingSpeedForCoordinateY() {
    using namespace AppInitializers;
    using namespace AppConsts;

    switch (GSettingSpeed) {
    case GAME_SETTINGS::SPEED_SLOW:
        return G_RACKETS_SLOW_SPEED;
    case GAME_SETTINGS::SPEED_NORMAL:
        return G_RACKETS_NORMAL_SPEED;
    case GAME_SETTINGS::SPEED_FAST:
        return G_RACKETS_FAST_SPEED;
    };

    return G_RACKETS_FAST_SPEED;
}

void MainMenuOperations::DrawEndGameMenu() {
    using namespace AppInitializers;
    using namespace AppConsts;

    GMenuFrame->draw();
    GMenuCopyrights->draw();
    GFinalScore->draw();
    GColonDynPos->setPosition(G_COLON_END_GAME_POS);
    GColonDynPos->draw();

    GPlayerOne->getScoreForEndGameMenu()->draw();
    GPlayerTwo->getScoreForEndGameMenu()->draw();

    if (GPlayerOne->hasWonGame()) {
        GPlayerOne->mIsHumanPlayer ? GP1Won->draw() : GAIWon->draw();
    }
    else {
        GPlayerTwo->mIsHumanPlayer ? GP2Won->draw() : GAIWon->draw();
    }

    GCurrentMenuItems.clear();

    GMenuItemOptionsDynPos->setPosition(G_MENU_ITEM_OPTIONS_AT_END_GAME_POS);
    GMenuItemMainMenuDynPos->setPosition(G_MENU_ITEM_MAIN_MENU_AT_END_GAME_POS);
    GMenuItemExitGameDynPos->setPosition(G_MENU_ITEM_EXIT_GAME_AT_END_GAME_POS);

    GCurrentMenuItems.push_back(GMenuItemPlayAgainDynGamePhase->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemOptionsDynPos->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemMainMenuDynPos->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemExitGameDynPos->drawMenuItem());
}

void MainMenuOperations::DrawPauseMenu() {
    using namespace AppInitializers;
    using namespace AppConsts;

    GMenuFrame->draw();
    GMenuCopyrights->draw();
    GCurrentScore->draw();
    GColonDynPos->setPosition(G_COLON_PAUSE_POS);
    GColonDynPos->draw();

    GPlayerOne->getScoreForPauseMenu()->draw();
    GPlayerTwo->getScoreForPauseMenu()->draw();

    GCurrentMenuItems.clear();

    GMenuItemOptionsDynPos->setPosition(G_MENU_ITEM_OPTIONS_AT_PAUSE_POS);
    GMenuItemMainMenuDynPos->setPosition(G_MENU_ITEM_MAIN_MENU_AT_PAUSE_POS);
    GMenuItemExitGameDynPos->setPosition(G_MENU_ITEM_EXIT_GAME_AT_PAUSE_POS);

    GCurrentMenuItems.push_back(GMenuItemContinue->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemRestartDynGamePhase->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemOptionsDynPos->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemMainMenuDynPos->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemExitGameDynPos->drawMenuItem());
}

void MainMenuOperations::DrawInitialMenu() {
	using namespace AppInitializers;
    using namespace AppConsts;

	MainMenuOperations::DrawCommonMenuParts(true);
	GCurrentMenuItems.clear();

    GMenuItemOptionsDynPos->setPosition(G_MENU_ITEM_OPTIONS_AT_MAIN_MENU_POS);
    GMenuItemExitGameDynPos->setPosition(G_MENU_ITEM_EXIT_GAME_AT_MAIN_MENU_POS);

    GCurrentMenuItems.push_back(GMenuItemNewGame->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemOptionsDynPos->drawMenuItem());
	GCurrentMenuItems.push_back(GMenuItemCredits->drawMenuItem());
	GCurrentMenuItems.push_back(GMenuItemExitGameDynPos->drawMenuItem());
}

void MainMenuOperations::DrawNewGameMenu() {
    using namespace AppInitializers;
    using namespace AppConsts;

    MainMenuOperations::DrawCommonMenuParts(true);
    GCurrentMenuItems.clear();
    GMenuItemBack->mMainMenuOrGameTypeToActivateOnClick = GAME_PHASES::INITIAL_MENU;
    GCurrentMenuItems.push_back(GMenuItemBack->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemAIvsAI->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemP1vsAI->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemP1vsP2->drawMenuItem());
}

void MainMenuOperations::DrawChooseAIDifficultyMenu() {
    using namespace AppInitializers;
    using namespace AppConsts;

    MainMenuOperations::DrawCommonMenuParts(true);
    GMenuDifficulty->draw();

    GCurrentMenuItems.clear();
    GMenuItemBack->mMainMenuOrGameTypeToActivateOnClick = GAME_PHASES::NEW_GAME_MENU;
    GCurrentMenuItems.push_back(GMenuItemBack->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemEasy->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemMedium->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemHard->drawMenuItem());
}

void MainMenuOperations::DrawChooseSideMenu() {
    using namespace AppInitializers;
    using namespace AppConsts;

    MainMenuOperations::DrawCommonMenuParts(true);
    GMenuChooseSide->draw();

    GCurrentMenuItems.clear();
    GMenuItemBack->mMainMenuOrGameTypeToActivateOnClick = GAME_PHASES::CHOOSE_AI_DIFFICULTY_MENU;
    GCurrentMenuItems.push_back(GMenuItemBack->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemLeft->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemRight->drawMenuItem());
}

void MainMenuOperations::DrawOptionsMenu(AppConsts::GAME_PHASES previousGamePhase) {
    using namespace AppInitializers;
    using namespace AppConsts;

    MainMenuOperations::DrawCommonMenuParts(true);
    GMenuGameSpeed->draw();

    GCurrentMenuItems.clear();
    GMenuItemBack->mMainMenuOrGameTypeToActivateOnClick = previousGamePhase;
    GCurrentMenuItems.push_back(GMenuItemBack->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemSlow->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemNormal->drawMenuItem());
    GCurrentMenuItems.push_back(GMenuItemFast->drawMenuItem());

    MainMenuOperations::VisuallyActivateSpeedSetting();
}

void MainMenuOperations::DrawCreditsMenu() {
    using namespace AppInitializers;
    using namespace AppConsts;

    MainMenuOperations::DrawCommonMenuParts(false);
    GMenuCreditsText->draw();

    GCurrentMenuItems.clear();
    GCurrentMenuItems.push_back(GMenuItemCreditsBack->drawMenuItem());
}

void MainMenuOperations::DrawCommonMenuParts(bool drawMenuFrame) {
	using namespace AppInitializers;

	if (drawMenuFrame) {
		GMenuFrame->draw();
	}
	else {
		GBlackBackground->draw();
	}

	GMenuCopyrights->draw();
	GMenuThePongGameLogo->draw();

    GF1HowToPlayHint->draw();
}

bool MainMenuOperations::MouseCursorIsOverMenuItem(MenuItemSpriteWithoutMask* menuItem, short int mouseCoordinateX, short int mouseCoordinateY) {
	if (
		mouseCoordinateX >= menuItem->mUpperLeftCorner.x &&
		mouseCoordinateX <= menuItem->mLowerRightCorner.x &&
		mouseCoordinateY >= menuItem->mUpperLeftCorner.y &&
		mouseCoordinateY <= menuItem->mLowerRightCorner.y
		) {
		return true;
	}

	return false;
}

void MainMenuOperations::ActivateAndDeactivateMenuItem(short int mouseCoordinateX, short int mouseCoordinateY) {
	using namespace AppInitializers;
	// przy pomocy najechania nie mozna dezaktywowac settinga, setting moze byc dezaktywowany tylko poprzez klika na innego settinga z tej samej rodziny.
	// najlepiej by bylo zrobic tak - dodac tutaj informacje o tym, ze ma ta funkcja ma nie robic zadnych dzia�an majacych na celu dezaktywacje aktywnego setting menu item'a.
	// reszte dzialan moze wykonywac, ale nie moze dezaktywowac aktywny setting menu item - koniec kropka.
	// Aby zrealizowac to zadanie, bedzie konieczne dodanie funkcji, ktora bedzie odpowiedzialna za zwracanie pointera na setting menu item , ktory jest aktywny bazujac na aktualnej
	// wartosci zmiennej globalnej GSettingSpeed.

	MenuItemSpriteWithoutMask* menuItemToActivate = nullptr;
	// Patrze czy caly czas myszka jest na tym samym menu itemie , jezeli tak to nic nie robie.
	if (GActiveMenuItem != nullptr && MainMenuOperations::MouseCursorIsOverMenuItem(GActiveMenuItem, mouseCoordinateX, mouseCoordinateY)) {
		return;
	}

	for (auto constIt = GCurrentMenuItems.cbegin(); constIt != GCurrentMenuItems.cend(); ++constIt) {
		if (MainMenuOperations::MouseCursorIsOverMenuItem(*constIt, mouseCoordinateX, mouseCoordinateY)) {
			menuItemToActivate = *constIt;
		}
	}

	// oznacza to, ze jezdze sobie myszka gdzies po bokach i nic nie aktywuje ani dezaktywuje
	if (menuItemToActivate == nullptr && GActiveMenuItem == nullptr) {
		return;
	}

	// oznacza to, ze trzeba ten GActiveMenuItem dezaktywowac
	// ale nie ma go dezaktywowac jezeli ten menu item jest settingiem i jest aktywny
	if (GActiveMenuItem != nullptr) {
		if (GActiveMenuItem != MainMenuOperations::GetPointerToCurrentSpeedSettingSprite()) {
			GActiveMenuItem->drawMenuItem();
		}
		GActiveMenuItem = nullptr;
	}

	// jest menu item do aktywacji, trzeba go aktywowac
	if (menuItemToActivate != nullptr) {
		GActiveMenuItem = menuItemToActivate;
		menuItemToActivate->drawMenuItemActive();
	}

	GBackBuffer->present();
}

void MainMenuOperations::GenerateMenuViewBasedOnGCurrentGamePhase() {
    using namespace AppInitializers;
    using namespace AppConsts;

    switch (GCurrentGamePhase) {
    case GAME_PHASES::INITIAL_MENU:
        MainMenuOperations::DrawInitialMenu();
        break;
    case GAME_PHASES::NEW_GAME_MENU:
        MainMenuOperations::DrawNewGameMenu();
        break;
    case GAME_PHASES::CHOOSE_AI_DIFFICULTY_MENU:
        MainMenuOperations::DrawChooseAIDifficultyMenu();
        break;
    case GAME_PHASES::CHOOSE_SIDE_MENU:
        MainMenuOperations::DrawChooseSideMenu();
        break;
    case GAME_PHASES::OPTIONS_MENU:
        MainMenuOperations::DrawOptionsMenu(GCurrentGamePhase);
        break;
    case GAME_PHASES::CREDITS_MENU:
        MainMenuOperations::DrawCreditsMenu();
        break;
    case GAME_PHASES::END_GAME_MENU:
        MainMenuOperations::DrawEndGameMenu();
    case GAME_PHASES::PAUSE_MENU:
        MainMenuOperations::DrawPauseMenu();
        break;
    }

    GActiveMenuItem = nullptr;
    GBackBuffer->present();
}

void MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem() {
	using namespace AppInitializers;
	using namespace AppConsts;

	switch (GActiveMenuItem->mMainMenuOrGameTypeToActivateOnClick) {
	case GAME_PHASES::INITIAL_MENU:
        if (GCurrentGamePhase == GAME_PHASES::PAUSE_MENU) {
            GPreviousGamePlayPhaseInfoForPauseMenu = GAME_PHASES::NONE;
            GActiveMenuItem = nullptr;
        }

        GCurrentGamePhase = GAME_PHASES::INITIAL_MENU;
		MainMenuOperations::DrawInitialMenu();
		break;
	case GAME_PHASES::NEW_GAME_MENU:
        GCurrentGamePhase = GAME_PHASES::NEW_GAME_MENU;
        MainMenuOperations::DrawNewGameMenu();
		break;
	case GAME_PHASES::CHOOSE_AI_DIFFICULTY_MENU:
        GCurrentGamePhase = GAME_PHASES::CHOOSE_AI_DIFFICULTY_MENU;
        MainMenuOperations::DrawChooseAIDifficultyMenu();
		break;
	case GAME_PHASES::CHOOSE_SIDE_MENU:
        GCurrentGamePhase = GAME_PHASES::CHOOSE_SIDE_MENU;
        MainMenuOperations::ChangeDifficultySetting();
        MainMenuOperations::DrawChooseSideMenu();
		break;
	case GAME_PHASES::HUMAN_P1_VS_AI_GAME:
        GPlayerOneRacket->mVelocity.y = 0.0f;
        GPlayerTwoRacket->mVelocity.y = 0.0f;
        GPlayerOneRacket->setPosition(G_RACKETS_LEFT_DEFAULT_POS);
        GPlayerTwoRacket->setPosition(G_RACKETS_RIGHT_DEFAULT_POS);

        PuckHelpers::GenerateRandomPositionAndAngle(PuckHelpers::SelectStartingPucksDirection());

        MainMenuOperations::ChangeSideSetting();

        if (GCurrentGamePhase == GAME_PHASES::PAUSE_MENU) {
            GPreviousGamePlayPhaseInfoForPauseMenu = GAME_PHASES::NONE;
            GActiveMenuItem = nullptr;
        }
        else {
            GMenuItemRestartDynGamePhase->mMainMenuOrGameTypeToActivateOnClick = GAME_PHASES::HUMAN_P1_VS_AI_GAME;
        }

        GCurrentGamePhase = GAME_PHASES::HUMAN_P1_VS_AI_GAME;

		if (GSettingSide == GAME_SETTINGS::SIDE_LEFT) {
			GPlayerOne.reset(new HumanPlayer(GAME_SETTINGS::SIDE_LEFT, SPEED_BOOST_STATES::AVAILABLE, GPlayerOneRacket, GPlayerOneSpeedBoost));
			GPlayerTwo.reset(new AIPlayer(GAME_SETTINGS::SIDE_RIGHT, SPEED_BOOST_STATES::AVAILABLE, GPlayerTwoRacket, GPlayerTwoSpeedBoost, GSettingDifficulty));
            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerTwo);
		}
		else if (GSettingSide == GAME_SETTINGS::SIDE_RIGHT) {
			GPlayerOne.reset(new AIPlayer(GAME_SETTINGS::SIDE_LEFT, SPEED_BOOST_STATES::AVAILABLE, GPlayerOneRacket, GPlayerOneSpeedBoost, GSettingDifficulty));
			GPlayerTwo.reset(new HumanPlayer(GAME_SETTINGS::SIDE_RIGHT, SPEED_BOOST_STATES::AVAILABLE, GPlayerTwoRacket, GPlayerTwoSpeedBoost));
            GAIEventsDispatcher->defendOrDistanceAfterGameStarts(GPlayerOne);
		}

        GStopPuckAfterGoalDeltaTime = 0.0f;
        GStopPuckAfterGoal = true;
        COMPILE_LOG("GAME_PHASES::HUMAN_P1_VS_AI_GAME PuckHelpers::CountVelocityVectorsLengthUsingPythagoras(GPuck->mVelocity) = " + std::to_string(PuckHelpers::CountVelocityVectorsLengthUsingPythagoras(GPuck->mVelocity)), DEBUG_LOGS);
		break;
	case GAME_PHASES::HUMAN_P1_VS_HUMAN_P2_GAME:
        GPlayerOneRacket->mVelocity.y = 0.0f;
        GPlayerTwoRacket->mVelocity.y = 0.0f;
        GPlayerOneRacket->setPosition(G_RACKETS_LEFT_DEFAULT_POS);
        GPlayerTwoRacket->setPosition(G_RACKETS_RIGHT_DEFAULT_POS);

        PuckHelpers::GenerateRandomPositionAndAngle(PuckHelpers::SelectStartingPucksDirection());

        if (GCurrentGamePhase == GAME_PHASES::PAUSE_MENU) {
            GPreviousGamePlayPhaseInfoForPauseMenu = GAME_PHASES::NONE;
            GActiveMenuItem = nullptr;
        }
        else {
            GMenuItemRestartDynGamePhase->mMainMenuOrGameTypeToActivateOnClick = GAME_PHASES::HUMAN_P1_VS_HUMAN_P2_GAME;
        }

		GCurrentGamePhase = GAME_PHASES::HUMAN_P1_VS_HUMAN_P2_GAME;
		GPlayerOne.reset(new HumanPlayer(GAME_SETTINGS::SIDE_LEFT, SPEED_BOOST_STATES::AVAILABLE, GPlayerOneRacket, GPlayerOneSpeedBoost));
		GPlayerTwo.reset(new HumanPlayer(GAME_SETTINGS::SIDE_RIGHT, SPEED_BOOST_STATES::AVAILABLE, GPlayerTwoRacket, GPlayerTwoSpeedBoost));

        GStopPuckAfterGoalDeltaTime = 0.0f;
        GStopPuckAfterGoal = true;

		break;
	case GAME_PHASES::AI_VS_AI_GAME:
        GPlayerOneRacket->mVelocity.y = 0.0f;
        GPlayerTwoRacket->mVelocity.y = 0.0f;
        GPlayerOneRacket->setPosition(G_RACKETS_LEFT_DEFAULT_POS);
        GPlayerTwoRacket->setPosition(G_RACKETS_RIGHT_DEFAULT_POS);

        PuckHelpers::GenerateRandomPositionAndAngle(PuckHelpers::SelectStartingPucksDirection());

        if (GCurrentGamePhase == GAME_PHASES::PAUSE_MENU) {
            GPreviousGamePlayPhaseInfoForPauseMenu = GAME_PHASES::NONE;
            GActiveMenuItem = nullptr;
        }
        else {
            GMenuItemRestartDynGamePhase->mMainMenuOrGameTypeToActivateOnClick = GAME_PHASES::AI_VS_AI_GAME;
        }

        GCurrentGamePhase = GAME_PHASES::AI_VS_AI_GAME;
		GPlayerOne.reset(new AIPlayer(GAME_SETTINGS::SIDE_LEFT, SPEED_BOOST_STATES::AVAILABLE, GPlayerOneRacket, GPlayerOneSpeedBoost, GAME_SETTINGS::DIFFICULTY_HARD)); // AI should play with each other always on hard difficulty
		GPlayerTwo.reset(new AIPlayer(GAME_SETTINGS::SIDE_RIGHT, SPEED_BOOST_STATES::AVAILABLE, GPlayerTwoRacket, GPlayerTwoSpeedBoost, GAME_SETTINGS::DIFFICULTY_HARD)); // AI should play with each other always on hard difficulty

        GStopPuckAfterGoalDeltaTime = 0.0f;
        GStopPuckAfterGoal = true;

		break;
	case GAME_PHASES::OPTIONS_MENU:
        {
            GAME_PHASES previousGamePhase = GCurrentGamePhase;
            GCurrentGamePhase = GAME_PHASES::OPTIONS_MENU;
            MainMenuOperations::DrawOptionsMenu(previousGamePhase);
        }
		break;
	case GAME_PHASES::CREDITS_MENU:
        GCurrentGamePhase = GAME_PHASES::CREDITS_MENU;
        MainMenuOperations::DrawCreditsMenu();
		break;
	case GAME_PHASES::EXIT_GAME:
		DestroyWindow(GhMainWnd);
		break;
    case GAME_PHASES::END_GAME_MENU:
        GCurrentGamePhase = GAME_PHASES::END_GAME_MENU;
        MainMenuOperations::DrawEndGameMenu();
        break;
    case GAME_PHASES::PAUSE_MENU:
        GCurrentGamePhase = GAME_PHASES::PAUSE_MENU;
        MainMenuOperations::DrawPauseMenu();
        break;
    case GAME_PHASES::CONTINUE:
        GCurrentGamePhase = GPreviousGamePlayPhaseInfoForPauseMenu;
        GPreviousGamePlayPhaseInfoForPauseMenu = GAME_PHASES::NONE;
        GActiveMenuItem = nullptr;
        break;
	case GAME_PHASES::NONE:
		MainMenuOperations::ChangeAndVisuallyDeactivateOldSpeedSetting();
		MainMenuOperations::VisuallyActivateSpeedSetting();
		break;
	}

	GActiveMenuItem = nullptr;
	GBackBuffer->present();
}

void MainMenuOperations::ChangeDifficultySetting() {
	using namespace AppInitializers;
	using namespace AppConsts;

	AppConsts::GAME_SETTINGS newDifficultySetting = GActiveMenuItem->mSettingToActiveOnClick;

	switch (newDifficultySetting) {
	case GAME_SETTINGS::DIFFICULTY_EASY:
	case GAME_SETTINGS::DIFFICULTY_MEDIUM:
	case GAME_SETTINGS::DIFFICULTY_HARD:
		GSettingDifficulty = newDifficultySetting;
		return;
	}
}

void MainMenuOperations::ChangeSideSetting() {
	using namespace AppInitializers;
	using namespace AppConsts;

	AppConsts::GAME_SETTINGS newSideSetting = GActiveMenuItem->mSettingToActiveOnClick;

	switch (newSideSetting) {
	case GAME_SETTINGS::SIDE_LEFT:
	case GAME_SETTINGS::SIDE_RIGHT:
		GSettingSide = newSideSetting;
		return;
	}
}

void MainMenuOperations::ChangeAndVisuallyDeactivateOldSpeedSetting() {
	using namespace AppInitializers;
	using namespace AppConsts;
    using namespace GameController;
    using namespace PuckHelpers;

	GAME_SETTINGS speedSettingToChange = GActiveMenuItem->mSettingToActiveOnClick;
	// visually deactivating old speed setting
	MainMenuOperations::GetPointerToCurrentSpeedSettingSprite()->drawMenuItem();

	// change speed setting
    GPreviousSettingSpeed = GSettingSpeed;
	GSettingSpeed = speedSettingToChange;

    GPuck->mVelocity = IsCurrentMenuOfPauseMenuOrigin() ? GetPucksSettingSpeedVectorForPauseMenu() : GetPucksSettingSpeedVector();
    GRacketsChosenSpeedForCoordinateY = GetRacketsSettingSpeedForCoordinateY();
}

void MainMenuOperations::VisuallyActivateSpeedSetting() {
	using namespace AppInitializers;
	using namespace AppConsts;

	MainMenuOperations::GetPointerToCurrentSpeedSettingSprite()->drawMenuItemActive();
}

MenuItemSpriteWithoutMask* MainMenuOperations::GetPointerToCurrentSpeedSettingSprite() {
	using namespace AppInitializers;
	using namespace AppConsts;

	switch (GSettingSpeed) {
	case GAME_SETTINGS::SPEED_SLOW:
		return GMenuItemSlow;
	case GAME_SETTINGS::SPEED_NORMAL:
		return GMenuItemNormal;
	case GAME_SETTINGS::SPEED_FAST:
		return GMenuItemFast;
	}
}

void MainMenuOperations::DrawHowToPlayMenuAndPresent() {
    using namespace AppInitializers;

    GMenuHowToPlay->draw();
    GBackBuffer->present();
}
