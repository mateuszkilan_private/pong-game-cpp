#include "../../Header Files/Namespaces/Utils.h"
#include "../../Header Files/Namespaces/AppConsts.h"
#include <iostream>
#include <fstream>
#include <cassert>


using namespace std;
using namespace Test;

#define ONE_SPACE cout << "\n";
#define TWO_SPACES cout << "\n\n";
#define L << " " <<

std::vector<LogSection> G_LOG_SECTIONS_TO_LOG = {
    DEFEND_LOGS,
    ATTACK_LOGS,
    DISTANCE_LOGS,
    NONE_LOGS,
    POSITION_RESET_LOGS,
    IDLE_LOGS,
    CHANGE_STATE_LOGS,
    ANALYSE_COLLISION_LOGS,
    DEBUG_LOGS
};

static std::string getStringifiedLogSection(LogSection logsSection) {
    switch (logsSection) {
    case DEFEND_LOGS: return "DEFEND_LOGS";
    case ATTACK_LOGS: return "ATTACK_LOGS";
    case DISTANCE_LOGS: return "DISTANCE_LOGS";
    case NONE_LOGS: return "NONE_LOGS";
    case POSITION_RESET_LOGS: return "POSITION_RESET_LOGS";
    case IDLE_LOGS: return "IDLE_LOGS";
    case CHANGE_STATE_LOGS: return "CHANGE_STATE_LOGS";
    case ANALYSE_COLLISION_LOGS: return "ANALYSE_COLLISION_LOGS";
    case DEBUG_LOGS: return "DEBUG_LOGS";
    default: return "ops! something went bananas at getStringifiedLogSection!";
    }
}

std::string getStringifiedBounceArea(AppConsts::BOUNCE_AREA bounceArea) {
    using namespace AppConsts;

    switch (bounceArea) {
    case BOUNCE_AREA::FROM_0_TO_10: return "BOUNCE_AREA::FROM_0_TO_10";
    case BOUNCE_AREA::FROM_10_TO_20: return "BOUNCE_AREA::FROM_10_TO_20";
    case BOUNCE_AREA::FROM_20_TO_30: return "BOUNCE_AREA::FROM_20_TO_30";
    case BOUNCE_AREA::FROM_30_TO_70: return "BOUNCE_AREA::FROM_30_TO_70";
    case BOUNCE_AREA::FROM_70_TO_80: return "BOUNCE_AREA::FROM_70_TO_80";
    case BOUNCE_AREA::FROM_80_TO_90: return "BOUNCE_AREA::FROM_80_TO_90";
    case BOUNCE_AREA::FROM_90_TO_100: return "BOUNCE_AREA::FROM_90_TO_100";
    case BOUNCE_AREA::NONE: return "NONE";
    default: return "ops! something went bananas at getStringifiedBounceArea!";
    }
}

static std::string constructFrontOfLogSection(std::string stringToConstructFrontFrom) {
    if (stringToConstructFrontFrom.size() > 30) {
        throw "Max size of stringToConstructFrontFrom is 30 characters";
    }

    std::string frontOfLogSection = "                              "; // 30 empty characters to fill with info. Practically it means 27 visible characters for each front

    for (int i = 0; i < stringToConstructFrontFrom.size(); i++) {
        frontOfLogSection[i] = stringToConstructFrontFrom[i];
    }

    return frontOfLogSection;
}

void LogToFile(const std::string& text, LogSection logsSection, bool shouldBePrintedOnlyOnce, int printId, const std::string filePath)
{
    static std::vector<int> idsOfPrints;

    if (std::find(G_LOG_SECTIONS_TO_LOG.cbegin(), G_LOG_SECTIONS_TO_LOG.cend(), logsSection) == G_LOG_SECTIONS_TO_LOG.cend()) {
        return;
    }

    if (shouldBePrintedOnlyOnce) {

        if (std::find(idsOfPrints.cbegin(), idsOfPrints.cend(), printId) != idsOfPrints.cend()) {
            return;
        }

        idsOfPrints.emplace_back(printId);
    }

    std::ofstream log(filePath, std::ios_base::out | std::ios_base::app);

    log << constructFrontOfLogSection("\n-" + getStringifiedLogSection(logsSection) + "/ ") + text;
    log.close();
}

template <typename T>
void Test::debug(T debugVariable, string description) {
    // remember about overloading insertion operator for your personal types of data
    ONE_SPACE
    cout << "DEBUG " + description + " >>>>> " << debugVariable;
    ONE_SPACE
}

void Test::separator(string description) {
    ONE_SPACE
    cout << "<<<<<<<<<<<<<<<<<<<< " << description << " >>>>>>>>>>>>>>>>>>>>";
    ONE_SPACE
}

template <typename T>
void Test::eq(string testDescription, T testVariable, T valueToMatch) {
    ONE_SPACE
    if (testVariable != valueToMatch) {
        cout << "    Test FAILED: " << testDescription;
        return;
    }

    cout << "    Test SUCCESS: " << testDescription;
}

template <typename T>
void Test::notEq(string testDescription, T testVariable, T valueToMatch) {
    ONE_SPACE
    if (testVariable == valueToMatch) {
        cout << "    Test FAILED: " << testDescription;
        return;
    }

    cout << "    Test SUCCESS: " << testDescription;
}

void Test::suite(string suiteDescription) {
    ONE_SPACE
    cout << "<< Test suite: " << suiteDescription << " >>";
}

void Test::suiteEnd() {
    ONE_SPACE
    cout << "<< Test suite END >>";
}
