#include "../../Header Files/Namespaces/GameController.h"

void GameController::RunGameLoop() {
	using namespace AppInitializers;
	using namespace AppConsts;

    QueryPerformanceCounter((LARGE_INTEGER*)&GTimeCount);
    GCurrentTimeInSeconds = (double)GTimeCount * GTimeScaleSeconds;
    GDeltaTimeInSeconds = GCurrentTimeInSeconds - GLastTimeInSeconds;

    GameController::DrawFPSAsWindowCaption();

    if (GameController::IsCurrentPhaseGamePlayPhase() && GMenuHowToPlayIsVisible == false) {
        if (GGameWasNotActive) {
            GDeltaTimeInSeconds = 0.0;
            GGameWasNotActive = false;
        }

        GPlayerOne->analyseOrChangeSpeedBoostState(GDeltaTimeInSeconds);
        GPlayerTwo->analyseOrChangeSpeedBoostState(GDeltaTimeInSeconds);
        if (GPlayerOne->isAI()) {
            GPlayerOne->runAI();
        }
        if (GPlayerTwo->isAI()) {
            GPlayerTwo->runAI();
        }
        GDISpritesMovement::runRacketsMovementDetection(GDeltaTimeInSeconds);
        if (!PuckHelpers::StopPuckAfterGoal(GDeltaTimeInSeconds)) {
            GDISpritesMovement::runPucksCollisionDetection(GDeltaTimeInSeconds);
        }
        if (GameController::IsCurrentPhaseGamePlayPhase()) {
            // During runPucksCollisionDetection there can be end of game, that is why we need to have this condition here
            GameController::DrawAndPresentGameplay(GDeltaTimeInSeconds);
        }
    }

    GLastTimeInSeconds = GCurrentTimeInSeconds;

	Sleep(GSleepTimeInMs);
}

void GameController::DrawAndPresentGameplay(float deltaTime) {
	using namespace AppInitializers;
	using namespace AppConsts;

    GBlackBackground->draw();
    GHashLine->draw();
    GPuck->draw();
    GPlayerOne->mRacketSprite->draw();
    GPlayerTwo->mRacketSprite->draw();

    if (GPlayerOne->mSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE) {
        GPlayerOne->mSpeedBoostSprite->draw();
    }

    if (GPlayerTwo->mSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE) {
        GPlayerTwo->mSpeedBoostSprite->draw();
    }

    GameController::AnalyseAndDrawScoreForGameArea(deltaTime);
    GBackBuffer->present();
}

void GameController::AnalyseAndDrawScoreForGameArea(float deltaTime) {
    using namespace AppInitializers;
    using namespace AppConsts;

    if (!GShowScore) {
        return;
    }

    GShowScoreDeltaTime += deltaTime;
    if (GShowScoreDeltaTime >= 0.5f) {
        GShowScore = false;
        GShowScoreDeltaTime = 0.0f;
        return;
    }

    GPlayerOne->getScoreForGameArea()->draw();
    GPlayerTwo->getScoreForGameArea()->draw();
}

void GameController::DrawFPSAsWindowCaption() {
	static int frameCnt = 0;
	static float timeElapsed = 0;
	static wchar_t buffer[256];

	++frameCnt;
    timeElapsed += AppInitializers::GDeltaTimeInSeconds;

	if (timeElapsed < 1.0f) return;

	wsprintf(buffer, L" | %d FPS", frameCnt);

	SetWindowText(AppInitializers::GhMainWnd, (AppConsts::G_WINDOW_CAPTION + buffer).c_str());

	frameCnt = 0;
	timeElapsed = 0.0f;
}

bool GameController::InitMainWnd(int showCmd) {
	using namespace AppConsts;
	using namespace AppInitializers;

	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GhAppInst;
	wc.hIcon = ::LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = ::LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)::GetStockObject(NULL_BRUSH);
	wc.lpszMenuName = 0;
	wc.lpszClassName = L"PongGameWnd";

	RegisterClass(&wc);

    RECT clientRect;
    clientRect.left = 0;
    clientRect.right = G_CLIENT_WIDTH;
    clientRect.top = 0;
    clientRect.bottom = G_CLIENT_HEIGHT;
    bool isSuccess = AdjustWindowRectEx(&clientRect, WS_SYSMENU | WS_CAPTION, true, 0);

	GhMainMenu = LoadMenu(GhAppInst, MAKEINTRESOURCE(IDR_PONG_MENU));
	GhMainWnd = ::CreateWindowEx(
        0,
		L"PongGameWnd",
		G_WINDOW_CAPTION.c_str(),
		WS_SYSMENU | WS_CAPTION,
		200,
		200,
        clientRect.right - clientRect.left,
		clientRect.bottom - clientRect.top,
		0,
		GhMainMenu,
		GhAppInst,
		0
	);

	if (GhMainWnd == NULL) {
		::MessageBox(NULL, L"Window creation failed!", L"CreateWindow error", MB_OK);
		return false;
	}

	ShowWindow(GhMainWnd, showCmd);
	UpdateWindow(GhMainWnd);
	return true;
}

bool GameController::IsCurrentPhaseGamePlayPhase() {
    using namespace AppInitializers;
    using namespace AppConsts;

    switch (GCurrentGamePhase) {
    case GAME_PHASES::HUMAN_P1_VS_AI_GAME:
    case GAME_PHASES::HUMAN_P1_VS_HUMAN_P2_GAME:
    case GAME_PHASES::AI_VS_AI_GAME:
        return true;
    }

    return false;
}

bool GameController::IsCurrentMenuOfPauseMenuOrigin() {
    using namespace AppInitializers;
    using namespace AppConsts;

    if (GPreviousGamePlayPhaseInfoForPauseMenu != GAME_PHASES::NONE) {
        return true;
    }

    return false;
}

LRESULT CALLBACK GameController::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	using namespace AppInitializers;
	using namespace AppConsts;

	switch (msg) {
    case WM_WINDOWPOSCHANGING:
        GGameWasNotActive = true;
        break;
    case WM_ENTERMENULOOP:
        GGameWasNotActive = true;
        break;
	case WM_KEYDOWN:
		switch (wParam) {
        case VK_ESCAPE:
            if (GCurrentGamePhase == GAME_PHASES::OPTIONS_MENU && GPreviousGamePlayPhaseInfoForPauseMenu != GAME_PHASES::NONE || GCurrentGamePhase == GAME_PHASES::PAUSE_MENU) {
                if (GPlayerOne->isHuman()) {
                    // It means that we are turning off pause menu and we need to make sure that racket will behave appropriately when going back to game.
                    auto isWPressed = (1 << 15) & GetKeyState(0x57) ? true : false;
                    auto isSPressed = (1 << 15) & GetKeyState(0x53) ? true : false;

                    if ((isWPressed && isSPressed) || (!isWPressed && !isSPressed)) {
                        GPlayerOneRacket->mVelocity.y = 0.0f;
                    }
                    else if (isWPressed) {
                        GPlayerOneRacket->mVelocity.y = GPlayerOne->mSpeedBoostState == SPEED_BOOST_STATES::ON ? -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : -GRacketsChosenSpeedForCoordinateY;
                    }
                    else {
                        GPlayerOneRacket->mVelocity.y = GPlayerOne->mSpeedBoostState == SPEED_BOOST_STATES::ON ? GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : GRacketsChosenSpeedForCoordinateY;
                    }
                }
                if (GPlayerTwo->isHuman()) {
                    // It means that we are turning off pause menu and we need to make sure that racket will behave appropriately when going back to game.
                    auto isUpPressed = (1 << 15) & GetKeyState(VK_UP) ? true : false;
                    auto isDownPressed = (1 << 15) & GetKeyState(VK_DOWN) ? true : false;

                    if ((isUpPressed && isDownPressed) || (!isUpPressed && !isDownPressed)) {
                        GPlayerTwoRacket->mVelocity.y = 0.0f;
                    }
                    else if (isUpPressed) {
                        GPlayerTwoRacket->mVelocity.y = GPlayerTwo->mSpeedBoostState == SPEED_BOOST_STATES::ON ? -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : -GRacketsChosenSpeedForCoordinateY;
                    }
                    else {
                        GPlayerTwoRacket->mVelocity.y = GPlayerTwo->mSpeedBoostState == SPEED_BOOST_STATES::ON ? GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : GRacketsChosenSpeedForCoordinateY;
                    }
                }
                GCurrentGamePhase = GPreviousGamePlayPhaseInfoForPauseMenu;
                GPreviousGamePlayPhaseInfoForPauseMenu = GAME_PHASES::NONE;
                GActiveMenuItem = nullptr; // This assignment allows the same menu option to be automatically highlighted again
                break;
            }

            if (!GameController::IsCurrentPhaseGamePlayPhase()) {
                break;
            }

            GGameWasNotActive = true;
            GPreviousGamePlayPhaseInfoForPauseMenu = GCurrentGamePhase;
            GCurrentGamePhase = GAME_PHASES::PAUSE_MENU;
            MainMenuOperations::DrawPauseMenu();
            GBackBuffer->present();
            SendMessage(GhMainWnd, WM_MOUSEMOVE, wParam, lParam);
            break;
		case VK_F1:
            GMenuHowToPlayIsVisible = true;
            GGameWasNotActive = true;
            MainMenuOperations::DrawHowToPlayMenuAndPresent();
            break;
        // Player 1 controls
        case 0x57:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerOne->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(0x53)) {
                break;
            }
            GPlayerOneRacket->mVelocity.y = GPlayerOne->mSpeedBoostState == SPEED_BOOST_STATES::ON ? -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : -GRacketsChosenSpeedForCoordinateY;
            break;
        case 0x53:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerOne->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(0x57)) {
                break;
            }
            GPlayerOneRacket->mVelocity.y = GPlayerOne->mSpeedBoostState == SPEED_BOOST_STATES::ON ? GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : GRacketsChosenSpeedForCoordinateY;
            break;
        case VK_SPACE:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerOne->isAI()) {
                break;
            }

            {
                SPEED_BOOST_STATES& playerOneSpeedBoostState = GPlayerOne->mSpeedBoostState;
                if (playerOneSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE) {
                    playerOneSpeedBoostState = SPEED_BOOST_STATES::ON;

                    auto& playerOneRacketsVelocity = GPlayerOneRacket->mVelocity.y;
                    if (playerOneRacketsVelocity == -GRacketsChosenSpeedForCoordinateY) {
                       playerOneRacketsVelocity = -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER;
                    }
                    else if (playerOneRacketsVelocity == GRacketsChosenSpeedForCoordinateY) {
                       playerOneRacketsVelocity = GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER;
                    }
                }
                break;
            }
        // Player 2 controls
        case VK_UP:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerTwo->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(VK_DOWN)) {
                break;
            }
            GPlayerTwoRacket->mVelocity.y = GPlayerTwo->mSpeedBoostState == SPEED_BOOST_STATES::ON ? -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : -GRacketsChosenSpeedForCoordinateY;
            break;
        case VK_DOWN:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerTwo->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(VK_UP)) {
                break;
            }
            GPlayerTwoRacket->mVelocity.y = GPlayerTwo->mSpeedBoostState == SPEED_BOOST_STATES::ON ? GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : GRacketsChosenSpeedForCoordinateY;
            break;
        case VK_CONTROL:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerTwo->isAI()) {
                break;
            }

            {
                SPEED_BOOST_STATES& playerTwoSpeedBoostState = GPlayerTwo->mSpeedBoostState;
                if (playerTwoSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE) {
                    playerTwoSpeedBoostState = SPEED_BOOST_STATES::ON;

                    auto& playerTwoRacketsVelocity = GPlayerTwoRacket->mVelocity.y;
                    if (playerTwoRacketsVelocity == -GRacketsChosenSpeedForCoordinateY) {
                       playerTwoRacketsVelocity = -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER;
                    }
                    else if (playerTwoRacketsVelocity == GRacketsChosenSpeedForCoordinateY) {
                       playerTwoRacketsVelocity = GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER;
                    }
                }
                break;
            }
		}

		return NULL;
	case WM_KEYUP:
		switch (wParam) {
		case VK_F1:
            GMenuHowToPlayIsVisible = false;
            // if player is currently in game mode, then redraw will be done by game loop and not by keyup message
            if (GameController::IsCurrentPhaseGamePlayPhase()) break;
            MainMenuOperations::GenerateMenuViewBasedOnGCurrentGamePhase();
            SendMessage(GhMainWnd, WM_MOUSEMOVE, wParam, lParam);
            break;
        // Player 1 controls
        case 0x57:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerOne->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(0x53)) {
                GPlayerOneRacket->mVelocity.y = GPlayerOne->mSpeedBoostState == SPEED_BOOST_STATES::ON ? GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : GRacketsChosenSpeedForCoordinateY;
                break;
            }
            GPlayerOneRacket->mVelocity.y = 0.0f;
            break;
        case 0x53:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerOne->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(0x57)) {
                GPlayerOneRacket->mVelocity.y = GPlayerOne->mSpeedBoostState == SPEED_BOOST_STATES::ON ? -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : -GRacketsChosenSpeedForCoordinateY;
                break;
            }
            GPlayerOneRacket->mVelocity.y = 0.0f;
            break;
        // Player 2 controls
        case VK_UP:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerTwo->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(VK_DOWN)) {
                GPlayerTwoRacket->mVelocity.y = GPlayerTwo->mSpeedBoostState == SPEED_BOOST_STATES::ON ? GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : GRacketsChosenSpeedForCoordinateY;
                break;
            }
            GPlayerTwoRacket->mVelocity.y = 0.0f;
            break;
        case VK_DOWN:
            if (!IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible == true || GPlayerTwo->isAI()) {
                break;
            }

            if ((1 << 15) & GetKeyState(VK_UP)) {
                GPlayerTwoRacket->mVelocity.y = GPlayerTwo->mSpeedBoostState == SPEED_BOOST_STATES::ON ? -GRacketsChosenSpeedForCoordinateY * G_RACKETS_SPEED_BOOST_MODIFIER : -GRacketsChosenSpeedForCoordinateY;
                break;
            }
            GPlayerTwoRacket->mVelocity.y = 0.0f;
            break;
		}
		return NULL;
	case WM_CREATE:
		MainMenuOperations::WindowCreatedInitializer(hWnd);
		break;
	case WM_PAINT:
		MainMenuOperations::DrawInitialMenu();
        GBackBuffer->present();
		SendMessage(GhMainWnd, WM_MOUSEMOVE, wParam, lParam);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam)) {
		case ID_FILE_EXIT:
			DestroyWindow(GhMainWnd);
			break;
        case ID_LESS_FPS_LESS_CPU_USAGE_MENU:
            {
                HMENU menu = GetMenu(hWnd);
                CheckMenuItem(menu, ID_LESS_FPS_LESS_CPU_USAGE_MENU, MF_CHECKED);
                CheckMenuItem(menu, ID_MORE_FPS_MORE_CPU_USAGE_MENU, MF_UNCHECKED);
                GSleepTimeInMs = 1;
            }
            break;
        case ID_MORE_FPS_MORE_CPU_USAGE_MENU:
            {
                HMENU menu = GetMenu(hWnd);
                CheckMenuItem(menu, ID_LESS_FPS_LESS_CPU_USAGE_MENU, MF_UNCHECKED);
                CheckMenuItem(menu, ID_MORE_FPS_MORE_CPU_USAGE_MENU, MF_CHECKED);
                GSleepTimeInMs = 0;
            }
            break;
		}

		return NULL;
	case WM_LBUTTONUP:
		// We don't want to do anything if void will be clicked or when game phases is not menu
		if (GActiveMenuItem == nullptr || GameController::IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible) return NULL;
		MainMenuOperations::ChangeMenuOrRunGameBasedOnGActiveMenuItem();
		SendMessage(GhMainWnd, WM_MOUSEMOVE, wParam, lParam);
		return NULL;
	case WM_MOUSEMOVE:
		// We don't want to do anything if GCurrentGamePhase is game type phase
        if (GameController::IsCurrentPhaseGamePlayPhase() || GMenuHowToPlayIsVisible) {
            return NULL;
        }
        // Here we activate every menu item after hovering over it with mouse.
        {
            POINT mouseCoordinates{};
            GetCursorPos(&mouseCoordinates);
            ScreenToClient(GhMainWnd, &mouseCoordinates);
            MainMenuOperations::ActivateAndDeactivateMenuItem(mouseCoordinates.x, mouseCoordinates.y);
        }
		return NULL;
	case WM_DESTROY:
		srand(1); // unseed random number generator
        COMPILE_LOG("WM_DESTROY PuckHelpers::CountVelocityVectorsLengthUsingPythagoras(GPuck->mVelocity) = " + std::to_string(PuckHelpers::CountVelocityVectorsLengthUsingPythagoras(GPuck->mVelocity)), DEBUG_LOGS);
		PostQuitMessage(NULL);
		return NULL;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}
