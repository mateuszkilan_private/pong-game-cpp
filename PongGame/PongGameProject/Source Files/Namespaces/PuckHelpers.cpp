#include "../../Header Files/Namespaces/PuckHelpers.h"

using namespace AppInitializers;
using namespace AppConsts;
using namespace GameController;
using namespace MainMenuOperations;
using namespace GDISpritesMovement;

const Vec2 PuckHelpers::GetEffectivePuckCollisionRange(Vec2 pucksPos) {
    auto pucksUpperYCoordinate = pucksPos.y - GHalfOfPucksHeight;
    auto pucksLowerYCoordinate = pucksPos.y + GHalfOfPucksHeight;

    return Vec2(pucksUpperYCoordinate + 1, pucksLowerYCoordinate - 1);
}

const Vec2 PuckHelpers::GetPucksSettingSpeedVector() {
    switch (GSettingSpeed) {
    case GAME_SETTINGS::SPEED_SLOW:
        return G_PUCKS_BASE_SPEED * G_PUCKS_SLOW_SPEED_MODIFIER;
    case GAME_SETTINGS::SPEED_NORMAL:
        return G_PUCKS_BASE_SPEED * G_PUCKS_NORMAL_SPEED_MODIFIER;
    case GAME_SETTINGS::SPEED_FAST:
        return G_PUCKS_BASE_SPEED * G_PUCKS_FAST_SPEED_MODIFIER;
    };

    return G_PUCKS_BASE_SPEED * G_PUCKS_FAST_SPEED_MODIFIER;
}

const Vec2 PuckHelpers::GetPucksSettingSpeedVectorForPauseMenu() {
    // When we are in pause menu it means that puck can have bounce modifier changed by fast racket. We need to take that into consideration.
    auto pucksVel = GPuck->mVelocity;
    auto previousSpeedModifier = 0.0f;
    auto currentSpeedModifier = 0.0f;

    switch (GPreviousSettingSpeed) {
    case GAME_SETTINGS::SPEED_SLOW:
        previousSpeedModifier = G_PUCKS_SLOW_SPEED_MODIFIER;
        break;
    case GAME_SETTINGS::SPEED_NORMAL:
        previousSpeedModifier = G_PUCKS_NORMAL_SPEED_MODIFIER;
        break;
    case GAME_SETTINGS::SPEED_FAST:
        previousSpeedModifier = G_PUCKS_FAST_SPEED_MODIFIER;
        break;
    }

    switch (GSettingSpeed) {
    case GAME_SETTINGS::SPEED_SLOW:
        currentSpeedModifier = G_PUCKS_SLOW_SPEED_MODIFIER;
        break;
    case GAME_SETTINGS::SPEED_NORMAL:
        currentSpeedModifier = G_PUCKS_NORMAL_SPEED_MODIFIER;
        break;
    case GAME_SETTINGS::SPEED_FAST:
        currentSpeedModifier = G_PUCKS_FAST_SPEED_MODIFIER;
        break;
    }

    pucksVel = (pucksVel / GPucksSpeedBounceModifier) / previousSpeedModifier;

    return (pucksVel * currentSpeedModifier) * GPucksSpeedBounceModifier;
}

const float PuckHelpers::GetYCoordinateOfPucksCollisionPointWithRacket(const Vec2& racketRange) {
    // This function assumes that there is/will be collision with racket. Otherwise this function will not work correctly.
    auto upperYCollisionRange = 0.0f;
    auto lowerYCollisionRange = 0.0f;
    const auto pucksRange = GetEffectivePuckCollisionRange();

    upperYCollisionRange = pucksRange.x < racketRange.x ? racketRange.x : pucksRange.x;
    lowerYCollisionRange = pucksRange.y > racketRange.y ? racketRange.y : pucksRange.y;

    return upperYCollisionRange + (lowerYCollisionRange - upperYCollisionRange) / 2.0f;
}

bool PuckHelpers::StopPuckAfterGoal(float deltaTime) {
    if (!GStopPuckAfterGoal) {
        return GStopPuckAfterGoal;
    }

    GStopPuckAfterGoalDeltaTime += deltaTime;

    if (GStopPuckAfterGoalDeltaTime >= 1.0f) {
        GAIEventsDispatcher->aiChangeStateToDefenseOrDistanceFromPositionResetOrIdle(GPlayerOne);
        GAIEventsDispatcher->aiChangeStateToDefenseOrDistanceFromPositionResetOrIdle(GPlayerTwo);
        GStopPuckAfterGoal = false;
    }

    return GStopPuckAfterGoal;
}

void PuckHelpers::AnalyseCollision(std::shared_ptr<Player>& collisionalPlayer, std::shared_ptr<Player>& otherPlayer, AppConsts::COLLISION_SIDES probableCollisionSide) {
    const auto racketRange = collisionalPlayer->getRacketsRange();
    const auto pucksCollisionRange = GetEffectivePuckCollisionRange();

    COMPILE_LOG_STATEMENT(if (collisionalPlayer->isAI()) {
        COMPILE_LOG("AnalyseCollision()/GPuck->mVelocity = (" + std::to_string(GPuck->mVelocity.x) + ", " + std::to_string(GPuck->mVelocity.y) + ")", ANALYSE_COLLISION_LOGS);
        COMPILE_LOG("AnalyseCollision()/GPuck->getPosition() = (" + std::to_string(GPuck->getPosition().x) + ", " + std::to_string(GPuck->getPosition().y) + ") REFERENCE ID 1", ANALYSE_COLLISION_LOGS);
        COMPILE_LOG("AnalyseCollision()/collisionalPlayer->getRacketsRange() = (" + std::to_string(collisionalPlayer->getRacketsRange().x) + ", " + std::to_string(collisionalPlayer->getRacketsRange().y) + ") REFERENCE ID 2", ANALYSE_COLLISION_LOGS);
        COMPILE_LOG("AnalyseCollision()/collisionalPlayer->mRacketSprite->getPosition() = (" + std::to_string(collisionalPlayer->mRacketSprite->getPosition().x) + ", " + std::to_string(collisionalPlayer->mRacketSprite->getPosition().y) + ") REFERENCE ID 2", ANALYSE_COLLISION_LOGS);

        AppConsts::BOUNCE_AREA aiActualBounceArea = collisionalPlayer->deduceBounceArea(GetYCoordinateOfPucksCollisionPointWithRacket(collisionalPlayer->getRacketsRange()));
        AppConsts::BOUNCE_AREA aiChosenBounceArea = std::get<0>(std::dynamic_pointer_cast<AIPlayer>(collisionalPlayer)->mChosenBounceAreaAndAbsRangeTuple);

        COMPILE_LOG("AnalyseCollision()/AI's actual bounce area = " + getStringifiedBounceArea(aiActualBounceArea) + " REFERENCE ID: BOUNCE_AREA", ANALYSE_COLLISION_LOGS);

        if (aiChosenBounceArea == aiActualBounceArea) {
            COMPILE_LOG("AI's chosen bounce area IS EQUAL to AI's actual bounce area +++++, BOUNCE_AREA = " + getStringifiedBounceArea(aiActualBounceArea) + " | REFERENCE ID: BOUNCE_AREA", ANALYSE_COLLISION_LOGS);
        }
        else {
            COMPILE_LOG("AI's chosen bounce area IS NOT EQUAL to AI's actual bounce area -----, aiChosenBounceArea = " + getStringifiedBounceArea(aiChosenBounceArea) + ", aiActualBounceArea = " + getStringifiedBounceArea(aiActualBounceArea) + " | REFERENCE ID: BOUNCE_AREA", ANALYSE_COLLISION_LOGS);
        }
    })

    if (pucksCollisionRange.y < racketRange.x || pucksCollisionRange.x > racketRange.y) {
        // There is goal. Reset speed modifier to 1.0 and pucks speed
        if (collisionalPlayer->isAI()) {
            COMPILE_LOG("AnalyseCollision()/AI Player lost goal!", ANALYSE_COLLISION_LOGS);
        }
        if (GPucksSpeedBounceModifier != 1.0f) {
            auto& pucksVel = GPuck->mVelocity;
            auto initialPucksVel = Vec2(pucksVel.x / GPucksSpeedBounceModifier, pucksVel.y / GPucksSpeedBounceModifier);
            pucksVel.x = initialPucksVel.x;
            pucksVel.y = initialPucksVel.y;
            GPucksSpeedBounceModifier = 1.0f;
        }
        otherPlayer->incrementScore();
        if (otherPlayer->hasWonGame()) {
            GMenuItemPlayAgainDynGamePhase->mMainMenuOrGameTypeToActivateOnClick = GCurrentGamePhase;
            GenerateRandomPositionAndAngle(SelectStartingPucksDirection());
            GCurrentGamePhase = GAME_PHASES::END_GAME_MENU;
            MainMenuOperations::DrawEndGameMenu();
            GPlayerOneRacket->mVelocity = Vec2(0.0f, 0.0f);
            GPlayerTwoRacket->mVelocity = Vec2(0.0f, 0.0f);
            GBackBuffer->present();
            return;
        }
        GShowScoreDeltaTime = 0.0f;
        GShowScore = true;
        GStopPuckAfterGoalDeltaTime = 0.0f;
        GStopPuckAfterGoal = true;
        PuckHelpers::GenerateRandomPositionAndAngle(collisionalPlayer->mSidePlayingOn);

        GAIEventsDispatcher->aiChangeStateToPositionResetAfterScoringOrLosingGoal(GPlayerOne);
        GAIEventsDispatcher->aiChangeStateToPositionResetAfterScoringOrLosingGoal(GPlayerTwo);

        return;
    }

    RunVelocityModifierAlgorithm(collisionalPlayer);
    RunBounceAngleModifierAlgorithm(collisionalPlayer, probableCollisionSide);

    GAIEventsDispatcher->aiChangeStateToDistanceOrDefenseAfterBounce(GPlayerOne);
    GAIEventsDispatcher->aiChangeStateToDistanceOrDefenseAfterBounce(GPlayerTwo);
}

const float PuckHelpers::CountVelocityVectorsLengthUsingPythagoras(const Vec2 velVec) {
    return sqrt((velVec.x * velVec.x) + (velVec.y * velVec.y));
}

void PuckHelpers::RunVelocityModifierAlgorithm(std::shared_ptr<Player>& collisionalPlayer) {
    auto speedBoostState = collisionalPlayer->mSpeedBoostState;
    auto isRacketMoving = collisionalPlayer->isRacketMoving();
    auto& pucksVel = GPuck->mVelocity;
    const auto pucksSettingSpeedVector = GetPucksSettingSpeedVector();

    float oldPucksSpeedBounceModifier = GPucksSpeedBounceModifier;
    GPucksSpeedBounceModifier = speedBoostState == SPEED_BOOST_STATES::ON ? 1.6f : (GPucksSpeedBounceModifier > 1.0f ? GPucksSpeedBounceModifier - 0.2f : 1.0f);
    Vec2 oldPucksVel = Vec2(pucksVel.x / oldPucksSpeedBounceModifier, pucksVel.y / oldPucksSpeedBounceModifier);

    pucksVel.x = oldPucksVel.x * GPucksSpeedBounceModifier;
    pucksVel.y = oldPucksVel.y * GPucksSpeedBounceModifier;
}

AppConsts::GAME_SETTINGS PuckHelpers::SelectStartingPucksDirection() {
    auto randomNumber0To1 = rand() % 2;

    if (randomNumber0To1 == 0) {
        return GAME_SETTINGS::SIDE_LEFT;
    }

    return GAME_SETTINGS::SIDE_RIGHT;
}

void PuckHelpers::GenerateRandomPositionAndAngle(AppConsts::GAME_SETTINGS pucksDirection) {
    int pucksAngle;
    GPuck->setPosition(Vec2(320.0f, (float)(rand() % 21 + 230)));
    auto pucksVelVectorLength = CountVelocityVectorsLengthUsingPythagoras();

    if (pucksDirection == GAME_SETTINGS::SIDE_LEFT) {
        pucksAngle = -(rand() % 11 + 175);
    }
    else {
        pucksAngle = rand() % 6;

        if (rand() % 2 == 0) {
            pucksAngle = -pucksAngle;
        }
    }

    GPuck->mVelocity = Vec2(pucksVelVectorLength * cos(pucksAngle * PI / 180.0), pucksVelVectorLength * sin(pucksAngle * PI / 180.0));
}

void PuckHelpers::RunBounceAngleModifierAlgorithm(std::shared_ptr<Player>& collisionalPlayer, AppConsts::COLLISION_SIDES probableCollisionSide) {
    const auto yCollisionCoordinate = GetYCoordinateOfPucksCollisionPointWithRacket(collisionalPlayer->getRacketsRange());
    auto& pucksVelVec = GPuck->mVelocity;

    switch (collisionalPlayer->deduceBounceArea(yCollisionCoordinate)) {
    case BOUNCE_AREA::FROM_0_TO_10:
        {
            const auto pucksVelVecLength = CountVelocityVectorsLengthUsingPythagoras();
            COMPILE_LOG("FROM_0_TO_10 BEFORE pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
            if (collisionalPlayer->mSidePlayingOn == GAME_SETTINGS::SIDE_RIGHT) {
                pucksVelVec.x = pucksVelVecLength * cos(-120.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(-120.0 * PI / 180.0);
            }
            else {
                pucksVelVec.x = pucksVelVecLength * cos(-60.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(-60.0 * PI / 180.0);
            }
            COMPILE_LOG("FROM_0_TO_10 AFTER pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()) + "\n", DEBUG_LOGS);
        }
        break;
    case BOUNCE_AREA::FROM_10_TO_20:
        {
            const auto pucksVelVecLength = CountVelocityVectorsLengthUsingPythagoras();
            COMPILE_LOG("FROM_10_TO_20 BEFORE pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
            if (collisionalPlayer->mSidePlayingOn == GAME_SETTINGS::SIDE_RIGHT) {
                pucksVelVec.x = pucksVelVecLength * cos(-140.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(-140.0 * PI / 180.0);
            }
            else {
                pucksVelVec.x = pucksVelVecLength * cos(-40.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(-40.0 * PI / 180.0);
            }
            COMPILE_LOG("FROM_10_TO_20 AFTER pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()) + "\n", DEBUG_LOGS);
        }

        break;
    case BOUNCE_AREA::FROM_20_TO_30:
        pucksVelVec.x = -pucksVelVec.x;
        COMPILE_LOG("FROM_20_TO_30 BEFORE pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
        switch (probableCollisionSide) {
        case COLLISION_SIDES::BOTTOM_AND_RIGHT:
        case COLLISION_SIDES::RIGHT:
        case COLLISION_SIDES::TOP_AND_LEFT:
            COMPILE_LOG("FROM_20_TO_30 while 1 pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
            {
                auto tempResultForX = (pucksVelVec.x * cos(10.0 * PI / 180.0)) - (pucksVelVec.y * sin(10.0 * PI / 180.0));
                pucksVelVec.y = (pucksVelVec.x * sin(10.0 * PI / 180.0)) + (pucksVelVec.y * cos(10.0 * PI / 180.0));
                pucksVelVec.x = tempResultForX;
            }
            break;
        case COLLISION_SIDES::TOP_AND_RIGHT:
        case COLLISION_SIDES::BOTTOM_AND_LEFT:
        case COLLISION_SIDES::LEFT:
            COMPILE_LOG("FROM_20_TO_30 while 2 pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
            {
                auto tempResultForX = (pucksVelVec.x * cos(-10.0 * PI / 180.0)) - (pucksVelVec.y * sin(-10.0 * PI / 180.0));
                pucksVelVec.y = (pucksVelVec.x * sin(-10.0 * PI / 180.0)) + (pucksVelVec.y * cos(-10.0 * PI / 180.0));
                pucksVelVec.x = tempResultForX;
            }
            break;
        }
        COMPILE_LOG("FROM_20_TO_30 AFTER pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()) + "\n", DEBUG_LOGS);
        break;
    case BOUNCE_AREA::FROM_30_TO_70:
        {
            COMPILE_LOG("FROM_30_TO_70 BEFORE pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
            const auto vecLength = PuckHelpers::CountVelocityVectorsLengthUsingPythagoras();
            pucksVelVec.y = 0.0f;
            pucksVelVec.x = pucksVelVec.x > 0.0f ? -vecLength : vecLength;
            COMPILE_LOG("FROM_30_TO_70 AFTER pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()) + "\n", DEBUG_LOGS);
        }
        break;
    case BOUNCE_AREA::FROM_70_TO_80:
        COMPILE_LOG("FROM_70_TO_80 BEFORE pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
        pucksVelVec.x = -pucksVelVec.x;

        switch (probableCollisionSide) {
        case COLLISION_SIDES::BOTTOM_AND_RIGHT:
        case COLLISION_SIDES::TOP_AND_LEFT:
        case COLLISION_SIDES::LEFT:
            {
                auto tempResultForX = (pucksVelVec.x * cos(10.0 * PI / 180.0)) - (pucksVelVec.y * sin(10.0 * PI / 180.0));
                pucksVelVec.y = (pucksVelVec.x * sin(10.0 * PI / 180.0)) + (pucksVelVec.y * cos(10.0 * PI / 180.0));
                pucksVelVec.x = tempResultForX;
            }
            break;
        case COLLISION_SIDES::TOP_AND_RIGHT:
        case COLLISION_SIDES::RIGHT:
        case COLLISION_SIDES::BOTTOM_AND_LEFT:
            {
                auto tempResultForX = (pucksVelVec.x * cos(-10.0 * PI / 180.0)) - (pucksVelVec.y * sin(-10.0 * PI / 180.0));
                pucksVelVec.y = (pucksVelVec.x * sin(-10.0 * PI / 180.0)) + (pucksVelVec.y * cos(-10.0 * PI / 180.0));
                pucksVelVec.x = tempResultForX;
            }
            break;
        }
        COMPILE_LOG("FROM_70_TO_80 AFTER pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()) + "\n", DEBUG_LOGS);
        break;
    case BOUNCE_AREA::FROM_80_TO_90:
        {
            COMPILE_LOG("FROM_80_TO_90 BEFORE pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
            const auto pucksVelVecLength = CountVelocityVectorsLengthUsingPythagoras();

            if (collisionalPlayer->mSidePlayingOn == GAME_SETTINGS::SIDE_RIGHT) {
                pucksVelVec.x = pucksVelVecLength * cos(140.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(140.0 * PI / 180.0);
            }
            else {
                pucksVelVec.x = pucksVelVecLength * cos(40.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(40.0 * PI / 180.0);
            }
            COMPILE_LOG("FROM_80_TO_90 AFTER pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()) + "\n", DEBUG_LOGS);
        }

        break;
    case BOUNCE_AREA::FROM_90_TO_100:
        {
            COMPILE_LOG("FROM_90_TO_100 BEFORE pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()), DEBUG_LOGS);
            const auto pucksVelVecLength = CountVelocityVectorsLengthUsingPythagoras();

            if (collisionalPlayer->mSidePlayingOn == GAME_SETTINGS::SIDE_RIGHT) {
                pucksVelVec.x = pucksVelVecLength * cos(120.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(120.0 * PI / 180.0);
            }
            else {
                pucksVelVec.x = pucksVelVecLength * cos(60.0 * PI / 180.0);
                pucksVelVec.y = pucksVelVecLength * sin(60.0 * PI / 180.0);
            }
            COMPILE_LOG("FROM_90_TO_100 AFTER pucksVelVecLength = " + std::to_string(CountVelocityVectorsLengthUsingPythagoras()) + "\n", DEBUG_LOGS);
        }

        break;
    case BOUNCE_AREA::NONE:
        break;
    }
}

const bool PuckHelpers::IsPuckMovingLeft() {
    if (GPuck->mVelocity.x < 0.0f) {
        return true;
    }

    return false;
}

const bool PuckHelpers::IsPuckMovingRight() {
    return !IsPuckMovingLeft();
}

void PuckHelpers::GetPredictionVec2OfNextCollision(Vec2& puckVel, Vec2& puckPos) {
    if (puckVel.x > 0) {
        if (puckVel.y > 0) {
            // bottom or right
            auto pxUntilRight = GRightBorderOfGameArea - puckPos.x;
            auto pxUntilBottom = GBottomBorderOfGameArea - puckPos.y;

            if (pxUntilRight / puckVel.x < pxUntilBottom / puckVel.y) {
                // COLLISION_SIDES::RIGHT will be first
                puckPos = Vec2(GRightBorderOfGameArea, -1.0f);
                // we don't need to modify abstract puckVel here
            }
            else {
                // COLLISION_SIDES::BOTTOM will be first
                // Here we update puckVel because we still will not know after this calculation where on left or right will it hit. We only know that it will hit top and bottom
                puckPos = Vec2(-1.0f, GBottomBorderOfGameArea);
                puckVel = Vec2(puckVel.x, -puckVel.y);
            }
            return;
        }
        else if (puckVel.y < 0) {
            // top or right
            auto pxUntilRight = GRightBorderOfGameArea - puckPos.x;
            auto pxUntilTop = puckPos.y - GTopBorderOfGameArea;

            if (pxUntilRight / puckVel.x < pxUntilTop / std::abs(puckVel.y)) {
                // COLLISION_SIDES::RIGHT will be first
                puckPos = Vec2(GRightBorderOfGameArea, -1.0f);
                // we don't need to modify abstract puckVel here
            }
            else {
                // COLLISION_SIDES::TOP will be first
                // Here we update puckVel because we still will not know after this calculation where on left or right will it hit. We only know that it will hit top and bottom
                puckPos = Vec2(-1.0f, GTopBorderOfGameArea);
                puckVel = Vec2(puckVel.x, -puckVel.y);
            }
            return;
        }

        puckPos = Vec2(GRightBorderOfGameArea, 0.0f); // COLLISION_SIDES::RIGHT
        // direct hit onto goal line, don't need to modify puckVel
        return;
    }
    else if (puckVel.x < 0) {
        if (puckVel.y > 0) {
            // bottom or left
            auto pxUntilLeft = puckPos.x - GLeftBorderOfGameArea;
            auto pxUntilBottom = GBottomBorderOfGameArea - puckPos.y;

            if (pxUntilLeft / std::abs(puckVel.x) < pxUntilBottom / puckVel.y) {
                // COLLISION_SIDES::LEFT will be first
                puckPos = Vec2(GLeftBorderOfGameArea, -1.0f);
                // we don't need to modify abstract puckVel because it is direct goal line hit now.
            }
            else {
                // COLLISION_SIDES::BOTTOM will be first
                // Here we update puckVel because we still will not know after this calculation where on left or right will it hit. We only know that it will hit top and bottom
                puckPos = Vec2(-1.0f, GBottomBorderOfGameArea);
                puckVel = Vec2(puckVel.x, -puckVel.y);
            }
            return;
        }
        else if (puckVel.y < 0) {
            // COLLISION_SIDES::TOP_AND_LEFT
            auto pxUntilLeft = puckPos.x - GLeftBorderOfGameArea;
            auto pxUntilTop = puckPos.y - GTopBorderOfGameArea;

            if (pxUntilLeft / std::abs(puckVel.x) < pxUntilTop / std::abs(puckVel.y)) {
                // COLLISION_SIDES::LEFT will be first
                puckPos = Vec2(GLeftBorderOfGameArea, -1.0f);
                // direct hit, don't need to modify puckVel
            }
            else {
                // COLLISION_SIDES::TOP will be first
                // Here we update puckVel because we still will not know after this calculation where on left or right will it hit. We only know that it will hit top and bottom
                puckPos = Vec2(-1.0f, GTopBorderOfGameArea);
                puckVel = Vec2(puckVel.x, -puckVel.y);
            }
            return;
        }

        puckPos = Vec2(GLeftBorderOfGameArea, 0.0f); // COLLISION_SIDES::LEFT;
        // don't need to modify puckVel - it is direct hit on the goal line.
        return;
    }

    if (puckVel.y > 0) {
        // COLLISION_SIDES::BOTTOM;
        puckPos = Vec2(0.0f, GBottomBorderOfGameArea);
        puckVel = Vec2(puckVel.x, -puckVel.y);
        return;
    }
    else if (puckVel.y < 0) {
        // COLLISION_SIDES::TOP;

        puckPos = Vec2(0.0f, GTopBorderOfGameArea);
        puckVel = Vec2(puckVel.x, -puckVel.y);
        return;
    }

    puckPos = Vec2(-1.0f, -1.0f); // COLLISION_SIDES::NONE; It means that puck is not moving
}
