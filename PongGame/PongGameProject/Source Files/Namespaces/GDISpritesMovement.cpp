#include "../../Header Files/Namespaces/GDISpritesMovement.h"

using namespace AppConsts;
using namespace AppInitializers;
using namespace PuckHelpers;

namespace GDISpritesMovement {
    void runRacketsMovementDetection(double dt) {
        using namespace AppInitializers;
        using namespace AppConsts;

        auto playerOneRacketsYPos = GPlayerOneRacket->getPosition().y;
        auto playerTwoRacketsYPos = GPlayerTwoRacket->getPosition().y;
        auto halfOfRacketsHeight = (float)GPlayerOneRacket->height() / 2;

        GPlayerOneRacket->setPositionsCoordinateY(playerOneRacketsYPos + dt * GPlayerOneRacket->mVelocity.y);
        GPlayerTwoRacket->setPositionsCoordinateY(playerTwoRacketsYPos + dt * GPlayerTwoRacket->mVelocity.y);

        if (playerOneRacketsYPos < halfOfRacketsHeight) {
            GPlayerOneRacket->setPositionsCoordinateY(halfOfRacketsHeight);
        }
        if (playerOneRacketsYPos > G_CLIENT_HEIGHT - halfOfRacketsHeight) {
            GPlayerOneRacket->setPositionsCoordinateY(G_CLIENT_HEIGHT - halfOfRacketsHeight);
        }

        if (playerTwoRacketsYPos < halfOfRacketsHeight) {
            GPlayerTwoRacket->setPositionsCoordinateY(halfOfRacketsHeight);
        }
        if (playerTwoRacketsYPos > G_CLIENT_HEIGHT - halfOfRacketsHeight) {
            GPlayerTwoRacket->setPositionsCoordinateY(G_CLIENT_HEIGHT - halfOfRacketsHeight);
        }

        GPlayerOneSpeedBoost->setPositionsCoordinateY(playerOneRacketsYPos);
        GPlayerTwoSpeedBoost->setPositionsCoordinateY(playerTwoRacketsYPos);
    }

	void runPucksCollisionDetection(double dt) {
        if (_initialCollisionCheckAndMoveOfThePuck(dt)) {
            return;
        }

		while (dt != 0.0) {
			AppConsts::COLLISION_SIDES collision = _probablePucksCollisionSides();
			_calculatePucksCollision(collision, dt);
		}
	}

    bool _initialCollisionCheckAndMoveOfThePuck(double dt) {
        using namespace AppInitializers;

        auto puckPos = GPuck->getPosition();
        auto& puckVel = GPuck->mVelocity;
        double distanceX = dt * puckVel.x;
        double distanceY = dt * puckVel.y;
        double posWithDistX = puckPos.x + distanceX;
        double posWithDistY = puckPos.y + distanceY;

        if (
            posWithDistX > GLeftBorderOfGameArea &&
            posWithDistX < GRightBorderOfGameArea &&
            posWithDistY > GTopBorderOfGameArea &&
            posWithDistY < GBottomBorderOfGameArea
        ) {
            GPuck->setPosition(Vec2(posWithDistX, posWithDistY));
            return true;
        }

        return false;
    }

	void _calculatePucksCollision(AppConsts::COLLISION_SIDES& collision, double& dt) {
		double colOneT = 0.0f;
		double colTwoT = 0.0f;
		double timeToCollision = 0.0f;
        auto puckPos = GPuck->getPosition();
		auto& puckVel = GPuck->mVelocity;

		switch (collision) {
		case COLLISION_SIDES::BOTTOM_AND_RIGHT:
			// what if puckVel.x or y is equal to 0 ? In this case it can't be. If it was it would landed in different case scenario.
			colOneT = ((double)GBottomBorderOfGameArea - puckPos.y) / puckVel.y; // bottom collision time
			colTwoT = ((double)GRightBorderOfGameArea - puckPos.x) / puckVel.x; // right collision time
			timeToCollision = (colOneT < colTwoT ? colOneT : colTwoT);

			if (dt > timeToCollision) {
				// it means that there will be collision
                auto isRightSideCollision = colOneT == colTwoT ? true : (colTwoT < colOneT ? true : false);
                dt -= timeToCollision;
                GPuck->setPosition(puckPos.x + (puckVel.x * timeToCollision), puckPos.y + (puckVel.y * timeToCollision));

                if (isRightSideCollision) {

                    AnalyseCollision(GPlayerTwo, GPlayerOne, collision);
                }
                else {
                    _reversePucksVelVec(colOneT, colTwoT);
                }

				break;
			}

            GPuck->setPosition(puckPos.x + (puckVel.x * dt), puckPos.y + (puckVel.y * dt));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::TOP_AND_RIGHT:
			colOneT = std::abs((puckPos.y - (double)GTopBorderOfGameArea) / puckVel.y); // top collision time
			colTwoT = ((double)GRightBorderOfGameArea - puckPos.x) / puckVel.x; // right collision time
			timeToCollision = (colOneT < colTwoT ? colOneT : colTwoT);
			
			if (dt > timeToCollision) {
                auto isRightSideCollision = colOneT == colTwoT ? true : (colTwoT < colOneT ? true : false);
				dt -= timeToCollision;
                GPuck->setPosition(puckPos.x + (puckVel.x * timeToCollision), puckPos.y - (std::abs(puckVel.y) * timeToCollision));

                if (isRightSideCollision) {

                    AnalyseCollision(GPlayerTwo, GPlayerOne, collision);
                }
                else {
                    _reversePucksVelVec(colOneT, colTwoT);
                }

				break;
			}
			
            GPuck->setPosition(puckPos.x + (puckVel.x * dt), puckPos.y - (std::abs(puckVel.y) * dt));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::RIGHT:
			colTwoT = ((double)GRightBorderOfGameArea - puckPos.x) / puckVel.x; // right collision time
			
			if (dt > colTwoT) {
				dt -= colTwoT;
                GPuck->setPositionsCoordinateX(puckPos.x + (puckVel.x * colTwoT));

                AnalyseCollision(GPlayerTwo, GPlayerOne, collision);
				break;
			}
			
            GPuck->setPositionsCoordinateX(puckPos.x + (puckVel.x * dt));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::BOTTOM_AND_LEFT:
			colOneT = ((double)GBottomBorderOfGameArea - puckPos.y) / puckVel.y; // bottom collision time
			colTwoT = (puckPos.x - (double)GLeftBorderOfGameArea) / std::abs(puckVel.x); // left collision time
			timeToCollision = (colOneT < colTwoT ? colOneT : colTwoT);

			if (dt > timeToCollision) {
                auto isLeftSideCollision = colOneT == colTwoT ? true : (colTwoT < colOneT ? true : false);
				dt -= timeToCollision;
                GPuck->setPosition(puckPos.x - (std::abs(puckVel.x) * timeToCollision), puckPos.y + (puckVel.y * timeToCollision));

                if (isLeftSideCollision) {

                    AnalyseCollision(GPlayerOne, GPlayerTwo, collision);
                }
                else {
                    _reversePucksVelVec(colOneT, colTwoT);
                }

				break;
			}
			
            GPuck->setPosition(puckPos.x - (std::abs(puckVel.x) * dt), puckPos.y + (puckVel.y * dt));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::TOP_AND_LEFT:
			colOneT = std::abs((puckPos.y - (double)GTopBorderOfGameArea) / puckVel.y); // top collision time
			colTwoT = (puckPos.x - (double)GLeftBorderOfGameArea) / std::abs(puckVel.x); // left collision time
			timeToCollision = (colOneT < colTwoT ? colOneT : colTwoT);

			if (dt > timeToCollision) {
                auto isLeftSideCollision = colOneT == colTwoT ? true : (colTwoT < colOneT ? true : false);
				dt -= timeToCollision;
                GPuck->setPosition(puckPos.x - (std::abs(puckVel.x) * timeToCollision), puckPos.y - (std::abs(puckVel.y) * timeToCollision));

                if (isLeftSideCollision) {

                    AnalyseCollision(GPlayerOne, GPlayerTwo, collision);
                }
                else {
                    _reversePucksVelVec(colOneT, colTwoT);
                }

				
				break;
			}

            GPuck->setPosition(puckPos.x - (std::abs(puckVel.x) * dt), puckPos.y - (std::abs(puckVel.y) * dt));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::LEFT:
			colTwoT = (puckPos.x - (double)GLeftBorderOfGameArea) / std::abs(puckVel.x); // left collision time

			if (dt > colTwoT) {
				dt -= colTwoT;
                GPuck->setPositionsCoordinateX(puckPos.x - (std::abs(puckVel.x) * colTwoT));

                AnalyseCollision(GPlayerOne, GPlayerTwo, collision);
				break;
			}

            GPuck->setPositionsCoordinateX(puckPos.x - (std::abs(puckVel.x) * dt));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::BOTTOM:
			colOneT = ((double)GBottomBorderOfGameArea - puckPos.y) / puckVel.y; // bottom collision time

			if (dt > colOneT) {
				dt -= colOneT;
                GPuck->setPositionsCoordinateY(puckPos.y + (puckVel.y * colOneT));
				puckVel.y = -puckVel.y;
				break;
			}

            GPuck->setPositionsCoordinateY(puckPos.y + (puckVel.y * colOneT));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::TOP:
			colOneT = std::abs((puckPos.y - (double)GTopBorderOfGameArea) / puckVel.y); // top collision time

			if (dt > colOneT) {
				dt -= colOneT;
                GPuck->setPositionsCoordinateY(puckPos.y - (std::abs(puckVel.y) * colOneT));
				puckVel.y = -puckVel.y;
				break;
			}

            GPuck->setPositionsCoordinateY(puckPos.y - (std::abs(puckVel.y) * dt));
			dt = 0.0f;
			break;
		case COLLISION_SIDES::NONE:
			// if predicted COLLISION_SIDES is equal to NONE then it means that ball is not moving at all
			break;
		}
	}

	void _reversePucksVelVec(double& colOneT, double& colTwoT) {
		using namespace AppInitializers;
		if (colOneT < colTwoT) {
			GPuck->mVelocity.y = -GPuck->mVelocity.y;
			return;
		}
		else if (colOneT > colTwoT) {
			GPuck->mVelocity.x = -GPuck->mVelocity.x;
			return;
		}

		GPuck->mVelocity.y = -GPuck->mVelocity.y;
		GPuck->mVelocity.x = -GPuck->mVelocity.x;
		return;
	}

	AppConsts::COLLISION_SIDES _probablePucksCollisionSides() {
		using namespace AppInitializers;
		using namespace AppConsts;

		Vec2& puckVel = GPuck->mVelocity;

		if (puckVel.x > 0) {
			if (puckVel.y > 0) {
				return COLLISION_SIDES::BOTTOM_AND_RIGHT;
			}
			else if (puckVel.y < 0) {
				return COLLISION_SIDES::TOP_AND_RIGHT;
			}

			return COLLISION_SIDES::RIGHT;
		}
		else if (puckVel.x < 0) {
			if (puckVel.y > 0) {
				return COLLISION_SIDES::BOTTOM_AND_LEFT;
			}
			else if (puckVel.y < 0) {
				return COLLISION_SIDES::TOP_AND_LEFT;
			}

			return COLLISION_SIDES::LEFT;
		}

		if (puckVel.y > 0) {
			return COLLISION_SIDES::BOTTOM;
		}
		else if (puckVel.y < 0) {
			return COLLISION_SIDES::TOP;
		}

		return COLLISION_SIDES::NONE;
	}
}
