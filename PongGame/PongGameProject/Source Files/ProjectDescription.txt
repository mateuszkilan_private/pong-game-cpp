For the sake of speed of test development I had to change some default behaviour of some functions/methods. You can find the changed lines by searching the solution for "CHANGED_FOR_TESTS" phrase.

Needed classes:
1. Sprite (gPuck, gPaddlePlayer1, gPaddlePlayer2)
2. Backbuffer (to fix overdraw effect)
3. Vec2 (for Sprite class as dependecy)
4. HumanPlayer
5. AIPlayer

Needed namespaces:
1. AppConsts - here we hold application constants
2. AppInitializers - most important variables/instances that we create at the start of the program. Later those "initializers" are changeable
3. GDISpritesMovement - this namespace will include all methods needed by puck sprite to perform proper collision detection
4. GameController - here we will hold all logic that will be responsible for rendering menu and all of its options

Development dependencies:
- Test namespace - this is my personal testing framework, it allows us to make unit tests.

User story:
1. I turn on the Pong Game
2. I can see menu with four options:
a) New Game
	aa) Human(P1) vs AI
		Choose AI difficulty (radio buttons):
		aaa) Easy
		aab) Medium
		aac) Hard
		Choose side you want to play on (radio buttons):
		aad) Left
		aae) Right
	ab)	Human(P1) vs Human(P2)
		GAME STARTS
	ac)	AI vs AI
		GAME STARTS
b) Options
	ba) Game speed (radio buttons):
		baa) Slow
		bab) Normal
		bac) Fast (default value)
c) Credits
	Game was created by passionate developer Mateusz Kilan blah blah blah...
d) Exit Game
	GAME QUITS

Menu items placing (in pixels):
When we talk about placing particular asset in the window we placing it by providing its center point, also I will provide here all assets width and height:

Common bitmaps:
	a. BlackBackground.bmp - 640 x 480 - Vc(0, 0), Vm(320, 240)
	b. MenuCopyrights.bmp - 224 x 21 - Vc(208, 406), Vm(320, 417)
	c. MenuFrame.bmp - 640 x 480 - Vc(0, 0), Vm(320, 240)
	d. ThePongGameLogo.bmp - 250 x 70 - Vc(195, 51), Vm(320, 86)
	e. MenuItemBack.bmp & MenuItemBackActive.bmp - 227 x 30 - Vc(207, 176), Vm(321, 191)
    f. F1HowToPlayHint.bmp - 133 x 18 - Vc(254, 126), Vm(321, 135)

1. Main Menu View
	a. MenuItemNewGame.bmp & MenuItemNewGameActive.bmp - 227 x 30 - Vc(207, 176), Vm(321, 191)
	b. MenuItemOptions.bmp & MenuItemOptionsActive.bmp - 227 x 30 - Vc(207, 220), Vm(321, 235)
	c. MenuItemCredits.bmp & MenuItemCreditsActive.bmp - 227 x 30 - Vc(207, 264), Vm(321, 279)
	d. MenuItemExitGame.bmp & MenuItemExitGameActive.bmp - 227 x 30 - Vc(207, 307), Vm(321, 322)

1.1 New Game View
	a. MenuItemAIvsAI.bmp & MenuItemAIvsAIActive.bmp - 227 x 30 - Vc(207, 307), Vm(321, 322)
	b. MenuItemP1vsAI.bmp & MenuItemP1vsAIActive.bmp - 227 x 30 - Vc(207, 220), Vm(321, 235)
	c. MenuItemP1vsP2.bmp & MenuItemP1vsP2.bmp - 227 x 30 - Vc(207, 264), Vm(321, 279)

1.1.1 AI Difficulty View
	a. MenuItemDifficulty.bmp - 227 x 30 - Vc(207, 220), Vm(321, 235)
	b. MenuItemEasy.bmp & MenuItemEasyActive.bmp - 227 x 30 - Vc(207, 264), Vm(321, 279)
	c. MenuItemMedium.bmp & MenuItemMediumActive.bmp - 227 x 30 - Vc(207, 307), Vm(321, 322)
	d. MenuItemHard.bmp & MenuItemHardActive.bmp - 227 x 30 - Vc(207, 350), Vm(321, 365)

1.1.2 Choose Side View
	a. MenuItemChooseSide.bmp - 227 x 30 - Vc(207, 220), Vm(321, 235)
	b. MenuItemLeft & MenuItemLeftActive- 227 x 30 - Vc(207, 264), Vm(321, 279)
	c. MenuItemRight & MenuItemRightActive - 227 x 30 - Vc(207, 307), Vm(321, 322)

1.2 Options View
	a. MenuItemGameSpeed.bmp - 227 x 30 - Vc(207, 220), Vm(321, 235)
	b. MenuItemFast.bmp & MenuItemFastActive.bmp - 227 x 30 - Vc(207, 264), Vm(321, 279)
	c. MenuItemNormal.bmp & MenuItemNormalActive.bmp - 227 x 30 - Vc(207, 307), Vm(321, 322)
	d. MenuItemSlow.bmp & MenuItemSlowActive.bmp - 227 x 30 - Vc(207, 350), Vm(321, 365)

1.3 Credits View
	a. CreditsText.bmp - 545 x 126 - Vc(55, 206), Vm(328, 269)
	b. MenuItemBack.bmp & MenuItemBackActive.bmp - 227 x 30 - Vc(207, 160), Vm(321, 175)

1.4 Gameplay View
	a. HashLine.bmp - 2 x 480 - Vc(319, 0), Vm(320, 240)
	b. Ball.bmp - 15 x 15 - Movable object, Vc and Vm will be dinamically generated.
	c. Racket.bmp - 15 x 100 - Movable object:
		For P1 - Vc(0, 190), Vm(8, 240)
		For P2 - Vc(625, 190), Vm(633, 240)
	d. SpeedBoostPlusSign.bmp - 8 x 8 - Movable object:
		For P1 - Vc(4, 236), Vm(8, 240)
		For P2 - Vc(629, 236), Vm(633, 240)

1.5 HowToPlay View
	a. MenuHowToPlay.bmp - 640 x 480 - Vc(0, 0), Vm(320, 240)

1.6 End Game Menu View
    a. FinalScore.bmp - 200 x 35 - Vc(), Vm(320, 72)
    b. AIWon.bmp - 87 x 24 - Vc(), Vm(320, 173)
    c. P1Won.bmp - 94 x 24 - Vc(), Vm(320, 173)
    d. P2Won.bmp - 94 x 24 - Vc(), Vm(320, 173)
    e. ${any single digit number}.bmp - 29 x 58 - Vc(), Vm(280, 125) - for left side, Vm(357, 125) - for right side
    f. ${any multi digits number}.bmp - 70 x 58 - Vc(), Vm(261, 125) - for left side, Vm(378, 125) - for rightside
    g. Colon.bmp - 8 x 25 - Vc(), Vm(319, 124)
    h. MenuItemPlayAgain.bmp & MenuItemPlayAgainActive.bmp - 227 x 30 - Vc(), Vm(320, 235)
    i. MenuItemOptions.bmp & MenuItemOptionsActive.bmp - 227 x 30 - Vc(), Vm(320, 279)
    j. MenuItemMainMenu.bmp & MenuItemMainMenuActive.bmp - 227 x 30 - Vc(), Vm(320, 322)
    k. MenuItemExitGame.bmp & MenuItemExitGameActive.bmp - 227 x 30 - Vc(), Vm(320, 366)

1.7 Pause Game Menu View
    a. CurrentScore.bmp - 242 x 35 - Vc(), Vm(319, 72)
    b. MenuItemContinue.bmp & MenuItemContinueActive.bmp - 227 x 30 - Vc(), Vm(320, 191)
    c. MenuItemRestart.bmp & MenuItemRestartActive.bmp - 227 x 30 - Vc(), Vm(320, 235)
    d. MenuItemOptions.bmp & MenuItemOptionsActive.bmp - 227 x 30 - Vc(), Vm(320, 279)
    e. MenuItemMainMenu.bmp & MenuItemMainMenuActive.bmp - 227 x 30 - Vc(), Vm(320, 322)
    f. MenuItemExitGame.bmp & MenuItemExitGameActive.bmp - 227 x 30 - Vc(), Vm(320, 366)

MENU ITEMS AND SETTING MENU ITEMS MOVE/CLICK BEHAVIOUR DESCRIPTION:

Behaviour of menu items:
1. No menu item is active and I'm moving mouse beyond and menu items.
- Nothing should happen.
2. No menu item is active, I'm moving mouse over menu item.
- This menu item that I moved my mouse over to should be activated.
3. Menu item is active and I'm moving mouse beyond any menu items.
- Active menu item should become deactivated.
4. Menu item is active and I move mouse over to different menu item.
- Currently active menu item should be deactivated. New menu item that mouse is hovering over should be activated.
5. Menu item is active and I'm moving mouse over this menu item.
- Nothing should happen.

Specific behaviour of setting menu item:
1. When I enter screen where option should be always set, then that option's default setting menu item should be automatically activated.
2. If I move mouse over active setting menu item, then this item should be kept active.
3. If I move mouse over different setting menu item (which is not chosen) then both old and new setting menu items should be visible as active.
4. If I move mouse over different setting menu item (which is not chosen) and click it, then this new setting should be chosen and activated, and old one should be deactivated.

Player, AIPlayer and HumanPlayer classes description:
What Player instance object needs to have:
- His racket sprite
- His speed boost sprite
- speed boost ready state
- side he is playing on
What Human instance object needs to have:
- he needs to run only Player instance constructor (on this step of project)
What AIPlayer needs to have:
- difficulty level
- functions to calculate movement (not sure of this yet though)

ALGORYTM - DETEKCJA KOLIZJI:
- Projekt do liczenia czasu musi uzywac performance timer'a
- Detekcja kolizji musi by� bardziej wydajna i zu�ywa� procesor w mniejszym stopniu ni� moja poprzednia implementacja detekcji kolizji
- Je�eli kr��ek dotknie �ciany za paletk� wtedy gracz po przeciwnej stronie pola gry dostaje punkt a pozycja kr��ka jest resetowana i kierunek wektora pr�dko�ci jest losowany.
  Przy czym zakres losowania musi gwarantowa� odbicie si� od jednej albo drugiej paletki. Czyli wsp�rz�dn� "y" nie mo�e by� r�wna 0.
- Detekcja kolizji musi bra� pod uwag� fakt, �e kr��ek odbija� si� b�dzie wy��cznie od paletki graczy i od �ciany g�rnej i dolnej. 

Przy czym dodatkowo detekcja kolizji musi wzi�� pod uwag� dwa aspekty:
    a) Pr�dko�� paletki:
        Paletki b�d� posiada�y dwa stany pr�dko�ci:
        - Pr�dko�� normalna wynikaj�ca z wci�ni�cia przycisku ruchu w g�r� lub w d�:
          * W tym przypadku podczas odbicia kr��ka od paletki w ruchu NIE ULEGA dodatniej modyfikacji pr�dko�� kr��ka.
          * Je�eli lec�cy do danego gracza kr��ek by� odbity od paletki w stanie speed boost'a albo posiada� jeszcze zmodyfikowan� ponad 1.0 warto�� pr�dko�ci (1.1/1.2/1.3) to podczas odbijania tego kr��ka przez gracza, kt�rego paletka porusza si� z normaln� pr�dko�ci�, zmniejszeniu ulega zmodyfikowana warto�� pr�dko�ci kr��ka o 0.1, minimalnie warto�� ta mo�e zmniejszy� si� do wyj�ciowej pr�dko�ci kr��ka czyli modyfikatora 1.0.
          * Je�eli paletka gracza NIE BY�A w stanie speed boost i paletka nie by�a w ruchu w trakcie odbijania kr��ka i kr��ek posiada� warto�� modyfikatora pr�dko�ci 1.1/1.2/1.3
            to warto�� modyfikatora pr�dko�ci po odbiciu kr��ka ulegnie zmniejszeniu o 0.1 do minimalnie warto�ci bazowej r�wnej 1.0.
        - Pr�dko�� speed boost'owa przyspieszaj�ca ruch paletki o dany % na okre�lon� ilo�� czasu
          * Je�eli do danego gracza leci kr��ek o modyfikatorze pr�dko�ci 1.0 (czyli bazowej)/1.1/1.2/1.3 i dany gracz w��czy stan speed boost i odbije kr��ek b�d�c w ruchu to
            pr�dko�� ruchu kr��ka ulegnie zmodyfikowaniu do 1.3 (je�eli wcze�niej by� r�wny 1.3 to dalej b�dzie r�wny 1.3).
          * Je�eli paletka gracza by�a w stanie speed boost ale paletka nie by�a w ruchu w trakcie odbijania kr��ka i kr��ek posiada� warto�� modyfikatora pr�dko�ci 1.1/1.2/1.3
            to warto�� modyfikatora pr�dko�ci po odbiciu kr��ka ulegnie zmniejszeniu o 0.1 do minimalnie warto�ci bazowej r�wnej 1.0.

    b) Miejsce uderzenia kr��ka w paletk�:
        - Podczas obicia od paletki detekcja b�dzie rozpoznawa� w kt�rym miejscu paletki kr��ek si� odbija, program wykona algorytm MKOKOP.
        - Je�eli �rodek kr��ka uderzy idealnie w �rodek paletki to wtedy modyfikator k�ta odbicia odwr�ci wsp�rz�dn� "x" wektora pr�dko�ci, znak wsp�rz�dnej "y" wektora pr�dko�ci si� nie zmieni.
        - Im odbicie b�dzie mia�o miejsce bli�ej kraw�dzi g�rnej i dolnej paletki tym wi�kszy b�dzie modyfikator k�ta odbicia.

    c) Odbijanie si� od �ciany g�rnej i dolnej zawsze odwraca znak wsp�rz�dnej "y" wektora pr�dko�ci, wsp�lrz�dna "x" pozostaje bez zmian. Wa�ne jest to, �e podczas odbijania od �ciany g�rnej lub dolnej nie zmienia si� modyfikator wektora pr�dko�ci.

ALGORYTM - MODYFIKACJA K�TA ODBICIA KR��KA OD PALETKI (MKOKOP):
W zale�o�ci od tego w kt�rym miejscu kr��ek odbije si� od paletki innemu zmodyfikowaniu ulegnie k�t odbicia kr��ka.
Opis dzia�ania algorytmu:
1) Najpierw trzeba obliczy� obszar kolizyjny kr��ka z paletk� gracza. Ten obszar jest r�wny wysoko�ci kr��ka , kt�ra mo�emy zmapowa� na paletk�.
2) Nast�pnie znaj�c ten obszar dzielimy go przez 2 i poznajemy wspolrzedna "y" punktu kolizyjnego z paletk� danego gracza. Wspolrzedna "x" nie jest nam potrzebna.
   Moze zaistniec koniecznosc zaokr�glenia tej wartosci w gore albo w dol, to czy bedzie to zaokr�glone w gore czy w dol zalezy od tego, czy po zaokr�gleniu wspolrzedna "y" da sie zmapowac na paletk�.
3) Nast�pnie mapujemy wsp�rz�dn� y tego punktu na paletke, w tym momencie wiemy w ktorym miejscu si� dokladnie odbija kr��ek od paletki. Ten punkt y jest wspo�rz�dna y podpunktu kolizyjnego.
4) Nast�pnie przeliczamy k�t odbicia kr��ka od paletki:
    - 0 - 10 - Odwracamy wektor pr�dko�ci w taki sposob aby kr��ek porusza� si� zawsze w g�r� i w prawo lub lewo.
      Aby obliczy� wektor po odbiciu musimy znac jego d�ugo��. Jako , �e dost�pne s� 3 pr�dko�ci i mog� by� modyfikowane poprzez algorytm MPK to konieczne b�dzie dynamiczne
      przeliczanie d�ugo�ci wektora "L".
      Maj�c d�ugo�� "L" i znaj�c k�t o jaki chcemy obr�ci� wektor wzgl�dem osi X jeste�my w stanie obliczy� wsp�rz�dne tego obr�conego wektora.
      Dla paletki prawego gracza:
      x' = L * cos(-120)
      y' = L * sin(-120)
      Dla paletki lewego gracza:
      x' = L * cos(-60)
      y' = L * sin(-60)
      W tym przypadku k�t bazowy wynosi 0 stopni.
    - 10 - 20 - Odwracamy wektor pr�dko�ci w taki sposob aby kr��ek porusza� si� zawsze w g�r� i w prawo lub lewo.
      Aby obliczy� wektor po odbiciu musimy znac jego d�ugo��. Jako , �e dost�pne s� 3 pr�dko�ci i mog� by� modyfikowane poprzez algorytm MPK to konieczne b�dzie dynamiczne
      przeliczanie d�ugo�ci wektora "L". D�ugo�� wektora L liczymy ze wzoru Pitagorasa.
      Maj�c d�ugo�� "L" i znaj�c k�t o jaki chcemy obr�ci� wektor wzgl�dem osi X jeste�my w stanie obliczy� wsp�rz�dne tego obr�conego wektora.
      Dla paletki prawego gracza:
      x' = L * cos(-140)
      y' = L * sin(-140)
      Dla paletki lewego gracza:
      x' = L * cos(-40)
      y' = L * sin(-40)
      W tym przypadku k�t bazowy wynosi 0 stopni.
    - 20 - 30 - Negujemy wspolrzedna "x" i stosujemy wzor funkcji f(x) = -|x| do obliczenia wspolrzednej "y".
      Nast�pnie:
      Paletka gracza prawego:
      1) Je�eli wektor odbije si� od �ciany BOTTOM_RIGHT - Obliczamy jego obr�t o 10 stopni
      2) Je�eli wektor odbije si� od �ciany TOP_RIGHT - Obliczamy jego obrot o -10 stopni
      3) Je�eli wektor odbije si� od �ciany RIGHT - Obliczamy jego obrot o 10 stopni (uwaga na znak)
      Paletka gracza lewego:
      1) Je�eli wektor odbije si� od �ciany BOTTOM_LEFT - Obliczamy jego obr�t o -10 stopni
      2) Je�eli wektor odbije si� od �ciany TOP_LEFT - Obliczamy jego obr�t o 10 stopni
      3) Je�eli wektor odbije si� od �ciany LEFT - Obliczamy jego obr�t o -10 stopni (uwaga na znak)
    - 30 - 70 - Negujemy wspolrzedna "x" i stosujemy wzor funkcji f(x) = -|x| do obliczenia wspolrzednej "y"
    - 70 - 80 - Negujemy wspolrzedna "x" i stosujemy wzor funkcji f(x) = -|x| do obliczenia wspolrzednej "y".
      Nast�pnie:
      Paletka gracza prawego:
      1) Je�eli wektor odbije si� od �ciany BOTTOM_RIGHT - Obliczamy jego obr�t o 10 stopni
      2) Je�eli wektor odbije si� od �ciany TOP_RIGHT - Obliczamy jego obrot o -10 stopni
      3) Je�eli wektor odbije si� od �ciany RIGHT - Obliczamy jego obrot o -10 stopni (uwaga na znak)
      Paletka gracza lewego:
      1) Je�eli wektor odbije si� od �ciany BOTTOM_LEFT - Obliczamy jego obr�t o -10 stopni
      2) Je�eli wektor odbije si� od �ciany TOP_LEFT - Obliczamy jego obr�t o 10 stopni
      3) Je�eli wektor odbije si� od �ciany LEFT - Obliczamy jego obr�t o 10 stopni (uwaga na znak)
    - 80 - 90 - Odwracamy wektor pr�dko�ci w taki sposob aby kr��ek porusza� si� zawsze w g�r� i w prawo lub lewo.
      Aby obliczy� wektor po odbiciu musimy znac jego d�ugo��. Jako , �e dost�pne s� 3 pr�dko�ci i mog� by� modyfikowane poprzez algorytm MPK to konieczne b�dzie dynamiczne
      przeliczanie d�ugo�ci wektora "L". D�ugo�� wektora L liczby ze wzoru Pitagorasa.
      Maj�c d�ugo�� "L" i znaj�c k�t o jaki chcemy obr�ci� wektor wzgl�dem osi X jeste�my w stanie obliczy� wsp�rz�dne tego obr�conego wektora.
      Dla paletki prawego gracza:
      x' = L * cos(140)
      y' = L * sin(140)
      Dla paletki lewego gracza:
      x' = L * cos(40)
      y' = L * sin(40)
      W tym przypadku k�t bazowy wynosi 0 stopni.
    - 90 - 100 - Odwracamy wektor pr�dko�ci w taki sposob aby kr��ek porusza� si� zawsze w g�r� i w prawo lub lewo.
      Aby obliczy� wektor po odbiciu musimy znac jego d�ugo��. Jako , �e dost�pne s� 3 pr�dko�ci i mog� by� modyfikowane poprzez algorytm MPK to konieczne b�dzie dynamiczne
      przeliczanie d�ugo�ci wektora "L".
      Maj�c d�ugo�� "L" i znaj�c k�t o jaki chcemy obr�ci� wektor wzgl�dem osi X jeste�my w stanie obliczy� wsp�rz�dne tego obr�conego wektora.
      Dla paletki prawego gracza:
      x' = L * cos(120)
      y' = L * sin(120)
      Dla paletki lewego gracza:
      x' = L * cos(60)
      y' = L * sin(60)
      W tym przypadku k�t bazowy wynosi 0 stopni.

ALGORYTM - MODYFIKACJA PR�DKO�CI KR��KA (MPK):
Wyj�ciowy modyfikator pr�dko�ci kr��ka wynosi 1.0. Warto�� ta mo�e ulec zmianie podczas:
- Odbicia kr��ka przez gracza, kt�rego paletka jest w stanie speed boost i si� porusza, w takiej sytuacji po odbiciu modyfikator b�dzie wynosi� 1.6.
- Odbicia kr��ka przez gracza, kt�rego paletka jest w stanie speed boost lub nie jest w stanie speed boost i dodatkowo si� nie porusza. W takiej sytuacji je�eli modyfikator odbijanego kr��ka jest r�wny 1.2/1.4/1.6 to ulegnie zmniejszeniu o 0.2 do minimalnej warto�ci bazowej 1.0.
- Odbicia kr��ka przez gracza, kt�rego paletka nie jest w stanie speed boost i si� porusza. W takiej sytuacji je�eli modyfikator odbijanego kr��ka jest r�wny 1.2/1.4/1.6 to ulegnie zmniejszeniu o 0.2 do minimalnej warto�ci bazowej (1.0).

EMKK - efektywne miejsce kolizji kr��ka

IMPLEMENTACJA ALGORYTMU DETEKCJI KOLIZJI

1. Kr��ek na pocz�tku posiada dwa wektory:
- Wektor pozycji - PosVec
- Wektor pr�dko�ci, kierunku i zwrotu - VelVec

Czas przestoju pomi�dzy wygenerowanymi klatkami nazywamy - szok - czasem przestoju, w skr�cie CP.

Zasada dzia�ania algorytmu:
- �ciana g�rna: 0px, o� Y
- �ciana dolna: 480px, o� Y
- �ciana lewa: 0px, o� X
- �ciana prawa: 640px, o� X

1. Obliczamy distanceX i distanceY. distanceX = CP * MPK * VelVec.x i distanceY = CP * MPK * VelVec.y
2. Nast�pnie bierzemy PosVec.x i dodajemy distanceX i PosVec.y i dodajemy distanceY:
    2.1 Je�eli wynik PosVec.x + distanceX jest zawarty w zakresie (0px ; 640px), to oznacza to �e nie dojdzie do odbicia ani od �ciany lewej ani od �ciany prawej i je�eli wynik PosVec.y + distanceY jest zawarty w zakresie (0px ; 480px), to oznacza to �e w CP nie dojdzie do odbicia ani od �ciany g�rnej ani od �ciany dolnej.
        - Modyfikujemy PosVec.x, PosVec.x += distanceX
        - Modyfikujemy PosVec.y, PosVec.y += distanceY
        ko�czymy detekcj� kolizji.
3. Prawid�owa detekcja kolizji:
    P�tla - je�eli CP > 0.0 to:
        3.1 Najpierw patrzymy na wektor pr�dko�ci aby ustali� prawdopodobn� kolizj� ze �cian�:
            - VelVec.x > 0 && VelVec.y > 0 => BOTTOM_AND_RIGHT
            - VelVec.x > 0 && VelVec.y < 0 => TOP_AND_RIGHT
            - VelVec.x > 0 && VelVec.y == 0 => RIGHT
            - VelVec.x < 0 && VelVec.y == 0 => LEFT
            - VelVec.y > 0 && VelVec.x < 0 => BOTTOM_AND_LEFT
            - VelVec.y < 0 && VelVec.x < 0 => TOP_AND_LEFT
            - VelVec.y > 0 && VelVec.x == 0 => BOTTOM
            - VelVec.y < 0 && VelVec.x == 0 => TOP
            - VelVec.y == 0 && VelVec.x == 0 => NONE
        3.2 Nast�pnie przechodzimy do funkcji przeliczaj�cej kolizj�:
            Je�eli w 3.1 wysz�o nam:
            3.2.1 {A}_AND_{B}:
                - Liczymy czas do kolizji z lini� �ciany g�rnej lub dolnej - A i z lini� paletki lewego gracza lub lini� paletki prawego gracza - B
                - Wybieramy ten czas, kt�ry jest mniejszy (je�eli s� takie same to oznacza to, �e dojdzie do kolizji z obydwoma liniami naraz, czyli uderzy w r�g)
                - Jezeli wybrany czas jest wiekszy od CP to dojdzie do kolizji , w przeciwnym wypadku odpalamy funkcje "_changePosition" albo liczymy to recznie i wychodzimy z funkcji
                - O ten wybrany czas modyfikujemy PosVec.x i PosVec.y, w tym momencie mamy kr��ek w pozycji kolizyjnej z dan� �cian� lub �cianami
                - Je�eli dojdzie do kolizji to musimy sprawdzi� czy EMKK znajduje si� na paletce odpowiedniego gracza:
                        a) Je�eli nie, to znaczy, �e gracz odbijaj�cy traci punkt na rzecz przeciwnika. Pozycja pi�ki jest resetowana a licznik wyniku odpowiednio inkrementowany.
                        b) Je�eli tak, to znaczy, �e graczowi uda�o odbi� si� kr��ek. W tym momencie wykonywany jest algorytm MPK okre�laj�cy modyfikator pr�dko�� kr��ka po odbiciu, jak r�wnie� wykonywany jest algorytm MKOKOP do okre�lenia modyfikacji k�ta odbicia kr��ka od paletki.
                - Nast�pnie odwracamy odpowiednio znaki VelVec w zaleznosci od ktorej linii si� odbije
                - Od CP odejmujemy wybrany czas.
                - wychodzimy ze switcha i wracamy do nadrz�dnej p�tli sprawdzaj�cej warto�� CP
            3.2.2 {A}:
                - Liczymy czas do kolizji z lini� �ciany g�rnej, dolnej lub lini� paletki gracza prawego lub lewego - A
                - Jezeli ten czas jest wiekszy od CP to dojdzie do kolizji , w przeciwnym wypadku odpalamy funkcje "_changePosition" albo liczymy to recznie i wychodzimy z funkcji
                - O ten czas modyfikujemy odpowiedni punkt PosVec, w tym momencie mamy kr��ek w pozycji kolizyjnej z dan� �cian�
                - Je�eli dojdzie do kolizji i dojdzie do kolizji z lini� paletki kt�rego� z graczy to musimy sprawdzi� czy EMKK znajduje si� na paletce odpowiedniego gracza:
                        a) Je�eli nie, to znaczy, �e gracz odbijaj�cy traci punkt na rzecz przeciwnika. Pozycja pi�ki jest resetowana a licznik wyniku odpowiednio inkrementowany.
                        b) Je�eli tak, to znaczy, �e graczowi uda�o odbi� si� kr��ek. W tym momencie wykonywany jest algorytm MPK okre�laj�cy modyfikator pr�dko�� kr��ka po odbiciu, jak r�wnie� wykonywany jest algorytm MKOKOP do okre�lenia modyfikacji k�ta odbicia kr��ka od paletki.
                - Odwracamy znak odpowiedniego punktu VelVec
                - Od CP odejmujemy wybrany czas.
                - wychodzimy ze switcha i wracamy do nadrz�dnej p�tli sprawdzaj�cej warto�� CP
            3.2.3 NONE:
                - Je�eli wybrany zostanie NONE to oznacza to, �e kr��ek stoi w miejscu
                - Zerujemy CP
                - Wychodzimy ze switcha i wracamy do nadrzednej p�tli sprawdzaj�cej warto�� CP

ALGORYTM LOSOWANIA POZYCJI I KIERUNKU KR��KA (LPKK)

Je�eli rozpoczynana jest nowa gra to wtedy losowo wybierany jest gracz na kt�rego b�dzie pocz�tkowo lecia� kr��ek.
Wsp�rz�dna x pozycji startowej kr��ka jest sta�a i jest r�wna samemu �rodkowi pola gry czyli 320px (oczywi�cie z perspektywy wsp�rz�dnej x).
Wsp�rz�dna y pozycji startowej kr��ka jest losowana. Mo�e ona by� r�wna od 230px do 250px, na tych pikselach mo�e znajdowa� si� �rodek kr��ka.
Kierunek kr��ka r�wnie� jest losowany. W tym przypadku mo�e on by� r�wny:
1. Je�eli leci w stron� gracza lewego, od -175 do -185 stopni.
2. Je�eli leci w stron� gracza prawego, od -5 do +5 stopni.

JAK DODAC OBJ FILES Z PONGGAME DO PONGGAMETEST

To link the tests to the object or library files
If the code under test does not export the functions that you want to test, you can add the output .obj or .lib file to the dependencies of the test project.

1. Create a C++ test project.

On the File menu, choose New, Project, Visual C++,Test, C++ Unit Test Project.

2. In Solution Explorer, on the shortcut menu of the test project, choose Properties. The project properties window opens.

3. Choose Configuration Properties, Linker, Input, Additional Dependencies.

Choose Edit, and add the names of the .obj or .lib files. Do not use the full path names.

4. Choose Configuration Properties, Linker, General, Additional Library Directories.

Choose Edit, and add the directory path of the .obj or .lib files. The path is typically within the build folder of the project under test.

5. Choose Configuration Properties, VC++ Directories, Include Directories.

Choose Edit, and then add the header directory of the project under test.
