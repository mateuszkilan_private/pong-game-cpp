#include "../../Header Files/Classes/AIPlayer.h"
#include "../../Header Files/Namespaces/PuckHelpers.h"
#include <tuple>
#include "../../Header Files/Namespaces/Utils.h"

using namespace AppInitializers;
using namespace AppConsts;
using namespace PuckHelpers;

AIPlayer::AIPlayer(AppConsts::GAME_SETTINGS sidePlayingOn, AppConsts::SPEED_BOOST_STATES speedBoostState, std::shared_ptr<SpriteWithoutMask>& racketSprite, std::shared_ptr<SpriteWithoutMask>& speedBoostSprite, AppConsts::GAME_SETTINGS difficultyLevel) :
    Player(sidePlayingOn, speedBoostState, racketSprite, speedBoostSprite, false),
    mDifficultyLevel(difficultyLevel)
{
    mAIState = AI_STATES::IDLE;
    mAIStateFuncPtr = &AIPlayer::doIdling;
    mTimeUntilCollisionWithAIGoalLine = 0.0f;
    mRacketSprite->mVelocity.y = 0.0f;

    mIdlingSetupDone = false;
    mDefenseSetupDone = false;
    mAttackSetupDone = false;
    mPositionResettingSetupDone = false;
    mDistancingSetupDone = false;

    mDefendRecallBasedOnTurnedSpeedBoost = false;

    mTrickShotAttackAreasTopTupleVector.resize(0);
    mTrickShotAttackAreasBottomTupleVector.resize(0);
    mNormalShotAttackAreasTopTupleVector.resize(0);
    mNormalShotAttackAreasBottomTupleVector.resize(0);

    mGoalLineEffectivePucksCollRange = Vec2(-1.0f, -1.0f);

    switch (mDifficultyLevel) {
    case GAME_SETTINGS::DIFFICULTY_EASY:
        mAnalyseOrChangeSpeedBoostStatePtr = &AIPlayer::analyseOrChangeSpeedBoostStateEasy;
        mGetRacketsYCoordSpeedPtr = &AIPlayer::getRacketsYCoordSpeedEasy;
        mReactionLagInSeconds = G_AI_EASY_DEFEND_LAG_IN_SECONDS;
        mRunDistancingSetupPtr = &AIPlayer::runDistancingSetupEasy;
        break;
    case GAME_SETTINGS::DIFFICULTY_MEDIUM:
        mAnalyseOrChangeSpeedBoostStatePtr = &AIPlayer::analyseOrChangeSpeedBoostStateMedium;
        mGetRacketsYCoordSpeedPtr = &AIPlayer::getRacketsYCoordSpeedMedium;
        mReactionLagInSeconds = G_AI_MEDIUM_DEFEND_LAG_IN_SECONDS;
        mRunDistancingSetupPtr = &AIPlayer::runDistancingSetupMediumAndHard;
        break;
    case GAME_SETTINGS::DIFFICULTY_HARD:
        mAnalyseOrChangeSpeedBoostStatePtr = &AIPlayer::analyseOrChangeSpeedBoostStateHard;
        mGetRacketsYCoordSpeedPtr = &AIPlayer::getRacketsYCoordSpeedHard;
        mReactionLagInSeconds = G_AI_HARD_DEFEND_LAG_IN_SECONDS;
        mRunDistancingSetupPtr = &AIPlayer::runDistancingSetupMediumAndHard;
        break;
    default:
        break;
    }
}

void AIPlayer::runDistancingSetupEasy() {
    mRacketSprite->mVelocity.y = 0.0f;
    mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
}

void AIPlayer::runDistancingSetupMediumAndHard() {
    auto racketsPosYCoord = mRacketSprite->getPosition().y;
    auto halfOfClientsHeight = G_CLIENT_HEIGHT / 2;

    mAmountOfDistanceWhichRacketNeedsToCover = halfOfClientsHeight - racketsPosYCoord;

    COMPILE_LOG("runDistancingSetupMediumAndHard()/halfOfClientsHeight = " + std::to_string(halfOfClientsHeight), DISTANCE_LOGS);
    COMPILE_LOG("runDistancingSetupMediumAndHard()/racketsPosYCoord = " + std::to_string(racketsPosYCoord), DISTANCE_LOGS);
    COMPILE_LOG("runDistancingSetupMediumAndHard()/mAmountOfDistanceWhichRacketNeedsToCover = " + std::to_string(mAmountOfDistanceWhichRacketNeedsToCover), DISTANCE_LOGS);

    if (mAmountOfDistanceWhichRacketNeedsToCover == 0.0f) {
        return;
    }

    if (racketsPosYCoord > halfOfClientsHeight) {
        // racket needs to move top
        mRacketSprite->mVelocity.y = -getRacketYCoordSpeedBasedOnSpeedBoost();
    }
    else {
        // racket needs to move bottom
        mRacketSprite->mVelocity.y = getRacketYCoordSpeedBasedOnSpeedBoost();
    }
}

void AIPlayer::changeAIStateAndStateFuncPtr(AI_STATES newAIState) {
    COMPILE_LOG("calling AIPlayer::changeAIStateAndStateFuncPtr", CHANGE_STATE_LOGS);
    switch (mAIState) {
    case AI_STATES::IDLE:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE FROM IDLE xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        idlingCleanUp();
        break;
    case AI_STATES::DEFENSE:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE FROM DEFENSE xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        defendingCleanUp();
        break;
    case AI_STATES::ATTACK:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE FROM ATTACK xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        attackingCleanUp();
        break;
    case AI_STATES::DISTANCING:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE FROM DISTANCING xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        distancingCleanUp();
        break;
    case AI_STATES::POSITION_RESET:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE FROM POSITION_RESET xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        positionResettingCleanUp();
        break;
    default:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE FROM ... default behaviour - this shouldn't have happened xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        break;
    }

    switch (newAIState) {
    case AI_STATES::IDLE:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE TO IDLE xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        mAIStateFuncPtr = &AIPlayer::doIdling;
        break;
    case AI_STATES::DEFENSE:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE TO DEFENSE xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        mAIStateFuncPtr = &AIPlayer::doDefending;
        break;
    case AI_STATES::DISTANCING:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE TO DISTANCING xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        mAIStateFuncPtr = &AIPlayer::doDistancing;
        break;
    case AI_STATES::ATTACK:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE TO ATTACK xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        mAIStateFuncPtr = &AIPlayer::doAttacking;
        break;
    case AI_STATES::POSITION_RESET:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE TO POSITION_RESET xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        mAIStateFuncPtr = &AIPlayer::doPositionResetting;
        break;
    default:
        COMPILE_LOG("xxxxxxxxxxxx CHANGING STATE TO ... default behaviour - this shouldn't have happened xxxxxxxxxxxx", CHANGE_STATE_LOGS);
        break;
    }

    mAIState = newAIState;
};

void AIPlayer::runAI() {
    (this->*mAIStateFuncPtr)();
};

void AIPlayer::runIdlingSetup() {
    mRacketSprite->mVelocity.y = 0.0f;
}

void AIPlayer::doIdling() {
    if (!mIdlingSetupDone) {
        mIdlingSetupDone = true;
        runIdlingSetup();
    }
};

void AIPlayer::runDefendingSetup() {
    calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine();
    COMPILE_LOG("============== STARTING calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine results logging ==============", DEFEND_LOGS);
    COMPILE_LOG("mTimeUntilCollisionWithAIGoalLine = " + std::to_string(mTimeUntilCollisionWithAIGoalLine), DEFEND_LOGS);
    COMPILE_LOG("mGoalLineCollPos = (" + std::to_string(mGoalLineCollPos.x) + ", " + std::to_string(mGoalLineCollPos.y) + ") REFERENCE ID 1", DEFEND_LOGS);
    COMPILE_LOG("============== ENDING calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine results logging ==============\n\n", DEFEND_LOGS);
    if (calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel() == AI_STATES::ATTACK) {
        changeAIStateAndStateFuncPtr(AI_STATES::ATTACK);
    }
};

void AIPlayer::doDefending() {
    mTimerForReactionLagInSeconds += GDeltaTimeInSeconds;

    // If we have doDefending() recalled based on AI that is trying to defend with speed boost then we don't want to wait once again for lag timer.
    if ((mTimerForReactionLagInSeconds >= mReactionLagInSeconds || mDefendRecallBasedOnTurnedSpeedBoost) && !mDefenseSetupDone) {
        COMPILE_LOG("XXXXXXXXXXXXXXXXXXXXXXXX DEFENDING SETUP START XXXXXXXXXXXXXXXXXXXXXXXX", DEFEND_LOGS);
        mTimerForReactionLagInSeconds = 0.0f;
        mDefenseSetupDone = true;
        runDefendingSetup();
    }
};

void AIPlayer::doDistancing() {
    if (!mDistancingSetupDone) {
        mDistancingSetupDone = true;
        runDistancingSetup();
    }

    COMPILE_LOG("doDistancing()/mAmountOfDistanceWhichRacketNeedsToCover = " + std::to_string(mAmountOfDistanceWhichRacketNeedsToCover), DISTANCE_LOGS);
    COMPILE_LOG("doDistancing()/mRacketSprite->mVelocity.y = " + std::to_string(mRacketSprite->mVelocity.y), DISTANCE_LOGS);

    if (mAmountOfDistanceWhichRacketNeedsToCover != 0.0f) {
        if (mAmountOfDistanceWhichRacketNeedsToCover > 0.0f) {
            // It means that racket needs to move bottom
            mAmountOfDistanceWhichRacketNeedsToCover -= mRacketSprite->mVelocity.y * GDeltaTimeInSeconds;
            if (mAmountOfDistanceWhichRacketNeedsToCover < 0.0f) {
                mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
                mRacketSprite->mVelocity.y = 0.0f;
            }
        }
        else {
            // It means that racket needs to move top
            mAmountOfDistanceWhichRacketNeedsToCover -= mRacketSprite->mVelocity.y * GDeltaTimeInSeconds;
            if (mAmountOfDistanceWhichRacketNeedsToCover > 0.0f) {
                mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
                mRacketSprite->mVelocity.y = 0.0f;
            }
        }
    }
};
void AIPlayer::doAttacking() {
    if (!mAttackSetupDone) {
        COMPILE_LOG("XXXXX ATTACK SETUP START XXXXX", ATTACK_LOGS);
        COMPILE_LOG("FOR VISUAL REPRESENTATION mGoalLineCollPos = (" + std::to_string(mGoalLineCollPos.x) + ", " + std::to_string(mGoalLineCollPos.y) + ") REFERENCE ID 1", ATTACK_LOGS);
        COMPILE_LOG("FOR VISUAL REPRESENTATION getRacketsRange() = (" + std::to_string(getRacketsRange().x) + ", " + std::to_string(getRacketsRange().y) + ") REFERENCE ID 2", ATTACK_LOGS);
        COMPILE_LOG("FOR VISUAL REPRESENTATION mRacketSprite->getPosition() = (" + std::to_string(mRacketSprite->getPosition().x) + ", " + std::to_string(mRacketSprite->getPosition().y) + ") REFERENCE ID 2", ATTACK_LOGS);
        mAttackSetupDone = true;
        runAttackSetup();
    }

    COMPILE_LOG("Logging once int the check loop mAmountOfDistanceWhichRacketNeedsToCover = " + std::to_string(mAmountOfDistanceWhichRacketNeedsToCover), ATTACK_LOGS, true, 5);
    if (mAmountOfDistanceWhichRacketNeedsToCover > 0.0f) {
        // It means that racket is moving bottom
        if (mRacketSprite->getPosition().y >= mRacketsYCoordShouldBeEqual) {
            COMPILE_LOG("Stopping AI's racket!", ATTACK_LOGS, true, 1);
            COMPILE_LOG("AI's racket position vector = (" + std::to_string(mRacketSprite->getPosition().x) + ", " + std::to_string(mRacketSprite->getPosition().y) + ")", ATTACK_LOGS, true, 2);
            mRacketSprite->mVelocity.y = 0.0f;
        }
    }
    else {
        // It means that racket is moving top
        if (mRacketSprite->getPosition().y <= mRacketsYCoordShouldBeEqual) {
            COMPILE_LOG("Stopping AI's racket!", ATTACK_LOGS, true, 3);
            COMPILE_LOG("AI's racket position vector = (" + std::to_string(mRacketSprite->getPosition().x) + ", " + std::to_string(mRacketSprite->getPosition().y) + ")", ATTACK_LOGS, true, 4);
            mRacketSprite->mVelocity.y = 0.0f;
        }
    }
};

inline void AIPlayer::runDistancingSetup() {
    (this->*mRunDistancingSetupPtr)();
}

void AIPlayer::runPositionResettingSetup() {
    auto racketsPosYCoord = mRacketSprite->getPosition().y;
    auto halfOfClientsHeight = G_CLIENT_HEIGHT / 2;

    mAmountOfDistanceWhichRacketNeedsToCover = halfOfClientsHeight - racketsPosYCoord;

    if (mAmountOfDistanceWhichRacketNeedsToCover == 0.0f) {
        return;
    }

    if (racketsPosYCoord > halfOfClientsHeight) {
        // racket needs to move top
        mRacketSprite->mVelocity.y = -getRacketYCoordSpeedBasedOnSpeedBoost();
    }
    else {
        // racket needs to move bottom
        mRacketSprite->mVelocity.y = getRacketYCoordSpeedBasedOnSpeedBoost();
    }
}

void AIPlayer::doPositionResetting() {
    if (!mPositionResettingSetupDone) {
        mPositionResettingSetupDone = true;
        runPositionResettingSetup();
    }

    if (mAmountOfDistanceWhichRacketNeedsToCover != 0.0f) {
        if (mAmountOfDistanceWhichRacketNeedsToCover > 0.0f) {
            // It means that racket needs to move bottom
            mAmountOfDistanceWhichRacketNeedsToCover -= mRacketSprite->mVelocity.y * GDeltaTimeInSeconds;
            if (mAmountOfDistanceWhichRacketNeedsToCover < 0.0f) {
                mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
            }
        }
        else {
            // It means that racket needs to move top
            mAmountOfDistanceWhichRacketNeedsToCover -= mRacketSprite->mVelocity.y * GDeltaTimeInSeconds;
            if (mAmountOfDistanceWhichRacketNeedsToCover > 0.0f) {
                mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
            }
        }
    }
    else {
        changeAIStateAndStateFuncPtr(AI_STATES::IDLE);
    }
};

float AIPlayer::getRacketsYCoordSpeedEasy() {
    return (GRacketsChosenSpeedForCoordinateY * G_AI_EASY_PERCENTAGE_OF_NORMAL_RACKET_SPEED) / 100;
}

float AIPlayer::getRacketsYCoordSpeedMedium() {
    return (GRacketsChosenSpeedForCoordinateY * G_AI_MEDIUM_PERCENTAGE_OF_NORMAL_RACKET_SPEED) / 100;
}

float AIPlayer::getRacketsYCoordSpeedHard() {
    return (GRacketsChosenSpeedForCoordinateY * G_AI_HARD_PERCENTAGE_OF_NORMAL_RACKET_SPEED) / 100;
}

inline float AIPlayer::getRacketYCoordSpeedBasedOnSpeedBoost() {
    auto racketsYCoordSpeed = (this->*mGetRacketsYCoordSpeedPtr)();

    return mSpeedBoostState == SPEED_BOOST_STATES::ON ? racketsYCoordSpeed * G_RACKETS_SPEED_BOOST_MODIFIER : racketsYCoordSpeed;
}

void AIPlayer::analyseOrChangeSpeedBoostState(float timeSinceLastFrame) {
    (this->*mAnalyseOrChangeSpeedBoostStatePtr)(timeSinceLastFrame);
}

void AIPlayer::analyseOrChangeSpeedBoostStateHard(float timeSinceLastFrame) {
    using namespace AppConsts;
    using namespace AppInitializers;

    switch (mSpeedBoostState) {
    case SPEED_BOOST_STATES::ON:
        mSpeedBoostDeltaTime += timeSinceLastFrame;
        if (mSpeedBoostDeltaTime >= 3.0f) {
            mSpeedBoostState = SPEED_BOOST_STATES::RECHARGING;
            mSpeedBoostDeltaTime = 0.0f;
            if (mRacketSprite->mVelocity.y != 0.0f) {
                mRacketSprite->mVelocity.y = mRacketSprite->mVelocity.y < 0.0f ? -getRacketYCoordSpeedBasedOnSpeedBoost() : getRacketYCoordSpeedBasedOnSpeedBoost();
            }
        }
        break;
    case SPEED_BOOST_STATES::RECHARGING:
        mSpeedBoostDeltaTime += timeSinceLastFrame;
        if (mSpeedBoostDeltaTime >= 3.0f) {
            mSpeedBoostState = SPEED_BOOST_STATES::AVAILABLE;
            mSpeedBoostDeltaTime = 0.0f;
        }
        break;
    }

    Player::analyseOrChangeSpeedBoostState(timeSinceLastFrame);
};

void AIPlayer::analyseOrChangeSpeedBoostStateMedium(float timeSinceLastFrame) {}; // AI on medium doesn't ues speed boost at all.

void AIPlayer::analyseOrChangeSpeedBoostStateEasy(float timeSinceLastFrame) {}; // AI on easy doesn't use speed boost at all.

auto AIPlayer::calculateDistanceBetweenTwoVec2(Vec2 vecOne, Vec2 vecTwo) {
    using namespace std;
    return sqrt(pow(vecTwo.x - vecOne.x, 2.0f) + pow(vecTwo.y - vecOne.y, 2.0f));
}

Vec2 AIPlayer::calculateLinearFunctionEquation(Vec2 velAbstractForPuck, Vec2 posAbstractForPuck) {
    // posAbstractForPuck position 1 for set of equations
    // Here we create 2nd point to have 2 points in order to create linear function's equation
    const auto velVecPos = Vec2(posAbstractForPuck.x + velAbstractForPuck.x, posAbstractForPuck.y + velAbstractForPuck.y); // Position 2 for set of equations

    // example of linear function ax + b = y
    //  200a + b = 160
    //  100a + b = 110
    // - =============
    //  100a + 0 = 50
    // this is exactly what below equation is showing. I only have two elements because 1 - 1 is 0. I don't need to store this result or anything, it would be just a waste of memory.
    auto subtractionResult = std::vector<float>{ posAbstractForPuck.x - velVecPos.x, posAbstractForPuck.y - velVecPos.y };

    // now we need to calculate a
    // 100a = 50
    // a = 50 / 100
    auto aInLinearFunc = subtractionResult[1] / subtractionResult[0];

    // now we need to substitute "a" number to one of equations to calculate "b"
    auto bInLinearFunc = velVecPos.y - velVecPos.x * aInLinearFunc;

    // method returns Vec2(x, y). x == a & y == b
    return Vec2(aInLinearFunc, bInLinearFunc);
}

void AIPlayer::calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine() {
    auto currentPuckVel = GPuck->mVelocity;
    auto nextVelAbstractOfThePuck = GPuck->mVelocity;
    auto currentPuckPos = GPuck->getPosition();

    auto nextPosAbstractOfThePuck = GPuck->getPosition();
    auto aInLinearFunc = 0.0f;
    auto bInLinearFunc = 0.0f;
    auto exitLoop = false;

    while (true) {
        // I need to call function that will tell AI where puck will collide. With top, down, left or right. This way I can calculate the second coordinate and have complete Vec2 point of next collision point.
        auto aAndBOfLinearFuncEquation = calculateLinearFunctionEquation(currentPuckVel, currentPuckPos);
        aInLinearFunc = aAndBOfLinearFuncEquation.x;
        bInLinearFunc = aAndBOfLinearFuncEquation.y;

        GetPredictionVec2OfNextCollision(nextVelAbstractOfThePuck, nextPosAbstractOfThePuck);

        // In this conditions nextPosAbstractOfThePuck are codes not values - look at GetPredictionVec2OfNextCollision's return values for reference
        if (nextPosAbstractOfThePuck.x == -1.0f && nextPosAbstractOfThePuck.y > 0.0f) {
            // It means that I need to calculate x. In this case puck will hit top or bottom.
            nextPosAbstractOfThePuck.x = (nextPosAbstractOfThePuck.y - bInLinearFunc) / aInLinearFunc;
        }
        else if (nextPosAbstractOfThePuck.x > 0.0f && nextPosAbstractOfThePuck.y == -1.0f) {
            // It means that I need to calculate y. In this case it means that puck will attack other player directly.
            nextPosAbstractOfThePuck.y = aInLinearFunc * nextPosAbstractOfThePuck.x + bInLinearFunc;
            mGoalLineCollPos = Vec2(nextPosAbstractOfThePuck.x, nextPosAbstractOfThePuck.y);
            exitLoop = true;
        }
        else if (nextPosAbstractOfThePuck.x == -1.0f && nextPosAbstractOfThePuck.y == -1.0f) {
            // It means that puck is not moving, this case will never happen. But for the sake of completeness we should just return in this scenario
            return;
        }
        else if (nextPosAbstractOfThePuck.y == 0.0f) {
            // It means that puck is going straight line left or right. In this case it will hit second player's goal line. We can exit the loop at the very bottom. I don't need to set any coordinates, both were set inside GetPredictionVec2OfNextCollision function
            mGoalLineCollPos = Vec2(nextPosAbstractOfThePuck.x, currentPuckPos.y);
            exitLoop = true;
        }
        // There is also case when pucks moves straight line vertically. In this case it will never hit any goal lines. I don't need to service this case bacause this scenario will never appear in real game.

        const auto pucksVelVecLength = CountVelocityVectorsLengthUsingPythagoras(currentPuckVel);
        const auto nextCollisionVecLength = calculateDistanceBetweenTwoVec2(currentPuckPos, nextPosAbstractOfThePuck);

        mTimeUntilCollisionWithAIGoalLine += nextCollisionVecLength / pucksVelVecLength;

        currentPuckVel = nextVelAbstractOfThePuck;
        currentPuckPos = nextPosAbstractOfThePuck;

        if (exitLoop) {
            break;
        }
    }
}

AppConsts::AI_STATES AIPlayer::calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel() {
    auto racketsPos = mRacketSprite->getPosition();
    auto aiRacketsTopCoordY = racketsPos.y - static_cast<float>(GHalfOfRacketsHeight);
    auto aiRacketsBottomCoordY = racketsPos.y + static_cast<float>(GHalfOfRacketsHeight);
    mGoalLineEffectivePucksCollRange = GetEffectivePuckCollisionRange(mGoalLineCollPos);

    COMPILE_LOG("========================= STARTING calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel logging =========================", DEFEND_LOGS);
    COMPILE_LOG("mGoalLineEffectivePucksCollRange = (" + std::to_string(mGoalLineEffectivePucksCollRange.x) + ", " + std::to_string(mGoalLineEffectivePucksCollRange.y) + ")", DEFEND_LOGS);

    auto willPuckHitAboveRacket = aiRacketsTopCoordY > mGoalLineEffectivePucksCollRange.y;
    auto willPuckHitBelowRacket = aiRacketsBottomCoordY < mGoalLineEffectivePucksCollRange.x;
    // if both above are false then it means that puck will hit where racket is currently positioned;

    if (willPuckHitAboveRacket) {
        auto vecLengthFromRacketsTopYCoordToEffectivePucksBottomYCoord = aiRacketsTopCoordY - mGoalLineEffectivePucksCollRange.y;
        mDistanceForRacketToCoverBeforeColWithPuck = vecLengthFromRacketsTopYCoordToEffectivePucksBottomYCoord;
        auto timeNeededForRacketToGetToEffectiveCollisionPos = vecLengthFromRacketsTopYCoordToEffectivePucksBottomYCoord / getRacketYCoordSpeedBasedOnSpeedBoost();

        COMPILE_LOG("===== PUCK WILL HIT ABOVE RACKET =====", DEFEND_LOGS);
        COMPILE_LOG("mDistanceForRacketToCoverBeforeColWithPuck = " + std::to_string(mDistanceForRacketToCoverBeforeColWithPuck), DEFEND_LOGS);
        COMPILE_LOG("timeNeededForRacketToGetToEffectiveCollisionPos = " + std::to_string(timeNeededForRacketToGetToEffectiveCollisionPos), DEFEND_LOGS);

        if (timeNeededForRacketToGetToEffectiveCollisionPos < mTimeUntilCollisionWithAIGoalLine) {
            // It means that AI will be able to defend from losing goal with at least 1 px.
            mTimeLeftToPlanAttack = mTimeUntilCollisionWithAIGoalLine - timeNeededForRacketToGetToEffectiveCollisionPos;
            mRacketsRelativePointBouncePlaceDuringFirstRecognition = 0;

            COMPILE_LOG("===== WILL BE ABLE TO DEFEND TOP, WILL CHANGE STATE TO ATTACK =====", DEFEND_LOGS);
            COMPILE_LOG("mTimeLeftToPlanAttack = " + std::to_string(mTimeLeftToPlanAttack), DEFEND_LOGS);
            COMPILE_LOG("mRacketsRelativePointBouncePlaceDuringFirstRecognition = " + std::to_string(mRacketsRelativePointBouncePlaceDuringFirstRecognition), DEFEND_LOGS);
            COMPILE_LOG("========================= ENDING calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel logging =========================\n\n", DEFEND_LOGS);

            if (mDifficultyLevel == GAME_SETTINGS::DIFFICULTY_HARD && mSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE && rand() % 3 == 1) {
                mSpeedBoostState = SPEED_BOOST_STATES::ON;
                mDefendRecallBasedOnTurnedSpeedBoost = true; // This is here needed to not go through lagTimer at the start of doDefending() method
                mDefenseSetupDone = false;
                doDefending();
            }

            return AI_STATES::ATTACK;
        }
        else {
            // AI will not be able to defend. In here I will give speed boost option and check once again if AI can defend it - for AI on medium and hard. For AI on easy it means that it will not be able to defend. For easy AI we will fake defensive move in order to make it look more human-like.
            // THIS CONDITION IS NOT FINISHED FOR MEDIUM AND HARD AI
            COMPILE_LOG("===== WILL NOT BE ABLE TO DEFEND TOP, CHANGING STATE TO NONE =====", DEFEND_LOGS);
            COMPILE_LOG("========================= ENDING calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel logging =========================\n\n", DEFEND_LOGS);

            if (mDifficultyLevel != GAME_SETTINGS::DIFFICULTY_HARD || mDefendRecallBasedOnTurnedSpeedBoost == true) {
                mTimeLeftToPlanAttack = 0.0f;
                mRacketsRelativePointBouncePlaceDuringFirstRecognition = -1;
                mRacketSprite->mVelocity.y = -getRacketYCoordSpeedBasedOnSpeedBoost(); // TODO - ustawic predkosc w zaleznosci od tego czy SI posiada aktywowany speed boost czy nie
                return AI_STATES::NONE; // We don't change to this state. We indicate that there is no state at all. During game AI can't be in none state.
            }
            else {
                // Here we are calling deffensive speed boost for AI on HARD difficulty.
                if (mSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE) {
                    mSpeedBoostState = SPEED_BOOST_STATES::ON;
                    mDefendRecallBasedOnTurnedSpeedBoost = true;
                    mDefenseSetupDone = false;
                    doDefending();
                }
                else {
                    mTimeLeftToPlanAttack = 0.0f;
                    mRacketsRelativePointBouncePlaceDuringFirstRecognition = -1;
                    mRacketSprite->mVelocity.y = -getRacketYCoordSpeedBasedOnSpeedBoost();
                    return AI_STATES::NONE; // We don't change to this state. We indicate that there is no state at all. During game AI can't be in none state.
                }
            }
        }
    }
    else if (willPuckHitBelowRacket) {
        auto vecLengthFromRacketsBottomYCoordToEffectivePucksTopYCoord = mGoalLineEffectivePucksCollRange.x - aiRacketsBottomCoordY;
        mDistanceForRacketToCoverBeforeColWithPuck = vecLengthFromRacketsBottomYCoordToEffectivePucksTopYCoord;
        auto timeNeededForRacketToGetToEffectiveCollisionPos = vecLengthFromRacketsBottomYCoordToEffectivePucksTopYCoord / getRacketYCoordSpeedBasedOnSpeedBoost();

        COMPILE_LOG("===== PUCK WILL HIT BELOW RACKET =====", DEFEND_LOGS);
        COMPILE_LOG("mDistanceForRacketToCoverBeforeColWithPuck = " + std::to_string(mDistanceForRacketToCoverBeforeColWithPuck), DEFEND_LOGS);
        COMPILE_LOG("timeNeededForRacketToGetToEffectiveCollisionPos = " + std::to_string(timeNeededForRacketToGetToEffectiveCollisionPos), DEFEND_LOGS);

        if (timeNeededForRacketToGetToEffectiveCollisionPos < mTimeUntilCollisionWithAIGoalLine) {
            // It means that AI will be able to defend from losing goal with at least 1 px.
            mTimeLeftToPlanAttack = mTimeUntilCollisionWithAIGoalLine - timeNeededForRacketToGetToEffectiveCollisionPos;
            // mRacketsRelativePointBouncePlaceDuringFirstRecognition = mRacketSprite->height(); // CHANGED_FOR_TESTS
            mRacketsRelativePointBouncePlaceDuringFirstRecognition = 100;

            COMPILE_LOG("===== WILL BE ABLE TO DEFEND BOTTOM, WILL CHANGE STATE TO ATTACK =====", DEFEND_LOGS);
            COMPILE_LOG("mTimeLeftToPlanAttack = " + std::to_string(mTimeLeftToPlanAttack), DEFEND_LOGS);
            COMPILE_LOG("mRacketsRelativePointBouncePlaceDuringFirstRecognition = " + std::to_string(mRacketsRelativePointBouncePlaceDuringFirstRecognition), DEFEND_LOGS);
            COMPILE_LOG("========================= ENDING calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel logging =========================\n\n", DEFEND_LOGS);

            if (mDifficultyLevel == GAME_SETTINGS::DIFFICULTY_HARD && mSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE && rand() % 3 == 1) {
                mSpeedBoostState = SPEED_BOOST_STATES::ON;
                mDefendRecallBasedOnTurnedSpeedBoost = true; // This is here needed to not go through lagTimer at the start of doDefending() method
                mDefenseSetupDone = false;
                doDefending();
            }

            return AI_STATES::ATTACK;
        }
        else {
            // AI will not be able to defend. In here I will give speed boost option and check once again if AI can defend it - for AI on medium and hard. For AI on easy it means that it will not be able to defend. For easy AI we will fake defensive move in order to make it look more human-like.
            COMPILE_LOG("===== WILL NOT BE ABLE TO DEFEND BOTTOM, CHANGING STATE TO NONE =====", DEFEND_LOGS);
            COMPILE_LOG("========================= ENDING calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel logging =========================\n\n", DEFEND_LOGS);

            if (mDifficultyLevel != GAME_SETTINGS::DIFFICULTY_HARD || mDefendRecallBasedOnTurnedSpeedBoost == true) {
                mTimeLeftToPlanAttack = 0.0f;
                mRacketsRelativePointBouncePlaceDuringFirstRecognition = -1;
                mRacketSprite->mVelocity.y = getRacketYCoordSpeedBasedOnSpeedBoost(); // TODO - ustawic predkosc w zaleznosci od tego czy SI posiada aktywowany speed boost czy nie
                return AI_STATES::NONE; // We don't change to this state. We indicate that there is no state at all. During game AI can't be in none state.
            }
            else {
                // Here we are calling deffensive speed boost for AI on HARD difficulty.
                if (mSpeedBoostState == SPEED_BOOST_STATES::AVAILABLE) {
                    mSpeedBoostState = SPEED_BOOST_STATES::ON;
                    mDefendRecallBasedOnTurnedSpeedBoost = true;
                    mDefenseSetupDone = false;
                    doDefending();
                }
                else {
                    mTimeLeftToPlanAttack = 0.0f;
                    mRacketsRelativePointBouncePlaceDuringFirstRecognition = -1;
                    mRacketSprite->mVelocity.y = getRacketYCoordSpeedBasedOnSpeedBoost();
                    return AI_STATES::NONE; // We don't change to this state. We indicate that there is no state at all. During game AI can't be in none state.
                }
            }
        }
    }
    else {
        // Puck will hit rackets current position without AI moving racket.
        mDistanceForRacketToCoverBeforeColWithPuck = 0.0f;
        mTimeLeftToPlanAttack = mTimeUntilCollisionWithAIGoalLine;
        auto racketsRange = getRacketsRange();
        float absoluteRacketsPixelBouncePlace = getYCoordOfPucksCollPntWithRacketForAI(racketsRange, mGoalLineEffectivePucksCollRange);

        mRacketsRelativePointBouncePlaceDuringFirstRecognition = absoluteRacketsPixelBouncePlace - racketsRange.x;

        COMPILE_LOG("===== PUCK WILL HIT AT RACKETS POSITION =====", DEFEND_LOGS);
        COMPILE_LOG("mDistanceForRacketToCoverBeforeColWithPuck = " + std::to_string(mDistanceForRacketToCoverBeforeColWithPuck), DEFEND_LOGS);
        COMPILE_LOG("===== WILL BE ABLE TO DEFEND AT RACKETS POSITION, WILL CHANGE STATE TO ATTACK =====", DEFEND_LOGS);
        COMPILE_LOG("mTimeLeftToPlanAttack = " + std::to_string(mTimeLeftToPlanAttack), DEFEND_LOGS);
        COMPILE_LOG("mRacketsRelativePointBouncePlaceDuringFirstRecognition = " + std::to_string(mRacketsRelativePointBouncePlaceDuringFirstRecognition), DEFEND_LOGS);
        COMPILE_LOG("========================= ENDING calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel logging =========================\n\n", DEFEND_LOGS);

        return AI_STATES::ATTACK;
    }
}

void AIPlayer::runAttackSetup() {
    auto pixelsDuringTimeLeftToPlanAttack = getRacketYCoordSpeedBasedOnSpeedBoost() * mTimeLeftToPlanAttack;
    COMPILE_LOG("runAttackSetup()/pixelsDuringTimeLeftToPlanAttack = " + std::to_string(pixelsDuringTimeLeftToPlanAttack), ATTACK_LOGS);

    if (mRacketsRelativePointBouncePlaceDuringFirstRecognition == 0) {
        // Puck will hit above racket

        //mRacketsRangeAtInitialCollWithPuck = Vec2(mGoalLineEffectivePucksCollRange.y, mGoalLineEffectivePucksCollRange.y + mRacketSprite->height()); // CHANGED_FOR_TESTS
        mRacketsRangeAtInitialCollWithPuck = Vec2(mGoalLineEffectivePucksCollRange.y, mGoalLineEffectivePucksCollRange.y + 100.0f);

        auto distanceFromModifiedRacketsTopToTopOfGameField = mRacketsRangeAtInitialCollWithPuck.x;
        auto maxDistanceCoverableByRacketTop = pixelsDuringTimeLeftToPlanAttack < distanceFromModifiedRacketsTopToTopOfGameField ? pixelsDuringTimeLeftToPlanAttack : distanceFromModifiedRacketsTopToTopOfGameField;
        mMaxDistanceCoverableByRacketTopAndBottom = Vec2(maxDistanceCoverableByRacketTop, 0.0f);

        COMPILE_LOG("runAttackSetup() ATTACK_DEDUCTION_DIRECTION::TOP", ATTACK_LOGS);
        COMPILE_LOG("runAttackSetup()/mRacketsRangeAtInitialCollWithPuck = (" + std::to_string(mRacketsRangeAtInitialCollWithPuck.x) + ", " + std::to_string(mRacketsRangeAtInitialCollWithPuck.y) + ")", ATTACK_LOGS);
        COMPILE_LOG("runAttackSetup()/mMaxDistanceCoverableByRacketTopAndBottom = (" + std::to_string(mMaxDistanceCoverableByRacketTopAndBottom.x) + ", " + std::to_string(mMaxDistanceCoverableByRacketTopAndBottom.y) + ")", ATTACK_LOGS);

        setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket(ATTACK_DEDUCTION_DIRECTION::TOP);

        mLastContactBounceAreaBottom = BOUNCE_AREA::NONE;
        mPixelsLeftInLastContactBounceAreaBottom = -1.0f;
        determinePossibleBounceAreasTop(mMaxDistanceCoverableByRacketTopAndBottom.x);

        topChooseBounceArea();
        topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity();
    }
    //else if (mRacketsRelativePointBouncePlaceDuringFirstRecognition == mRacketSprite->height()) { // CHANGED_FOR_TESTS
    else if (mRacketsRelativePointBouncePlaceDuringFirstRecognition == 100) {
        //mRacketsRangeAtInitialCollWithPuck = Vec2(mGoalLineEffectivePucksCollRange.x - mRacketSprite->height(), mGoalLineEffectivePucksCollRange.x); // CHANGED_FOR_TESTS

        mRacketsRangeAtInitialCollWithPuck = Vec2(mGoalLineEffectivePucksCollRange.x - 100.0f, mGoalLineEffectivePucksCollRange.x);
        auto distanceFromModifiedRacketsBottomToBottomOfGameField = G_CLIENT_HEIGHT - mRacketsRangeAtInitialCollWithPuck.y;
        auto maxDistanceCoverableByRacketBottom = pixelsDuringTimeLeftToPlanAttack < distanceFromModifiedRacketsBottomToBottomOfGameField ? pixelsDuringTimeLeftToPlanAttack : distanceFromModifiedRacketsBottomToBottomOfGameField;

        mMaxDistanceCoverableByRacketTopAndBottom = Vec2(0.0f, maxDistanceCoverableByRacketBottom);

        COMPILE_LOG("runAttackSetup() ATTACK_DEDUCTION_DIRECTION::BOTTOM", ATTACK_LOGS);
        COMPILE_LOG("runAttackSetup()/mRacketsRangeAtInitialCollWithPuck = (" + std::to_string(mRacketsRangeAtInitialCollWithPuck.x) + ", " + std::to_string(mRacketsRangeAtInitialCollWithPuck.y) + ")", ATTACK_LOGS);
        COMPILE_LOG("runAttackSetup()/mMaxDistanceCoverableByRacketTopAndBottom = (" + std::to_string(mMaxDistanceCoverableByRacketTopAndBottom.x) + ", " + std::to_string(mMaxDistanceCoverableByRacketTopAndBottom.y) + ")", ATTACK_LOGS);

        setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket(ATTACK_DEDUCTION_DIRECTION::BOTTOM);

        mLastContactBounceAreaTop = BOUNCE_AREA::NONE;
        mPixelsLeftInLastContactBounceAreaTop = -1.0f;

        determinePossibleBounceAreasBottom(mMaxDistanceCoverableByRacketTopAndBottom.y);
        bottomChooseBounceArea();
        bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity();
    }
    else {
        // It means that puck will hit at racket's current position

        mRacketsRangeAtInitialCollWithPuck = getRacketsRange();
        auto distanceFromRacketsTopToTopOfGameField = mRacketsRangeAtInitialCollWithPuck.x;
        auto distanceFromRacketsBottomToBottomOfGameField = G_CLIENT_HEIGHT - mRacketsRangeAtInitialCollWithPuck.y;

        auto maxDistanceCoverableByRacketTop = pixelsDuringTimeLeftToPlanAttack < distanceFromRacketsTopToTopOfGameField ? pixelsDuringTimeLeftToPlanAttack : distanceFromRacketsTopToTopOfGameField;
        auto maxDistanceCoverableByRacketBottom = pixelsDuringTimeLeftToPlanAttack < distanceFromRacketsBottomToBottomOfGameField ? pixelsDuringTimeLeftToPlanAttack : distanceFromRacketsBottomToBottomOfGameField;
        mMaxDistanceCoverableByRacketTopAndBottom = Vec2(maxDistanceCoverableByRacketTop, maxDistanceCoverableByRacketBottom);

        COMPILE_LOG("runAttackSetup() ATTACK_DEDUCTION_DIRECTION::TOP_AND_BOTTOM", ATTACK_LOGS);
        COMPILE_LOG("runAttackSetup()/mRacketsRangeAtInitialCollWithPuck = (" + std::to_string(mRacketsRangeAtInitialCollWithPuck.x) + ", " + std::to_string(mRacketsRangeAtInitialCollWithPuck.y) + ")", ATTACK_LOGS);
        COMPILE_LOG("runAttackSetup()/mMaxDistanceCoverableByRacketTopAndBottom = (" + std::to_string(mMaxDistanceCoverableByRacketTopAndBottom.x) + ", " + std::to_string(mMaxDistanceCoverableByRacketTopAndBottom.y) + ")", ATTACK_LOGS);

        setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket(ATTACK_DEDUCTION_DIRECTION::TOP_AND_BOTTOM);
        determinePossibleBounceAreasTop(mMaxDistanceCoverableByRacketTopAndBottom.x);
        determinePossibleBounceAreasBottom(mMaxDistanceCoverableByRacketTopAndBottom.y);

        auto willAttackTop = rand() % 2 ? false : true;

        if (willAttackTop) {
            topChooseBounceArea();
            topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity();
        }
        else { // bottom
            bottomChooseBounceArea();
            bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity();
        }
    }
}

void AIPlayer::setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket(ATTACK_DEDUCTION_DIRECTION direction) {
    if (direction == ATTACK_DEDUCTION_DIRECTION::TOP) {
        mRacketsRangeAfterMaxDistanceCoverableByRacketTop = Vec2(mRacketsRangeAtInitialCollWithPuck.x - mMaxDistanceCoverableByRacketTopAndBottom.x, mRacketsRangeAtInitialCollWithPuck.y - mMaxDistanceCoverableByRacketTopAndBottom.x);

        COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mRacketsRangeAfterMaxDistanceCoverableByRacketTop = (" + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketTop.x) + ", " + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketTop.y) + ")", ATTACK_LOGS);

        float absDecrementedGoalLineCoordY = getYCoordOfPucksCollPntWithRacketForAI(mRacketsRangeAfterMaxDistanceCoverableByRacketTop, mGoalLineEffectivePucksCollRange);

        if (absDecrementedGoalLineCoordY == -1.0f || absDecrementedGoalLineCoordY == mRacketsRangeAfterMaxDistanceCoverableByRacketTop.y) {
            // it means that AI can bounce with all possible bounce areas and all possible pixels inside those bounce areas.
            // -2.0f means that racket will be able to bounce with all areas and all pixels inside those bounce areas.
            // -1.0f means that it doesn't apply.
            mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = Vec2(-2.0f, -1.0f);

            COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = (" + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x) + ", " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y) + ")", ATTACK_LOGS);

            return;
        }

        auto relativeOffsetForRacket = mRacketsRangeAfterMaxDistanceCoverableByRacketTop.x;
        // We need it to have 0 to 100 distance in pixels of collision point with racket after max distance coverable in time that AI can plan its move
        mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = Vec2(absDecrementedGoalLineCoordY - relativeOffsetForRacket, -1.0f);

        COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = (" + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x) + ", " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y) + ")", ATTACK_LOGS);

        return;
    }
    else if (direction == ATTACK_DEDUCTION_DIRECTION::BOTTOM) {
        mRacketsRangeAfterMaxDistanceCoverableByRacketBottom = Vec2(mRacketsRangeAtInitialCollWithPuck.x + mMaxDistanceCoverableByRacketTopAndBottom.y, mRacketsRangeAtInitialCollWithPuck.y + mMaxDistanceCoverableByRacketTopAndBottom.y);

        COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mRacketsRangeAfterMaxDistanceCoverableByRacketBottom = (" + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.x) + ", " + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.y) + ")", ATTACK_LOGS);

        float absIncrementedGoalLineCoordY = getYCoordOfPucksCollPntWithRacketForAI(mRacketsRangeAfterMaxDistanceCoverableByRacketBottom, mGoalLineEffectivePucksCollRange);

        if (absIncrementedGoalLineCoordY == -1.0f || absIncrementedGoalLineCoordY == mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.x) {
            // it means that AI can bounce with all possible bounce areas and all possible pixels inside those bounce areas.
            // -2.0f means that racket will be able to bounce with all areas and all pixels inside those bounce areas.
            // -1.0f means that it doesn't apply.
            mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = Vec2(-1.0f, -2.0f);

            COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = (" + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x) + ", " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y) + ")", ATTACK_LOGS);

            return;
        }

        auto relativeOffsetForRacket = mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.x;
        // We need it to have 0 to 100 distance in pixels of collision point with racket after max distance coverable in time that AI can plan its move
        mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = Vec2(-1.0f, absIncrementedGoalLineCoordY - relativeOffsetForRacket);

        COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom = (" + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x) + ", " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y) + ")", ATTACK_LOGS);

        return;
    }
    else {
        // ATTACK_DEDUCTION_DIRECTION::TOP_AND_BOTTOM

        // TOP
        mRacketsRangeAfterMaxDistanceCoverableByRacketTop = Vec2(mRacketsRangeAtInitialCollWithPuck.x - mMaxDistanceCoverableByRacketTopAndBottom.x, mRacketsRangeAtInitialCollWithPuck.y - mMaxDistanceCoverableByRacketTopAndBottom.x);

        COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mRacketsRangeAfterMaxDistanceCoverableByRacketTop = (" + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketTop.x) + ", " + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketTop.y) + ")", ATTACK_LOGS);

        float absDecrementedGoalLineCoordY = getYCoordOfPucksCollPntWithRacketForAI(mRacketsRangeAfterMaxDistanceCoverableByRacketTop, mGoalLineEffectivePucksCollRange);

        if (absDecrementedGoalLineCoordY == -1.0f || absDecrementedGoalLineCoordY == mRacketsRangeAfterMaxDistanceCoverableByRacketTop.y) {
            // it means that AI can bounce with all possible bounce areas and all possible pixels inside those bounce areas.
            // -2.0f means that racket will be able to bounce with all areas and all pixels inside those bounce areas.
            mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x = -2.0f;

            COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x = " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x), ATTACK_LOGS);
        }
        else {
            auto relativeOffsetForRacket = mRacketsRangeAfterMaxDistanceCoverableByRacketTop.x;
            mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x = absDecrementedGoalLineCoordY - relativeOffsetForRacket;

            COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x = " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x), ATTACK_LOGS);
        }

        // BOTTOM
        auto mRacketsRangeAfterMaxDistanceCoverableByRacketBottom = Vec2(mRacketsRangeAtInitialCollWithPuck.x + mMaxDistanceCoverableByRacketTopAndBottom.y, mRacketsRangeAtInitialCollWithPuck.y + mMaxDistanceCoverableByRacketTopAndBottom.y);
        float absIncrementedGoalLineCoordY = getYCoordOfPucksCollPntWithRacketForAI(mRacketsRangeAfterMaxDistanceCoverableByRacketBottom, mGoalLineEffectivePucksCollRange);

        COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mRacketsRangeAfterMaxDistanceCoverableByRacketBottom = (" + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.x) + ", " + std::to_string(mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.y) + ")", ATTACK_LOGS);

        if (absIncrementedGoalLineCoordY == -1.0f || absIncrementedGoalLineCoordY == mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.x) {
            // it means that AI can bounce with all possible bounce areas and all possible pixels inside those bounce areas.
            // -2.0f means that racket will be able to bounce with all areas and all pixels inside those bounce areas.
            mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y = -2.0f;

            COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y = " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y), ATTACK_LOGS);
        }
        else {
            auto relativeOffsetForRacket = mRacketsRangeAfterMaxDistanceCoverableByRacketBottom.x;
            mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y = absIncrementedGoalLineCoordY - relativeOffsetForRacket;

            COMPILE_LOG("setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket()/mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y = " + std::to_string(mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y), ATTACK_LOGS);
        }
    }
}

void AIPlayer::determinePossibleBounceAreasTop(float maxDistanceTop) {
    auto relPucksCollYCoordTop = mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.x;

    if (relPucksCollYCoordTop == -2.0f) {
        // it means that AI will be able to bounce the puck with all possible bounce areas and within those areas - with all possible pixels.
        mPixelsLeftInLastContactBounceAreaTop = 10;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_90_TO_100;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -16.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, -16.5f, -26.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, -26.5f, -36.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, -36.5f, -76.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, -76.5f, -86.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, -86.5f, -96.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, -96.5f, -113.0f));

        COMPILE_LOG("determinePossibleBounceAreasTop() ALL BOUNCE AREAS ARE AVAILABLE", ATTACK_LOGS);
        return;
    }

    if (relPucksCollYCoordTop >= 0 && relPucksCollYCoordTop <= 10) {
        mPixelsLeftInLastContactBounceAreaTop = relPucksCollYCoordTop;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_0_TO_10;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -maxDistanceTop));
        mNormalShotAttackAreasTopTupleVector.resize(0);
    }
    else if (relPucksCollYCoordTop > 10 && relPucksCollYCoordTop <= 20) {
        mPixelsLeftInLastContactBounceAreaTop = relPucksCollYCoordTop - 10;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_10_TO_20;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -16.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, -16.5f, -maxDistanceTop));
        mNormalShotAttackAreasTopTupleVector.resize(0);
    }
    else if (relPucksCollYCoordTop > 20 && relPucksCollYCoordTop <= 30) {
        mPixelsLeftInLastContactBounceAreaTop = relPucksCollYCoordTop - 20;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_20_TO_30;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -16.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, -16.5f, -26.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, -26.5f, -maxDistanceTop));
    }
    else if (relPucksCollYCoordTop > 30 && relPucksCollYCoordTop <= 70) {
        mPixelsLeftInLastContactBounceAreaTop = relPucksCollYCoordTop - 30;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_30_TO_70;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -16.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, -16.5f, -26.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, -26.5f, -36.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, -36.5f, -maxDistanceTop));
    }
    else if (relPucksCollYCoordTop > 70 && relPucksCollYCoordTop <= 80) {
        mPixelsLeftInLastContactBounceAreaTop = relPucksCollYCoordTop - 70;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_70_TO_80;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -16.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, -16.5f, -26.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, -26.5f, -36.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, -36.5f, -76.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, -76.5f, -maxDistanceTop));
    }
    else if (relPucksCollYCoordTop > 80 && relPucksCollYCoordTop <= 90) {
        mPixelsLeftInLastContactBounceAreaTop = relPucksCollYCoordTop - 80;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_80_TO_90;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -16.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, -16.5f, -26.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, -26.5f, -36.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, -36.5f, -76.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, -76.5f, -86.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, -86.5f, -maxDistanceTop));
    }
    else if (relPucksCollYCoordTop > 90 && relPucksCollYCoordTop <= 100) { // until 99px because 100th pixel is taken care of by above if condition in the beginning of this function
        mPixelsLeftInLastContactBounceAreaTop = relPucksCollYCoordTop - 90;
        mLastContactBounceAreaTop = BOUNCE_AREA::FROM_90_TO_100;

        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 0.0f, -16.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, -16.5f, -26.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, -26.5f, -36.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, -36.5f, -76.5f));
        mNormalShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, -76.5f, -86.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, -86.5f, -96.5f));
        mTrickShotAttackAreasTopTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, -96.5f, -maxDistanceTop));
    }

    COMPILE_LOG("determinePossibleBounceAreasTop()/mTrickShotAttackAreasTopTupleVector.size() = " + std::to_string(mTrickShotAttackAreasTopTupleVector.size()), ATTACK_LOGS);
    COMPILE_LOG("determinePossibleBounceAreasTop()/mNormalShotAttackAreasTopTupleVector.size() = " + std::to_string(mNormalShotAttackAreasTopTupleVector.size()), ATTACK_LOGS);
    return;
}

void AIPlayer::determinePossibleBounceAreasBottom(float maxDistanceBottom) {
    auto relPucksCollYCoordBottom = mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom.y;

    if (relPucksCollYCoordBottom == -2.0f) {
        // it means that AI will be able to bounce the puck with all possible bounce areas and within those areas - with all possible pixels.
        mPixelsLeftInLastContactBounceAreaBottom = 10;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_0_TO_10;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, 16.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, 16.5f, 26.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, 26.5f, 36.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, 36.5f, 76.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, 76.5f, 86.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, 86.5f, 96.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 96.5f, 113.0f));
        COMPILE_LOG("determinePossibleBounceAreasBottom() ALL BOUNCE AREAS ARE AVAILABLE", ATTACK_LOGS);
        return;
    }

    if (relPucksCollYCoordBottom > 90 && relPucksCollYCoordBottom <= 100) {
        mPixelsLeftInLastContactBounceAreaBottom = 100 - relPucksCollYCoordBottom;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_90_TO_100;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, maxDistanceBottom));
        mNormalShotAttackAreasBottomTupleVector.resize(0);
    }
    else if (relPucksCollYCoordBottom > 80 && relPucksCollYCoordBottom <= 90) {
        mPixelsLeftInLastContactBounceAreaBottom = 90 - relPucksCollYCoordBottom;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_80_TO_90;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, 16.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, 16.5f, maxDistanceBottom));
        mNormalShotAttackAreasBottomTupleVector.resize(0);
    }
    else if (relPucksCollYCoordBottom > 70 && relPucksCollYCoordBottom <= 80) {
        mPixelsLeftInLastContactBounceAreaBottom = 80 - relPucksCollYCoordBottom;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_70_TO_80;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, 16.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, 16.5f, 26.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, 26.5f, maxDistanceBottom));
    }
    else if (relPucksCollYCoordBottom > 30 && relPucksCollYCoordBottom <= 70) {
        mPixelsLeftInLastContactBounceAreaBottom = 70 - relPucksCollYCoordBottom;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_30_TO_70;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, 16.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, 16.5f, 26.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, 26.5f, 36.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, 36.5f, maxDistanceBottom));
    }
    else if (relPucksCollYCoordBottom > 20 && relPucksCollYCoordBottom <= 30) {
        mPixelsLeftInLastContactBounceAreaBottom = 30 - relPucksCollYCoordBottom;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_20_TO_30;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, 16.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, 16.5f, 26.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, 26.5f, 36.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, 36.5f, 76.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, 76.5f, maxDistanceBottom));
    }
    else if (relPucksCollYCoordBottom > 10 && relPucksCollYCoordBottom <= 20) {
        mPixelsLeftInLastContactBounceAreaBottom = 20 - relPucksCollYCoordBottom;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_10_TO_20;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, 16.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, 16.5f, 26.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, 26.5f, 36.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, 36.5f, 76.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, 76.5f, 86.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, 86.5f, maxDistanceBottom));
    }
    else if (relPucksCollYCoordBottom >= 0 && relPucksCollYCoordBottom <= 10) {
        mPixelsLeftInLastContactBounceAreaBottom = 10 - relPucksCollYCoordBottom;
        mLastContactBounceAreaBottom = BOUNCE_AREA::FROM_0_TO_10;

        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_90_TO_100, 0.0f, 16.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_80_TO_90, 16.5f, 26.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_70_TO_80, 26.5f, 36.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_30_TO_70, 36.5f, 76.5f));
        mNormalShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_20_TO_30, 76.5f, 86.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_10_TO_20, 86.5f, 96.5f));
        mTrickShotAttackAreasBottomTupleVector.emplace_back(std::make_tuple(BOUNCE_AREA::FROM_0_TO_10, 96.5f, maxDistanceBottom));
    }

    COMPILE_LOG("determinePossibleBounceAreasBottom()/mTrickShotAttackAreasTopTupleVector.size() = " + std::to_string(mTrickShotAttackAreasTopTupleVector.size()), ATTACK_LOGS);
    COMPILE_LOG("determinePossibleBounceAreasBottom()/mNormalShotAttackAreasTopTupleVector.size() = " + std::to_string(mNormalShotAttackAreasTopTupleVector.size()), ATTACK_LOGS);
}

void AIPlayer::bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity() {
    auto minDistanceToGetToThisBounceArea = std::get<REL_MINIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple);
    auto pixelsInChosenBounceArea = std::get<REL_MAXIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple) - minDistanceToGetToThisBounceArea;
    auto chosenPixelInChosenBounceArea = rand() % (pixelsInChosenBounceArea == 0 ? 1 : static_cast<int>(pixelsInChosenBounceArea) + 1);
    auto bottomOffsetForChosenPixel = std::get<BOUNCE_AREA_GETTER>(mChosenBounceAreaAndAbsRangeTuple) == BOUNCE_AREA::FROM_30_TO_70 ? 40 : 10;

    if (mRacketsRelativePointBouncePlaceDuringFirstRecognition != 100) {
        mAmountOfDistanceWhichRacketNeedsToCover = -(100.0f - mRacketsRelativePointBouncePlaceDuringFirstRecognition + 6.5) + minDistanceToGetToThisBounceArea + (bottomOffsetForChosenPixel - chosenPixelInChosenBounceArea);

        mRacketSprite->mVelocity.y = getRacketYCoordSpeedBasedOnSpeedBoost();

        mRacketSprite->mVelocity.y = mAmountOfDistanceWhichRacketNeedsToCover < 0 ? -mRacketSprite->mVelocity.y : mRacketSprite->mVelocity.y;
    }
    else {
        mAmountOfDistanceWhichRacketNeedsToCover = mDistanceForRacketToCoverBeforeColWithPuck + minDistanceToGetToThisBounceArea + (bottomOffsetForChosenPixel - chosenPixelInChosenBounceArea);
        mRacketSprite->mVelocity.y = getRacketYCoordSpeedBasedOnSpeedBoost();
    }

    mRacketsYCoordShouldBeEqual = mRacketSprite->getPosition().y + mAmountOfDistanceWhichRacketNeedsToCover;

    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/chosenBouncePoint = " + std::to_string(chosenPixelInChosenBounceArea) + " | REFERENCE ID: BOUNCE_POINT", ATTACK_LOGS);
    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/minDistanceToGetToThisBounceArea = " + std::to_string(minDistanceToGetToThisBounceArea), ATTACK_LOGS);
    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/pixelsInChosenBounceArea = " + std::to_string(pixelsInChosenBounceArea), ATTACK_LOGS);
    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/chosenPixelInChosenBounceArea = " + std::to_string(chosenPixelInChosenBounceArea), ATTACK_LOGS);
    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/mAmountOfDistanceWhichRacketNeedsToCover = " + std::to_string(mAmountOfDistanceWhichRacketNeedsToCover) + " REFERENCE ID 2", ATTACK_LOGS);
    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/mRacketSprite->mVelocity.y = " + std::to_string(mRacketSprite->mVelocity.y), ATTACK_LOGS);
    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/mRacketsYCoordShouldBeEqual = " + std::to_string(mRacketsYCoordShouldBeEqual) + " REFERENCE ID 2", ATTACK_LOGS);
    COMPILE_LOG("bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/relativeBouncePixelOnRacketShouldBeEqual = " + std::to_string(100.0f - (minDistanceToGetToThisBounceArea + bottomOffsetForChosenPixel - 6.5f) + chosenPixelInChosenBounceArea) + " | REFERENCE ID: RELATIVE_BOUNCE_PIXEL", ANALYSE_COLLISION_LOGS);
}

void AIPlayer::bottomChooseBounceArea() {
    using namespace std;

    auto normalShotBottomVectorSize = static_cast<std::size_t>(mNormalShotAttackAreasBottomTupleVector.size());
    auto trickShotBottomVectorSize = static_cast<std::size_t>(mTrickShotAttackAreasBottomTupleVector.size());
    auto willAttackWithNormalShot = normalShotBottomVectorSize && rand() % 5 != 0 ? true : false;

    if (willAttackWithNormalShot) {

        switch (rand() % normalShotBottomVectorSize) {
        case 0:
            // BOUNCE_AREA::FROM_70_TO_80
            mChosenBounceAreaAndAbsRangeTuple = mNormalShotAttackAreasBottomTupleVector[0];
            break;
        case 1:
            // BOUNCE_AREA::FROM_30_TO_70
            mChosenBounceAreaAndAbsRangeTuple = mNormalShotAttackAreasBottomTupleVector[1];
            break;
        case 2:
            // BOUNCE_AREA::FROM_20_TO_30
            mChosenBounceAreaAndAbsRangeTuple = mNormalShotAttackAreasBottomTupleVector[2];
            break;
        }
    }
    else { //trick shot

        switch (rand() % trickShotBottomVectorSize) {
        case 0:
            // BOUNCE_AREA::FROM_90_TO_100
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasBottomTupleVector[0];
            break;
        case 1:
            // BOUNCE_AREA::FROM_80_TO_90
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasBottomTupleVector[1];
            break;
        case 2:
            // BOUNCE_AREA::FROM_10_TO_20
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasBottomTupleVector[2];
            break;
        case 3:
            // BOUNCE_AREA::FROM_0_TO_10
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasBottomTupleVector[3];
            break;
        }
    }

    COMPILE_LOG("bottomChooseBounceArea()/std::get<BOUNCE_AREA_GETTER>(mChosenBounceAreaAndAbsRangeTuple) = " + getStringifiedBounceArea(std::get<BOUNCE_AREA_GETTER>(mChosenBounceAreaAndAbsRangeTuple)) + " REFERENCE ID: BOUNCE_AREA", ATTACK_LOGS);
    COMPILE_LOG("bottomChooseBounceArea()/std::get<REL_MINIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple) = " + std::to_string(std::get<REL_MINIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple)), ATTACK_LOGS);
    COMPILE_LOG("bottomChooseBounceArea()/std::get<REL_MAXIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple) = " + std::to_string(std::get<REL_MAXIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple)), ATTACK_LOGS);
}

void AIPlayer::topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity() {
    auto minDistanceToGetToThisBounceArea = std::get<REL_MINIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple);
    auto pixelsInChosenBounceArea = std::abs(std::get<REL_MAXIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple) - minDistanceToGetToThisBounceArea);
    auto chosenPixelInChosenBounceArea = rand() % (pixelsInChosenBounceArea == 0 ? 1 : (static_cast<int>(pixelsInChosenBounceArea) + 1));

    if (mRacketsRelativePointBouncePlaceDuringFirstRecognition != 0) {
        // It means that puck will hit at rackets position
        mAmountOfDistanceWhichRacketNeedsToCover = minDistanceToGetToThisBounceArea + mRacketsRelativePointBouncePlaceDuringFirstRecognition - chosenPixelInChosenBounceArea + 6.5f;
        mRacketSprite->mVelocity.y = -getRacketYCoordSpeedBasedOnSpeedBoost();

        mRacketSprite->mVelocity.y = mAmountOfDistanceWhichRacketNeedsToCover > 0 ? -mRacketSprite->mVelocity.y : mRacketSprite->mVelocity.y;
    }
    else {
        mAmountOfDistanceWhichRacketNeedsToCover = -mDistanceForRacketToCoverBeforeColWithPuck + minDistanceToGetToThisBounceArea - chosenPixelInChosenBounceArea;
        mRacketSprite->mVelocity.y = -getRacketYCoordSpeedBasedOnSpeedBoost();
    }

    mRacketsYCoordShouldBeEqual = mRacketSprite->getPosition().y + mAmountOfDistanceWhichRacketNeedsToCover; // PLUS IS CORRECT IN HERE! WE ARE GOING UP SO + and - is - which is correct

    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/chosenBouncePoint = " + std::to_string(chosenPixelInChosenBounceArea) + " | REFERENCE ID: BOUNCE_POINT", ATTACK_LOGS);
    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/minDistanceToGetToThisBounceArea = " + std::to_string(minDistanceToGetToThisBounceArea), ATTACK_LOGS);
    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/pixelsInChosenBounceArea = " + std::to_string(pixelsInChosenBounceArea), ATTACK_LOGS);
    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/chosenPixelInChosenBounceArea = " + std::to_string(chosenPixelInChosenBounceArea), ATTACK_LOGS);
    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/mAmountOfDistanceWhichRacketNeedsToCover = " + std::to_string(mAmountOfDistanceWhichRacketNeedsToCover) + " REFERENCE ID 2", ATTACK_LOGS);
    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/mRacketSprite->mVelocity.y = " + std::to_string(mRacketSprite->mVelocity.y), ATTACK_LOGS);
    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/mRacketsYCoordShouldBeEqual = " + std::to_string(mRacketsYCoordShouldBeEqual) + " REFERENCE ID 2", ATTACK_LOGS);
    COMPILE_LOG("topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity()/relativeBouncePixelOnRacketShouldBeEqual = " + std::to_string(std::abs(minDistanceToGetToThisBounceArea) - 6.5f + chosenPixelInChosenBounceArea) + " | REFERENCE ID: RELATIVE_BOUNCE_PIXEL", ANALYSE_COLLISION_LOGS);
}

void AIPlayer::topChooseBounceArea() {
    auto normalShotTopVectorSize = static_cast<std::size_t>(mNormalShotAttackAreasTopTupleVector.size());
    auto trickShotTopVectorSize = static_cast<std::size_t>(mTrickShotAttackAreasTopTupleVector.size());
    auto willAttackWithNormalShot = normalShotTopVectorSize && rand() % 5 != 0 ? true : false;

    if (willAttackWithNormalShot) {
        switch (rand() % normalShotTopVectorSize) {
        case 0:
            // BOUNCE_AREA::FROM_20_TO_30
            mChosenBounceAreaAndAbsRangeTuple = mNormalShotAttackAreasTopTupleVector[0];
            break;
        case 1:
            // BOUNCE_AREA::FROM_30_TO_70
            mChosenBounceAreaAndAbsRangeTuple = mNormalShotAttackAreasTopTupleVector[1];
            break;
        case 2:
            // BOUNCE_AREA::FROM_70_TO_80
            mChosenBounceAreaAndAbsRangeTuple = mNormalShotAttackAreasTopTupleVector[2];
            break;
        }
    }
    else { //trick shot
        switch (rand() % trickShotTopVectorSize) {
        case 0:
            // BOUNCE_AREA::FROM_0_TO_10
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasTopTupleVector[0];
            break;
        case 1:
            // BOUNCE_AREA::FROM_10_TO_20
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasTopTupleVector[1];
            break;
        case 2:
            // BOUNCE_AREA::FROM_80_TO_90
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasTopTupleVector[2];
            break;
        case 3:
            // BOUNCE_AREA::FROM_90_TO_100
            mChosenBounceAreaAndAbsRangeTuple = mTrickShotAttackAreasTopTupleVector[3];
            break;
        }
    }

    COMPILE_LOG("topChooseBounceArea()/std::get<BOUNCE_AREA_GETTER>(mChosenBounceAreaAndAbsRangeTuple) = " + getStringifiedBounceArea(std::get<BOUNCE_AREA_GETTER>(mChosenBounceAreaAndAbsRangeTuple)) + " REFERENCE ID: BOUNCE_AREA", ATTACK_LOGS);
    COMPILE_LOG("topChooseBounceArea()/std::get<REL_MINIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple) = " + std::to_string(std::get<REL_MINIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple)), ATTACK_LOGS);
    COMPILE_LOG("topChooseBounceArea()/std::get<REL_MAXIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple) = " + std::to_string(std::get<REL_MAXIMUM_PX_CHANGE_GETTER>(mChosenBounceAreaAndAbsRangeTuple)), ATTACK_LOGS);
}

float AIPlayer::getYCoordOfPucksCollPntWithRacketForAI(const Vec2& racketsRange, const Vec2& pucksRange) {
    auto upperYCollisionRange = 0.0f;
    auto lowerYCollisionRange = 0.0f;

    upperYCollisionRange = pucksRange.x < racketsRange.x ? racketsRange.x : pucksRange.x;
    lowerYCollisionRange = pucksRange.y > racketsRange.y ? racketsRange.y : pucksRange.y;

    if (upperYCollisionRange > lowerYCollisionRange) {
        // it serves both cases when puck is out of boundry from top and bottom perspectives.
        return -1.0f;
    }

    return upperYCollisionRange + (lowerYCollisionRange - upperYCollisionRange) / 2.0f;
}

void AIPlayer::idlingCleanUp() {
    mIdlingSetupDone = false;
}

void AIPlayer::defendingCleanUp() {
    mDefenseSetupDone = false;
    mTimeUntilCollisionWithAIGoalLine = 0.0f; // this one HAS to be cleaned up manually because the only time it value changes is with +=
    mTimerForReactionLagInSeconds = 0.0f;
    mRacketSprite->mVelocity.y = 0.0f;
    mDefendRecallBasedOnTurnedSpeedBoost = false;
};

void AIPlayer::distancingCleanUp() {
    mDistancingSetupDone = false;
    mRacketSprite->mVelocity.y = 0.0f;

    mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
};

void AIPlayer::attackingCleanUp() {
    mAttackSetupDone = false;
    mRacketSprite->mVelocity.y = 0.0f;
    mChosenBounceAreaAndAbsRangeTuple = BounceAreaAndAbsRangeTuple{};

    mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
    mRacketsYCoordShouldBeEqual = 0.0f;

    mNormalShotAttackAreasBottomTupleVector.resize(0);
    mTrickShotAttackAreasBottomTupleVector.resize(0);
    mNormalShotAttackAreasTopTupleVector.resize(0);
    mTrickShotAttackAreasTopTupleVector.resize(0);
};

void AIPlayer::positionResettingCleanUp() {
    mPositionResettingSetupDone = false;
    mRacketSprite->mVelocity.y = 0.0f;

    mAmountOfDistanceWhichRacketNeedsToCover = 0.0f;
};
