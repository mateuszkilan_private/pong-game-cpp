#include "../../Header Files/Classes/Player.h"
#include "../../Header Files/Namespaces/PuckHelpers.h"

using namespace AppConsts;
using namespace PuckHelpers;

Player::Player(AppConsts::GAME_SETTINGS sidePlayingOn, AppConsts::SPEED_BOOST_STATES speedBoostState, std::shared_ptr<SpriteWithoutMask>& racketSprite, std::shared_ptr<SpriteWithoutMask>& speedBoostSprite, bool isHumanPlayer) {
	mSidePlayingOn = sidePlayingOn;
	mRacketSprite = racketSprite;
	mSpeedBoostSprite = speedBoostSprite;
    mIsHumanPlayer = isHumanPlayer;
    mSpeedBoostState = speedBoostState;
    mSpeedBoostDeltaTime = 0.0f;
    mScore = 0;
};

bool Player::isRacketMoving() {
    return mRacketSprite->mVelocity.y != 0 ? true : false;
};

const Vec2 Player::getRacketsRange() {
    auto racketsPos = mRacketSprite->getPosition();
    //auto halfOfRacketsHeight = (float)mRacketSprite->height() / 2.0f; // CHANGED_FOR_TESTS
    //return Vec2(racketsPos.y - halfOfRacketsHeight, racketsPos.y + halfOfRacketsHeight); // CHANGED_FOR_TESTS
    return Vec2(racketsPos.y - 50.0f, racketsPos.y + 50.0f);
}

const AppConsts::BOUNCE_AREA Player::deduceBounceArea(const float yCoordinate) {
    using namespace AppConsts;

    const auto racketsRange = getRacketsRange();
    COMPILE_LOG_STATEMENT(if (isAI()) {
        COMPILE_LOG("deduceBounceArea()/racketRelativeBouncePoint = " + std::to_string(yCoordinate - racketsRange.x) + " | REFERENCE ID: RELATIVE_BOUNCE_PIXEL", ANALYSE_COLLISION_LOGS);
    })

    if (yCoordinate <= racketsRange.x + 10) {
        COMPILE_LOG_STATEMENT(if (isAI()) {
            COMPILE_LOG("deduceBounceArea()/actualBouncePoint1 = " + std::to_string(yCoordinate - racketsRange.x) + " | REFERENCE ID: BOUNCE_POINT", ANALYSE_COLLISION_LOGS);
        })

        return BOUNCE_AREA::FROM_0_TO_10;
    }
    else if (yCoordinate <= racketsRange.x + 20) {
        COMPILE_LOG_STATEMENT(if (isAI()) {
            COMPILE_LOG("deduceBounceArea()/actualBouncePoint2 = " + std::to_string(yCoordinate - (racketsRange.x + 10)) + " | REFERENCE ID: BOUNCE_POINT", ANALYSE_COLLISION_LOGS);
        })

        return BOUNCE_AREA::FROM_10_TO_20;
    }
    else if (yCoordinate <= racketsRange.x + 30) {
        COMPILE_LOG_STATEMENT(if (isAI()) {
            COMPILE_LOG("deduceBounceArea()/actualBouncePoint3 = " + std::to_string(yCoordinate - (racketsRange.x + 20)) + " | REFERENCE ID: BOUNCE_POINT", ANALYSE_COLLISION_LOGS);
        })

        return BOUNCE_AREA::FROM_20_TO_30;
    }
    else if (yCoordinate <= racketsRange.x + 70) {
        COMPILE_LOG_STATEMENT(if (isAI()) {
            COMPILE_LOG("deduceBounceArea()/actualBouncePoint4 = " + std::to_string(yCoordinate - (racketsRange.x + 30)) + " | REFERENCE ID: BOUNCE_POINT", ANALYSE_COLLISION_LOGS);
        })

        return BOUNCE_AREA::FROM_30_TO_70;
    }
    else if (yCoordinate <= racketsRange.x + 80) {
        COMPILE_LOG_STATEMENT(if (isAI()) {
            COMPILE_LOG("deduceBounceArea()/actualBouncePoint5 = " + std::to_string(yCoordinate - (racketsRange.x + 70)) + " | REFERENCE ID: BOUNCE_POINT", ANALYSE_COLLISION_LOGS);
        })

        return BOUNCE_AREA::FROM_70_TO_80;
    }
    else if (yCoordinate <= racketsRange.x + 90) {
        COMPILE_LOG_STATEMENT(if (isAI()) {
            COMPILE_LOG("deduceBounceArea()/actualBouncePoint6 = " + std::to_string(yCoordinate - (racketsRange.x + 80)) + " | REFERENCE ID: BOUNCE_POINT", ANALYSE_COLLISION_LOGS);
        })

        return BOUNCE_AREA::FROM_80_TO_90;
    }
    else if (yCoordinate <= racketsRange.x + 100) {
        COMPILE_LOG_STATEMENT(if (isAI()) {
            COMPILE_LOG("deduceBounceArea()/actualBouncePoint7 = " + std::to_string(yCoordinate - (racketsRange.x + 90)) + " | REFERENCE ID: BOUNCE_POINT", ANALYSE_COLLISION_LOGS);
        })

        return BOUNCE_AREA::FROM_90_TO_100;
    }

    return BOUNCE_AREA::NONE;
}

void Player::analyseOrChangeSpeedBoostState(float deltaTime) {
    using namespace AppConsts;
    using namespace AppInitializers;

    switch (mSpeedBoostState) {
    case SPEED_BOOST_STATES::ON:
        mSpeedBoostDeltaTime += deltaTime;
        if (mSpeedBoostDeltaTime >= 3.0f) {
            mSpeedBoostState = SPEED_BOOST_STATES::RECHARGING;
            mSpeedBoostDeltaTime = 0.0f;
            if (mRacketSprite->mVelocity.y != 0.0f) {
                mRacketSprite->mVelocity.y = mRacketSprite->mVelocity.y < 0.0f ? -GRacketsChosenSpeedForCoordinateY : GRacketsChosenSpeedForCoordinateY;
            }
        }
        break;
    case SPEED_BOOST_STATES::RECHARGING:
        mSpeedBoostDeltaTime += deltaTime;
        if (mSpeedBoostDeltaTime >= 3.0f) {
            mSpeedBoostState = SPEED_BOOST_STATES::AVAILABLE;
            mSpeedBoostDeltaTime = 0.0f;
        }
        break;
    }
}

void Player::incrementScore() {
    ++mScore;
}

bool Player::hasWonGame() {
    return mScore == 10 ? true : false;
}

std::shared_ptr<SpriteWithoutMask>& Player::_getScoreSprite() {
    using namespace AppInitializers;

    switch (mScore) {
    case 0:
        return GScoreZeroDynPos;
    case 1:
        return GScoreOneDynPos;
    case 2:
        return GScoreTwoDynPos;
    case 3:
        return GScoreThreeDynPos;
    case 4:
        return GScoreFourDynPos;
    case 5:
        return GScoreFiveDynPos;
    case 6:
        return GScoreSixDynPos;
    case 7:
        return GScoreSevenDynPos;
    case 8:
        return GScoreEightDynPos;
    case 9:
        return GScoreNineDynPos;
    case 10:
        return GScoreTenDynPos;
    default:
        return GScoreZeroDynPos;
    }
}

std::shared_ptr<SpriteWithoutMask>& Player::getScoreForGameArea() {
    using namespace AppInitializers;
    using namespace AppConsts;

    Vec2 scorePosition = mSidePlayingOn == GAME_SETTINGS::SIDE_LEFT ? 
        (mScore != 10 ? G_SCORE_POS_GAME_AREA_SINGLE_DIGIT_LEFT_SIDE : G_SCORE_POS_GAME_AREA_MULTI_DIGITS_LEFT_SIDE) :
        (mScore != 10 ? G_SCORE_POS_GAME_AREA_SINGLE_DIGIT_RIGHT_SIDE : G_SCORE_POS_GAME_AREA_MULTI_DIGITS_RIGHT_SIDE);

    std::shared_ptr<SpriteWithoutMask>& scoreSprite = _getScoreSprite();
    scoreSprite->setPosition(scorePosition);

    return scoreSprite;
}

std::shared_ptr<SpriteWithoutMask>& Player::getScoreForEndGameMenu() {
    using namespace AppInitializers;
    using namespace AppConsts;

    Vec2 scorePosition = mSidePlayingOn == GAME_SETTINGS::SIDE_LEFT ?
        (mScore != 10 ? G_SCORE_POS_END_GAME_MENU_SINGLE_DIGIT_LEFT_SIDE : G_SCORE_POS_END_GAME_MENU_MULTI_DIGITS_LEFT_SIDE) :
        (mScore != 10 ? G_SCORE_POS_END_GAME_MENU_SINGLE_DIGIT_RIGHT_SIDE : G_SCORE_POS_END_GAME_MENU_MULTI_DIGITS_RIGHT_SIDE);

    std::shared_ptr<SpriteWithoutMask>& scoreSprite = _getScoreSprite();
    scoreSprite->setPosition(scorePosition);

    return scoreSprite;
}

std::shared_ptr<SpriteWithoutMask>& Player::getScoreForPauseMenu() {
    return getScoreForEndGameMenu();
}

bool Player::isHuman() {
    return mIsHumanPlayer;
};

bool Player::isAI() {
    return !mIsHumanPlayer;
};

AppConsts::AI_STATES Player::getState() {
    return mAIState;
}

bool Player::isPucksStateIncoming() {
    if (
        (mSidePlayingOn == GAME_SETTINGS::SIDE_LEFT && IsPuckMovingLeft()) ||
        (mSidePlayingOn == GAME_SETTINGS::SIDE_RIGHT && IsPuckMovingRight())
        ) {
        return true;
    }

    return false;
}

bool Player::isPucksStateDistancing() {
    return !isPucksStateIncoming();
}

void Player::runAI() {};
void Player::doIdling() {};
void Player::doDefending() {};
void Player::doDistancing() {};
void Player::doAttacking() {};
void Player::doPositionResetting() {};
void Player::changeAIStateAndStateFuncPtr(AppConsts::AI_STATES newAIState) { };
