#include "../../Header Files/Classes/AIEventsDispatcher.h"
#include "../../Header Files/Classes/Player.h"
#include "../../Header Files/Classes/player.h" // TODO - dlaczego tak ? sprawdzic to
#include "../../Header Files/Namespaces/PuckHelpers.h"
#include <memory>

using namespace AppConsts;
using namespace AppInitializers;
using namespace PuckHelpers;

void AIEventsDispatcher::idleEvent(std::shared_ptr<Player>& player) {
    switch (player->mAIState) {
    case AI_STATES::NONE:
    case AI_STATES::POSITION_RESET:
        player->changeAIStateAndStateFuncPtr(AI_STATES::IDLE);
        break;
    default:

        // I can also think about adding some kind of assert
        break;
    }
};
void AIEventsDispatcher::defendEvent(std::shared_ptr<Player>& player) {
    //std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast< std::shared_ptr<AIPlayer>::element_type >(player);

    switch (player->mAIState) {
    case AI_STATES::POSITION_RESET:
    case AI_STATES::IDLE:
    case AI_STATES::DISTANCING:
        player->changeAIStateAndStateFuncPtr(AI_STATES::DEFENSE);
        break;
    default:

        // I can also think about adding some kind of assert
        break;
    }
};
void AIEventsDispatcher::distanceEvent(std::shared_ptr<Player>& player) {
    //std::shared_ptr<AIPlayer> aiPlayer = std::dynamic_pointer_cast< std::shared_ptr<AIPlayer>::element_type >(player);

    switch (player->mAIState) {
    case AI_STATES::IDLE:
    case AI_STATES::POSITION_RESET:
    case AI_STATES::ATTACK:
        player->changeAIStateAndStateFuncPtr(AI_STATES::DISTANCING);
        break;
    default:

        // I can also think about adding some kind of assert
        break;
    }
};
void AIEventsDispatcher::attackEvent(std::shared_ptr<Player>& player) {
    switch (player->mAIState) {
    case AI_STATES::DEFENSE:
        player->changeAIStateAndStateFuncPtr(AI_STATES::ATTACK);
        break;
    default:
        // I can also think about adding some kind of assert

        break;
    }
};
void AIEventsDispatcher::positionResetEvent(std::shared_ptr<Player>& player) {
    switch (player->mAIState) {
    case AI_STATES::DEFENSE:
    case AI_STATES::DISTANCING:
    case AI_STATES::ATTACK:
        player->changeAIStateAndStateFuncPtr(AI_STATES::POSITION_RESET);
        break;
    default:

        // I can also think about adding some kind of assert
        break;
    }
};

void AIEventsDispatcher::defendOrDistanceAfterGameStarts(std::shared_ptr<Player>& player) {
    if (player->mSidePlayingOn == GAME_SETTINGS::SIDE_RIGHT) {
        IsPuckMovingRight() ? defendEvent(player) : distanceEvent(player);
        return;
    }

    IsPuckMovingLeft() ? defendEvent(player) : distanceEvent(player);
}

void AIEventsDispatcher::aiChangeStateToDistanceOrDefenseAfterBounce(std::shared_ptr<Player>& player) {

    if (player->isAI()) {
        auto aiState = player->getState();

        if (aiState == AI_STATES::ATTACK) {
            // It means that AI was attacking and it means that AI bounced the puck and now AI will distance outcomming puck.
            distanceEvent(player);
        }
        else if (aiState == AI_STATES::DISTANCING) {
            // It means that AI was distancing but oponent managed to bounce the puck back.
            defendEvent(player);
        }
    }
}

void AIEventsDispatcher::aiChangeStateToPositionResetAfterScoringOrLosingGoal(std::shared_ptr<Player>& player) {
    if (player->isAI()) {
        AI_STATES aiState = player->getState();

        // TODO - po debugowaniu pozbyc sie tego if'a ponizszego i powyzszej definicji zmiennej aiState bo pozniej beda nie potrzebne.
        if (aiState == AI_STATES::DEFENSE || aiState == AI_STATES::DISTANCING || aiState == AI_STATES::ATTACK) {
            // either AI scored a goal or lost a goal.
            positionResetEvent(player);
        }
    }
}

void AIEventsDispatcher::aiChangeStateToDefenseOrDistanceFromPositionResetOrIdle(std::shared_ptr<Player>& player) {
    if (player->isAI() && player->getState() == AI_STATES::POSITION_RESET || player->getState() == AI_STATES::IDLE) {
        if (player->isPucksStateIncoming()) {
            defendEvent(player);
            return;
        }

        distanceEvent(player);
    }
}
