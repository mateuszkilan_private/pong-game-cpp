#include "../../Header Files/Classes/HumanPlayer.h"

HumanPlayer::HumanPlayer(AppConsts::GAME_SETTINGS sidePlayingOn, AppConsts::SPEED_BOOST_STATES speedBoostState, std::shared_ptr<SpriteWithoutMask>& racketSprite, std::shared_ptr<SpriteWithoutMask>& speedBoostSprite) : Player(sidePlayingOn, speedBoostState, racketSprite, speedBoostSprite, true) {}
