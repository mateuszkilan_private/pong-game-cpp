#include "../../Header Files/Classes/SpriteWithoutMask.h"

SpriteWithoutMask::SpriteWithoutMask(HINSTANCE hAppInst, int imageID, const Vec2& p0, const Vec2& v0)
{
	mhAppInst = hAppInst;
	mhImage = LoadBitmap(hAppInst, MAKEINTRESOURCE(imageID));
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	mPosition = p0;
	mVelocity = v0;
	mUpperLeftCorner = Vec2(p0.x - int(0.5 * mImageBM.bmWidth), p0.y - int(0.5 * mImageBM.bmHeight));  // very often it is not updated
	mLowerRightCorner = Vec2(p0.x + int(0.5 * mImageBM.bmWidth), p0.y + int(0.5 * mImageBM.bmHeight)); // very often it is not updated
}

SpriteWithoutMask::~SpriteWithoutMask()
{
	DeleteObject(mhImage);
}

void SpriteWithoutMask::setPosition(Vec2 posVec) {
    mPosition = posVec;
    mUpperLeftCorner = Vec2(mPosition.x - int(0.5 * mImageBM.bmWidth), mPosition.y - int(0.5 * mImageBM.bmHeight));
    mLowerRightCorner = Vec2(mPosition.x + int(0.5 * mImageBM.bmWidth), mPosition.y + int(0.5 * mImageBM.bmHeight));
}

void SpriteWithoutMask::setPosition(float x, float y) {
    setPosition(Vec2(x, y));
}

void SpriteWithoutMask::setPositionsCoordinateY(float coordY) {
    mPosition.y = coordY;
    mUpperLeftCorner = Vec2(mPosition.x - int(0.5 * mImageBM.bmWidth), mPosition.y - int(0.5 * mImageBM.bmHeight));
    mLowerRightCorner = Vec2(mPosition.x + int(0.5 * mImageBM.bmWidth), mPosition.y + int(0.5 * mImageBM.bmHeight));
}

void SpriteWithoutMask::setPositionsCoordinateX(float coordX) {
    mPosition.x = coordX;
    mUpperLeftCorner = Vec2(mPosition.x - int(0.5 * mImageBM.bmWidth), mPosition.y - int(0.5 * mImageBM.bmHeight));
    mLowerRightCorner = Vec2(mPosition.x + int(0.5 * mImageBM.bmWidth), mPosition.y + int(0.5 * mImageBM.bmHeight));
}

Vec2 SpriteWithoutMask::getPosition() {
    return mPosition;
}

int SpriteWithoutMask::width()
{
	return mImageBM.bmWidth;
}

int SpriteWithoutMask::height()
{
	return mImageBM.bmHeight;
}

void SpriteWithoutMask::update(float dt)
{
	mPosition += mVelocity * dt;
}

SpriteWithoutMask* SpriteWithoutMask::draw(HDC hBackBufferDC, HDC hSpriteDC, DWORD drawMethod)
{
	int spritesWidth = width();
	int spritesHeight = height();

	int leftUpperCornerPosX = (int)mPosition.x - (spritesWidth / 2);
	int leftUpperCornerPosY = (int)mPosition.y - (spritesHeight / 2);

	HGDIOBJ oldObj = SelectObject(hSpriteDC, mhImage);
	BitBlt(hBackBufferDC, leftUpperCornerPosX, leftUpperCornerPosY, spritesWidth, spritesHeight, hSpriteDC, 0, 0, drawMethod);

	SelectObject(hSpriteDC, oldObj);

	return this;
}

SpriteWithoutMask* SpriteWithoutMask::draw() {
	using namespace AppInitializers;
	return draw(GBackBuffer->getDC(), GhSpriteDC, SRCCOPY);
}
