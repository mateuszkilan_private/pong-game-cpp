#include <Windows.h>
#include <mmsystem.h>
#include <time.h>
#include "../Header Files/Namespaces/GameController.h"

using namespace std;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR cmdLine, int showCmd) {
    using namespace AppInitializers;
	GhAppInst = hInstance;

	if (GameController::InitMainWnd(showCmd)) {
		MSG msg;
		SecureZeroMemory(&msg, sizeof(MSG));

        __int64 countsPerSecond = 0;
        bool perfExists = QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSecond) != 0;
        if (!perfExists) {
            ::MessageBoxW(nullptr, L"Performance timer does not exist!", L"Error", MB_OK);
        }
        GTimeScaleSeconds = 1.0 / (double)(countsPerSecond);

        QueryPerformanceCounter((LARGE_INTEGER*)&GTimeCount);
        GLastTimeInSeconds = (double)GTimeCount * GTimeScaleSeconds;

        srand((unsigned int)time(NULL));
		while (msg.message != WM_QUIT) {
			if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else {
				GameController::RunGameLoop();
			}
		}
		return (int)msg.wParam;
	}

	::MessageBox(nullptr, L"Window creation failed", L"Error", MB_OK);
	return NULL;
}
