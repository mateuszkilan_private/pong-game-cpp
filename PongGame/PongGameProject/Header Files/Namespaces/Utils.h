#pragma once

#include <string>
#include <vector>
#include "AppConsts.h"

enum LogSection {
    DEFEND_LOGS = 0,
    ATTACK_LOGS = 1,
    DISTANCE_LOGS = 2,
    NONE_LOGS = 3,
    POSITION_RESET_LOGS = 4,
    IDLE_LOGS = 5,
    CHANGE_STATE_LOGS = 6,
    ANALYSE_COLLISION_LOGS = 7,
    DEBUG_LOGS = 8
};

//#define LOGS_ARE_COMPILED

#ifdef LOGS_ARE_COMPILED
    #define COMPILE_LOG_STATEMENT(statement) \
        statement
    #define COMPILE_LOG(text, logsSection, ...) \
        LogToFile(text, logsSection, ##__VA_ARGS__)
#else
    #define COMPILE_LOG_STATEMENT(statement)
    #define COMPILE_LOG(text, logsSection, ...)
#endif

extern std::vector<LogSection> G_LOG_SECTIONS_TO_LOG;

extern void LogToFile(const std::string& text, LogSection logsSection, bool shouldBePrintedOnlyOnce = false, int printId = 0, const std::string filePath = "C:/Users/Mateusz/Desktop/Repositories C++/Projects/PongGame/ThePongGameLogger.txt");
extern std::string getStringifiedBounceArea(AppConsts::BOUNCE_AREA bounceArea);

namespace Test {
    template <typename T> void debug(T debugVariable, std::string description);
    void separator(std::string description);
    template <typename T> void eq(std::string testDescription, T testVariable, T valueToMatch);
    template <typename T> void notEq(std::string testDescription, T testVariable, T valueToMatch);
    void suite(std::string suiteDescription);
    void suiteEnd();
}
