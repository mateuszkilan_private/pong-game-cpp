#pragma once

#include "../Classes/SpriteWithoutMask.h"
#include "PuckHelpers.h"
#include "AppInitializers.h"
#include "AppConsts.h"
#include "cmath"

namespace GDISpritesMovement {
	extern void runPucksCollisionDetection(double dt);
    extern void runRacketsMovementDetection(double dt);
	extern AppConsts::COLLISION_SIDES _probablePucksCollisionSides();
	extern void _reversePucksVelVec(double& colOneT, double& colTwoT);
	extern void _calculatePucksCollision(AppConsts::COLLISION_SIDES& collision, double& dt);
    extern bool _initialCollisionCheckAndMoveOfThePuck(double dt);
}
