#pragma once

#include <string>
#include "../Classes/Vec2.h"

namespace AppConsts {
    extern const double PI;

    extern const float G_AI_EASY_DEFEND_LAG_IN_SECONDS;
    extern const float G_AI_MEDIUM_DEFEND_LAG_IN_SECONDS;
    extern const float G_AI_HARD_DEFEND_LAG_IN_SECONDS;

    extern const unsigned __int8 G_AI_EASY_PERCENTAGE_OF_NORMAL_RACKET_SPEED;
    extern const unsigned __int8 G_AI_MEDIUM_PERCENTAGE_OF_NORMAL_RACKET_SPEED;
    extern const unsigned __int8 G_AI_HARD_PERCENTAGE_OF_NORMAL_RACKET_SPEED;

	extern const int G_CLIENT_WIDTH;
	extern const int G_CLIENT_HEIGHT;
	extern const Vec2 G_CLIENT_CENTER;

	extern const std::wstring G_WINDOW_CAPTION;

    extern const float G_RACKETS_SLOW_SPEED;
    extern const float G_RACKETS_NORMAL_SPEED;
    extern const float G_RACKETS_FAST_SPEED;

    extern const Vec2 G_PUCKS_BASE_SPEED;
    extern const float G_PUCKS_SLOW_SPEED_MODIFIER;
    extern const float G_PUCKS_NORMAL_SPEED_MODIFIER;
    extern const float G_PUCKS_FAST_SPEED_MODIFIER;

    extern const float G_RACKETS_SPEED_BOOST_MODIFIER;

    extern const Vec2 G_SCORE_POS_GAME_AREA_SINGLE_DIGIT_LEFT_SIDE;
    extern const Vec2 G_SCORE_POS_GAME_AREA_MULTI_DIGITS_LEFT_SIDE;
    extern const Vec2 G_SCORE_POS_END_GAME_MENU_SINGLE_DIGIT_LEFT_SIDE;
    extern const Vec2 G_SCORE_POS_END_GAME_MENU_MULTI_DIGITS_LEFT_SIDE;
    extern const Vec2 G_SCORE_POS_PAUSE_MENU_SINGLE_DIGIT_LEFT_SIDE;
    extern const Vec2 G_SCORE_POS_PAUSE_MENU_MULTI_DIGITS_LEFT_SIDE;

    extern const Vec2 G_SCORE_POS_GAME_AREA_SINGLE_DIGIT_RIGHT_SIDE;
    extern const Vec2 G_SCORE_POS_GAME_AREA_MULTI_DIGITS_RIGHT_SIDE;
    extern const Vec2 G_SCORE_POS_END_GAME_MENU_SINGLE_DIGIT_RIGHT_SIDE;
    extern const Vec2 G_SCORE_POS_END_GAME_MENU_MULTI_DIGITS_RIGHT_SIDE;
    extern const Vec2 G_SCORE_POS_PAUSE_MENU_SINGLE_DIGIT_RIGHT_SIDE;
    extern const Vec2 G_SCORE_POS_PAUSE_MENU_MULTI_DIGITS_RIGHT_SIDE;

    extern const Vec2 G_MENU_ITEM_OPTIONS_AT_MAIN_MENU_POS;
    extern const Vec2 G_MENU_ITEM_OPTIONS_AT_END_GAME_POS;
    extern const Vec2 G_MENU_ITEM_OPTIONS_AT_PAUSE_POS;

    extern const Vec2 G_MENU_ITEM_EXIT_GAME_AT_MAIN_MENU_POS;
    extern const Vec2 G_MENU_ITEM_EXIT_GAME_AT_END_GAME_POS;
    extern const Vec2 G_MENU_ITEM_EXIT_GAME_AT_PAUSE_POS;

    extern const Vec2 G_MENU_ITEM_MAIN_MENU_AT_END_GAME_POS;
    extern const Vec2 G_MENU_ITEM_MAIN_MENU_AT_PAUSE_POS;

    extern const Vec2 G_COLON_END_GAME_POS;
    extern const Vec2 G_COLON_PAUSE_POS;

    extern const Vec2 G_RACKETS_LEFT_DEFAULT_POS;
    extern const Vec2 G_RACKETS_RIGHT_DEFAULT_POS;

    enum class RACKET_SPEED_STATES {
        MOVING = 0,
        STILL = 1
    };

    enum class AI_STATES {
        IDLE = 0,
        DEFENSE = 1,
        DISTANCING = 2,
        ATTACK = 3,
        POSITION_RESET = 4,
        NONE = 5 // NONE is only when game didn't start yet.
    };

	enum class GAME_PHASES {
		INITIAL_MENU = 0,
			NEW_GAME_MENU = 1,
				CHOOSE_AI_DIFFICULTY_MENU = 2,
				CHOOSE_SIDE_MENU = 3,
					HUMAN_P1_VS_AI_GAME = 4,
				HUMAN_P1_VS_HUMAN_P2_GAME = 5,
				AI_VS_AI_GAME = 6,
			OPTIONS_MENU = 7,
			CREDITS_MENU = 8,
		EXIT_GAME = 9,
        END_GAME_MENU = 10,
        PAUSE_MENU = 11,
        CONTINUE = 12,
		NONE = 13
	};

    enum class COLLISION_SIDES {
        NONE = 0,
        TOP = 1,
        RIGHT = 2,
        TOP_AND_RIGHT = 3,
        BOTTOM = 4,
        BOTTOM_AND_RIGHT = 6,
        LEFT = 7,
        TOP_AND_LEFT = 8,
        BOTTOM_AND_LEFT = 11
    };

	enum class GAME_SETTINGS {
		NONE = 0,
		DIFFICULTY_EASY = 1,
		DIFFICULTY_MEDIUM = 2,
		DIFFICULTY_HARD = 3,
		SIDE_LEFT = 4,
		SIDE_RIGHT = 5,
		SPEED_SLOW = 6,
		SPEED_NORMAL = 7,
		SPEED_FAST = 8
	};

    enum class SPEED_BOOST_STATES {
        AVAILABLE = 0,
        ON = 1,
        RECHARGING = 2
    };

    enum class BOUNCE_AREA {
        FROM_0_TO_10 = 0,
        FROM_10_TO_20 = 1,
        FROM_20_TO_30 = 2,
        FROM_30_TO_70 = 3,
        FROM_70_TO_80 = 4,
        FROM_80_TO_90 = 5,
        FROM_90_TO_100 = 6,
        NONE = 7
    };

    enum class ATTACK_DEDUCTION_DIRECTION {
        TOP = 0,
        BOTTOM = 1,
        TOP_AND_BOTTOM = 2
    };
}
