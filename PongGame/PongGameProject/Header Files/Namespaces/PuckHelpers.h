#pragma once

#include "../Classes/SpriteWithoutMask.h"
#include "../Classes/Player.h"
#include "../Classes/Vec2.h"
#include "AppConsts.h"
#include "AppInitializers.h"
#include "MainMenuOperations.h"
#include "Utils.h"
#include <cmath>


namespace PuckHelpers {
    extern const Vec2 GetEffectivePuckCollisionRange(Vec2 pucksPos = AppInitializers::GPuck->getPosition());
    extern const Vec2 GetPucksSettingSpeedVector();
    extern const Vec2 GetPucksSettingSpeedVectorForPauseMenu();
    extern bool StopPuckAfterGoal(float deltaTime);
    extern void AnalyseCollision(std::shared_ptr<Player>& collisionalPlayer, std::shared_ptr<Player>& otherPlayer, AppConsts::COLLISION_SIDES probableCollisionSide);
    extern void RunVelocityModifierAlgorithm(std::shared_ptr<Player>& collisionalPlayer);
    extern AppConsts::GAME_SETTINGS SelectStartingPucksDirection();
    extern void GenerateRandomPositionAndAngle(AppConsts::GAME_SETTINGS pucksDirection);
    extern void RunBounceAngleModifierAlgorithm(std::shared_ptr<Player>& collisionalPlayer, AppConsts::COLLISION_SIDES probableCollisionSide);
    extern const float GetYCoordinateOfPucksCollisionPointWithRacket(const Vec2& racketRange);
    extern void GetPredictionVec2OfNextCollision(Vec2& puckVel, Vec2& puckPos);
    extern const float CountVelocityVectorsLengthUsingPythagoras(const Vec2 velVec = AppInitializers::GPuck->mVelocity);
    extern const bool IsPuckMovingLeft();
    extern const bool IsPuckMovingRight();
}
