#pragma once

#include "AppConsts.h"
#include "../Classes/BackBuffer.h"
#include "../Classes/AIEventsDispatcher.h"
#include <vector>
#include <memory>


class MenuItemSpriteWithoutMask;
class SpriteWithoutMask;
class Player;
// since I'm forward declaring I can't use unique_ptr, I have to use shared_ptr instead
// explanation is available here http://stackoverflow.com/questions/6012157/is-stdunique-ptrt-required-to-know-the-full-definition-of-t

namespace AppInitializers {
	extern HWND GhMainWnd;
	extern HINSTANCE GhAppInst;
	extern HDC GhSpriteDC;
	extern HMENU GhMainMenu;

    extern int GSleepTimeInMs;

	extern short int GMouseCoordinateX;
	extern short int GMouseCoordinateY;

	extern BackBuffer* GBackBuffer;
    extern std::unique_ptr<AIEventsDispatcher> GAIEventsDispatcher;
    extern AppConsts::COLLISION_SIDES GRecentPucksCollisionSide;

	// VIEW BITMAPS BEGIN
	// Common bitmaps
	extern SpriteWithoutMask* GBlackBackground;
	extern SpriteWithoutMask* GMenuCopyrights;
	extern SpriteWithoutMask* GMenuFrame;
	extern SpriteWithoutMask* GMenuThePongGameLogo;
	extern MenuItemSpriteWithoutMask* GMenuItemBack;
    extern std::shared_ptr<SpriteWithoutMask> GF1HowToPlayHint;

	// Main Menu View
	extern MenuItemSpriteWithoutMask* GMenuItemNewGame;
	extern MenuItemSpriteWithoutMask* GMenuItemOptionsDynPos;
	extern MenuItemSpriteWithoutMask* GMenuItemCredits;
	extern MenuItemSpriteWithoutMask* GMenuItemExitGameDynPos;

	// New Game View
	extern MenuItemSpriteWithoutMask* GMenuItemAIvsAI;
	extern MenuItemSpriteWithoutMask* GMenuItemP1vsAI;
	extern MenuItemSpriteWithoutMask* GMenuItemP1vsP2;

	// AI Difficulty View
	extern MenuItemSpriteWithoutMask* GMenuItemEasy;
	extern MenuItemSpriteWithoutMask* GMenuItemMedium;
	extern MenuItemSpriteWithoutMask* GMenuItemHard;
	extern SpriteWithoutMask* GMenuDifficulty;

	// Choose Side View
	extern MenuItemSpriteWithoutMask* GMenuItemLeft;
	extern MenuItemSpriteWithoutMask* GMenuItemRight;
	extern SpriteWithoutMask* GMenuChooseSide;

	// Options View
	extern MenuItemSpriteWithoutMask* GMenuItemFast;
	extern MenuItemSpriteWithoutMask* GMenuItemNormal;
	extern MenuItemSpriteWithoutMask* GMenuItemSlow;
	extern SpriteWithoutMask* GMenuGameSpeed;

	// Credits View
	extern MenuItemSpriteWithoutMask* GMenuItemCreditsBack;
	extern SpriteWithoutMask* GMenuCreditsText;

	extern std::shared_ptr<SpriteWithoutMask> GPlayerOneRacket;
	extern std::shared_ptr<SpriteWithoutMask> GPlayerTwoRacket;

	extern std::shared_ptr<SpriteWithoutMask> GPlayerOneSpeedBoost;
	extern std::shared_ptr<SpriteWithoutMask> GPlayerTwoSpeedBoost;

	extern std::shared_ptr<SpriteWithoutMask> GHashLine;
	extern std::shared_ptr<SpriteWithoutMask> GPuck;

	extern std::shared_ptr<SpriteWithoutMask> GMenuHowToPlay;

    // EndGame View
    extern std::shared_ptr<SpriteWithoutMask> GAIWon;
    extern std::shared_ptr<SpriteWithoutMask> GP1Won;
    extern std::shared_ptr<SpriteWithoutMask> GP2Won;
    extern std::shared_ptr<SpriteWithoutMask> GColonDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GFinalScore;
    extern MenuItemSpriteWithoutMask* GMenuItemMainMenuDynPos;
    extern MenuItemSpriteWithoutMask* GMenuItemPlayAgainDynGamePhase;

    // Pause View
    extern std::shared_ptr<SpriteWithoutMask> GCurrentScore;
    extern MenuItemSpriteWithoutMask* GMenuItemContinue;
    extern MenuItemSpriteWithoutMask* GMenuItemRestartDynGamePhase;

    // Score numbers
    extern std::shared_ptr<SpriteWithoutMask> GScoreZeroDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreOneDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreTwoDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreThreeDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreFourDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreFiveDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreSixDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreSevenDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreEightDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreNineDynPos;
    extern std::shared_ptr<SpriteWithoutMask> GScoreTenDynPos;
	// VIEW BITMAPS END

    extern bool GShowScore;
    extern float GShowScoreDeltaTime;

    extern bool GStopPuckAfterGoal;
    extern float GStopPuckAfterGoalDeltaTime;

    extern float GRacketsChosenSpeedForCoordinateY;

    extern AppConsts::RACKET_SPEED_STATES GPlayerOneRacketSpeedState;
    extern AppConsts::RACKET_SPEED_STATES GPlayerTwoRacketSpeedState;

	extern std::shared_ptr<Player> GPlayerOne;
	extern std::shared_ptr<Player> GPlayerTwo;

    extern double GTimeScaleSeconds;
    extern __int64 GTimeCount;
    extern double GLastTimeInSeconds;
    extern double GCurrentTimeInSeconds;
    extern double GDeltaTimeInSeconds;

	extern bool GGameWasNotActive;

    extern int GLeftBorderOfGameArea;
    extern int GRightBorderOfGameArea;
    extern int GTopBorderOfGameArea;
    extern int GBottomBorderOfGameArea;

    extern int GHalfOfPucksWidth;
    extern int GHalfOfPucksHeight;

    extern int GHalfOfRacketsHeight;

    extern float GPucksSpeedBounceModifier;

	extern AppConsts::GAME_PHASES GCurrentGamePhase;
	extern AppConsts::GAME_SETTINGS GSettingDifficulty;
	extern AppConsts::GAME_SETTINGS GSettingSide;
	extern AppConsts::GAME_SETTINGS GSettingSpeed;
    extern AppConsts::GAME_SETTINGS GPreviousSettingSpeed;
	//extern AppConsts::MENU_ITEMS GActiveMenuItem;
    extern bool GMenuHowToPlayIsVisible;
    extern bool GPauseMenuIsVisible;
    extern AppConsts::GAME_PHASES GPreviousGamePlayPhaseInfoForPauseMenu;

	extern MenuItemSpriteWithoutMask* GActiveMenuItem;
	extern MenuItemSpriteWithoutMask* GActiveSettingMenuItem;
	extern std::vector<MenuItemSpriteWithoutMask*> GCurrentMenuItems;
}
