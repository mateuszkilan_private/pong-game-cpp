#pragma once

#include <Windows.h>
#include "AppConsts.h"
#include "AppInitializers.h"
#include "../Classes/BackBuffer.h"
#include "../Classes/SpriteWithoutMask.h"
#include "../Classes/MenuItemSpriteWithoutMask.h"
#include "../Classes/Player.h"
#include "../Classes/AIPlayer.h"
#include "../Classes/HumanPlayer.h"
#include "../Classes/AIEventsDispatcher.h"
#include "../resource2.h"
#include "PuckHelpers.h"
#include "GameController.h"


namespace MainMenuOperations {
	extern void WindowCreatedInitializer(HWND hWnd);
	extern void DrawInitialMenu();
    extern void DrawCommonMenuParts(bool drawMenuFrame = true);
	extern void ActivateAndDeactivateMenuItem(short int mouseCoordinateX, short int mouseCoordinateY);
	extern bool MouseCursorIsOverMenuItem(MenuItemSpriteWithoutMask* menuItem, short int mouseCoordinateX, short int mouseCoordinateY);
    extern void GenerateMenuViewBasedOnGCurrentGamePhase();
	extern void ChangeMenuOrRunGameBasedOnGActiveMenuItem();
	extern void ChangeDifficultySetting();
	extern void ChangeSideSetting();
	extern void ChangeAndVisuallyDeactivateOldSpeedSetting();
	extern void VisuallyActivateSpeedSetting();
	extern MenuItemSpriteWithoutMask* GetPointerToCurrentSpeedSettingSprite();
    extern const float GetRacketsSettingSpeedForCoordinateY();

    extern void DrawPauseMenu();
    extern void DrawEndGameMenu();
    extern void DrawHowToPlayMenuAndPresent();
    extern void DrawNewGameMenu();
    extern void DrawChooseAIDifficultyMenu();
    extern void DrawChooseSideMenu();
    extern void DrawOptionsMenu(AppConsts::GAME_PHASES previousGamePhase);
    extern void DrawCreditsMenu();
}
