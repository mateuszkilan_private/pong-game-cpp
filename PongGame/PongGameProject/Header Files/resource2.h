//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by C:\Users\Mateusz\Desktop\Repositories C++\Projects\PongGame\PongGame\PongGameProject\Resource Files\PongGame.rc
//
#define IDR_PONG_MENU                   1
#define BLACK_BACKGROUND_BM             8
#define MENU_FRAME_BM                   105
#define MENU_COPYRIGHTS_BM              106
#define MENU_ITEM_BACK_BM               108
#define MENU_ITEM_BACK_ACTIVE_BM        109
#define MENU_ITEM_NEW_GAME_BM           110
#define MENU_ITEM_NEW_GAME_ACTIVE_BM    111
#define MENU_ITEM_OPTIONS_BM            112
#define MENU_ITEM_OPTIONS_ACTIVE_BM     113
#define MENU_ITEM_CREDITS_BM            114
#define MENU_ITEM_CREDITS_ACTIVE_BM     115
#define MENU_ITEM_EXIT_GAME_BM          116
#define MENU_ITEM_EXIT_GAME_ACTIVE_BM   117
#define MENU_ITEM_AI_VS_AI_BM           118
#define MENU_ITEM_AI_VS_AI_ACTIVE_BM    119
#define MENU_ITEM_P1_VS_AI_BM           120
#define MENU_ITEM_P1_VS_AI_ACTIVE_BM    121
#define MENU_ITEM_P1_VS_P2_BM           122
#define MENU_ITEM_P1_VS_P2_ACTIVE_BM    123
#define MENU_DIFFICULTY_BM              124
#define MENU_ITEM_EASY_BM               125
#define MENU_ITEM_EASY_ACTIVE_BM        126
#define MENU_ITEM_MEDIUM_BM             127
#define MENU_ITEM_MEDIUM_ACTIVE_BM      128
#define MENU_ITEM_HARD_BM               129
#define MENU_ITEM_HARD_ACTIVE_BM        130
#define MENU_CHOOSE_SIDE_BM             131
#define MENU_ITEM_LEFT_BM               132
#define MENU_ITEM_LEFT_ACTIVE_BM        133
#define MENU_ITEM_RIGHT_BM              134
#define MENU_ITEM_RIGHT_ACTIVE_BM       135
#define MENU_GAME_SPEED_BM              136
#define MENU_ITEM_FAST_BM               137
#define MENU_ITEM_FAST_ACTIVE_BM        138
#define MENU_ITEM_NORMAL_BM             139
#define MENU_ITEM_NORMAL_ACTIVE_BM      140
#define MENU_ITEM_SLOW_BM               141
#define MENU_ITEM_SLOW_ACTIVE_BM        142
#define IDB_BITMAP1                     143
#define MENU_CREDITS_TEXT_BM            143
#define RACKET_BM                       144
#define HASH_LINE_BM                    145
#define BALL_BM                         146
#define IDB_BITMAP5                     147
#define SPEED_BOOST_PLUS_SIGN_BM        147
#define IDB_BITMAP2                     148
#define MENU_HOW_TO_PLAY_BM             148
#define MNEU_HOW_TO_PLAY_BM             149
#define F1_HOW_TO_PLAY_HINT             150
#define SCORE_EIGHT_BM                  151
#define SCORE_FIVE_BM                   152
#define SCORE_FOUR_BM                   153
#define SCORE_NINE_BM                   154
#define SCORE_ONE_BM                    155
#define SCORE_SEVEN_BM                  156
#define SCORE_SIX_BM                    157
#define SCORE_TEN_BM                    158
#define SCORE_THREE_BM                  159
#define SCORE_TWO_BM                    160
#define SCORE_ZERO                      161
#define SCORE_ZERO_BM                   161
#define AI_WON_BM                       162
#define COLON_BM                        163
#define FINAL_SCORE_BM                  164
#define MENU_ITEM_MAIN_MENU_BM          165
#define MENU_ITEM_MAIN_MENU_ACTIVE_BM   166
#define IDB_BITMAP9                     167
#define MENU_ITEM_PLAY_AGAIN_BM         167
#define MENU_ITEM_PLAY_AGAIN_ACTIVE_BM  168
#define P1_WON_BM                       169
#define P2_WON_BM                       170
#define CURRENT_SCORE_BM                171
#define MENU_ITEM_CONTINUE_BM           172
#define MENU_ITEM_CONTINUE_ACTIVE_BM    173
#define MENU_ITEM_RESTART_BM            174
#define IDB_BITMAP8                     175
#define MENU_ITEM_RESTART_ACTIVE_BM     175
#define ID_FILE_EXIT                    40001
#define ID_Menu                         40002
#define ID_VIDEOSETTINGS_PERFORMANCEFOCUS 40003
#define ID_LESS_FPS_LESS_CPU_USAGE_MENU 40004
#define ID_MORE_FPS_MORE_CPU_USAGE_MENU 40005
#define MENU_THE_PONG_GAME_LOGO_BM      65535

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        176
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
