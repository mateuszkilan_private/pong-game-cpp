#pragma once
#include "Player.h"

class HumanPlayer : public Player {
public:
	HumanPlayer(AppConsts::GAME_SETTINGS sidePlayingOn, AppConsts::SPEED_BOOST_STATES speedBoostState, std::shared_ptr<SpriteWithoutMask>& racketSprite, std::shared_ptr<SpriteWithoutMask>& speedBoostSprite);
};
