#pragma once
#include "Player.h"
#include <tuple>


using BounceAreaAndAbsRangeTuple = std::tuple<AppConsts::BOUNCE_AREA, float, float>;

class AIPlayer : public Player {
friend class AIEventsDispatcher;
friend class AIPlayerAccessor; // for implementation inside test environment to access private members of this class
public:
	AIPlayer(AppConsts::GAME_SETTINGS sidePlayingOn, AppConsts::SPEED_BOOST_STATES speedBoostState, std::shared_ptr<SpriteWithoutMask>& racketSprite, std::shared_ptr<SpriteWithoutMask>& speedBoostSprite, AppConsts::GAME_SETTINGS difficultyLevel);
    void runAI();
private:
    enum {
        BOUNCE_AREA_GETTER = 0,
        REL_MINIMUM_PX_CHANGE_GETTER = 1,
        REL_MAXIMUM_PX_CHANGE_GETTER = 2
    };

    // MEMBER METHODS
    void doIdling();
    void doDefending();
    void doDistancing();
    void doAttacking();
    void doPositionResetting();

    void defendingCleanUp();
    void distancingCleanUp();
    void attackingCleanUp();
    void positionResettingCleanUp();
    void idlingCleanUp();

    void analyseOrChangeSpeedBoostStateEasy(float timeSinceLastFrame);
    void analyseOrChangeSpeedBoostStateMedium(float timeSinceLastFrame);
    void analyseOrChangeSpeedBoostStateHard(float timeSinceLastFrame);
    void analyseOrChangeSpeedBoostState(float timeSinceLastFrame);
    float getRacketsYCoordSpeedEasy();
    float getRacketsYCoordSpeedMedium();
    float getRacketsYCoordSpeedHard();
    float getRacketYCoordSpeedBasedOnSpeedBoost();
    void changeAIStateAndStateFuncPtr(AppConsts::AI_STATES newAIState);
    auto calculateDistanceBetweenTwoVec2(Vec2 vecOne, Vec2 vecTwo);
    Vec2 calculateLinearFunctionEquation(Vec2 velAbstractForPuck, Vec2 posAbstractForPuck);
    void calculateCollisionPosAndTimeUntilPuckCollidesWithGoalLine();
    AppConsts::AI_STATES calculateRacketsBouncePixelAndTimeLeftToPlanAttackAndGetStateChangeAndSetFakeDefenseVel();

    void runIdlingSetup();
    void runDefendingSetup();
    void runDistancingSetup();
    void runDistancingSetupEasy();
    void runDistancingSetupMediumAndHard();
    void runAttackSetup();
    void runPositionResettingSetup();

    void topChooseBounceArea();
    void bottomChooseBounceArea();
    void bottomCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity();
    void topCalculateAmountOfPixelsWhichRacketNeedsToCoverAndSetVelocity();

    void determinePossibleBounceAreasTop(float maxDistanceTop);
    void determinePossibleBounceAreasBottom(float maxDistanceBottom);
    void setMaxRelativePucksCollisionYCoordWithRacketAfterMaxDistanceCoverableByRacket(AppConsts::ATTACK_DEDUCTION_DIRECTION direction);

    float getYCoordOfPucksCollPntWithRacketForAI(const Vec2& racketsRange, const Vec2& pucksRange);

    // MEMBER VARIABLES
    bool mDefendRecallBasedOnTurnedSpeedBoost;
    void (AIPlayer::* mRunDistancingSetupPtr)();
    float mReactionLagInSeconds;
    float (AIPlayer::* mGetRacketsYCoordSpeedPtr)();
    float mRacketsYCoordShouldBeEqual;
    float mDistanceForRacketToCoverBeforeColWithPuck;
    Vec2 mMaxDistanceCoverableByRacketTopAndBottom;
    float mAmountOfDistanceWhichRacketNeedsToCover;
    Vec2 mRacketsRangeAfterMaxDistanceCoverableByRacketTop;
    Vec2 mRacketsRangeAfterMaxDistanceCoverableByRacketBottom;
    Vec2 mRacketsRangeAtInitialCollWithPuck;
    Vec2 mMaxRelativePucksEffectiveCollYCoordWithRacketAfterMaxDistanceCoverableByRacketTopAndBottom;
    short int mPixelsLeftInLastContactBounceAreaTop;
    short int mPixelsLeftInLastContactBounceAreaBottom;
    AppConsts::BOUNCE_AREA mLastContactBounceAreaTop;
    AppConsts::BOUNCE_AREA mLastContactBounceAreaBottom;
    Vec2 mGoalLineEffectivePucksCollRange;
    std::vector<BounceAreaAndAbsRangeTuple> mTrickShotAttackAreasTopTupleVector;
    std::vector<BounceAreaAndAbsRangeTuple> mTrickShotAttackAreasBottomTupleVector;
    std::vector<BounceAreaAndAbsRangeTuple> mNormalShotAttackAreasTopTupleVector;
    std::vector<BounceAreaAndAbsRangeTuple> mNormalShotAttackAreasBottomTupleVector;

    bool mIdlingSetupDone;
    bool mPositionResettingSetupDone;
    bool mDistancingSetupDone;
    bool mDefenseSetupDone;
    bool mAttackSetupDone;
public:
    BounceAreaAndAbsRangeTuple mChosenBounceAreaAndAbsRangeTuple;
    float mTimerForReactionLagInSeconds;
    int mRacketsRelativePointBouncePlaceDuringFirstRecognition;
    float mTimeLeftToPlanAttack;
    Vec2 mGoalLineCollPos;
    float mTimeUntilCollisionWithAIGoalLine;
    AppConsts::GAME_SETTINGS mDifficultyLevel;
    void (AIPlayer::* mAnalyseOrChangeSpeedBoostStatePtr)(float timeSinceLastFrame);
    void (AIPlayer::* mAIStateFuncPtr)();
};
