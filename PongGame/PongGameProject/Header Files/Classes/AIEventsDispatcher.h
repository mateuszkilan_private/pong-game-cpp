#pragma once

#include <memory>

class Player;

class AIEventsDispatcher {
public:
    void idleEvent(std::shared_ptr<Player>& player);
    void defendEvent(std::shared_ptr<Player>& player);
    void distanceEvent(std::shared_ptr<Player>& player);
    void attackEvent(std::shared_ptr<Player>& player);
    void positionResetEvent(std::shared_ptr<Player>& player);

    void defendOrDistanceAfterGameStarts(std::shared_ptr<Player>& player);
    void aiChangeStateToDistanceOrDefenseAfterBounce(std::shared_ptr<Player>& player);
    void aiChangeStateToPositionResetAfterScoringOrLosingGoal(std::shared_ptr<Player>& player);
    void aiChangeStateToDefenseOrDistanceFromPositionResetOrIdle(std::shared_ptr<Player>& player);
};
