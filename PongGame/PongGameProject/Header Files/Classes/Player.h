#pragma once
#include "../Namespaces/AppConsts.h"
#include "../Namespaces/AppInitializers.h"
#include "SpriteWithoutMask.h"


class Player {
friend class AIEventsDispatcher;
public:
    Player(AppConsts::GAME_SETTINGS sidePlayingOn, AppConsts::SPEED_BOOST_STATES speedBoostState, std::shared_ptr<SpriteWithoutMask>& racketSprite, std::shared_ptr<SpriteWithoutMask>& speedBoostSprite, bool isHumanPlayer);

    AppConsts::GAME_SETTINGS mSidePlayingOn;
    AppConsts::SPEED_BOOST_STATES mSpeedBoostState;
    bool mIsHumanPlayer;
    std::shared_ptr<SpriteWithoutMask> mRacketSprite;
	std::shared_ptr<SpriteWithoutMask> mSpeedBoostSprite;
    float mSpeedBoostDeltaTime;
    int mScore;

    bool isHuman();
    bool isAI();
    bool isRacketMoving();
    const Vec2 getRacketsRange();
    const AppConsts::BOUNCE_AREA deduceBounceArea(const float yCoordinate);
    virtual void analyseOrChangeSpeedBoostState(float deltaTime);
    void incrementScore();
    bool hasWonGame();
    std::shared_ptr<SpriteWithoutMask>& getScoreForGameArea();
    std::shared_ptr<SpriteWithoutMask>& getScoreForEndGameMenu();
    std::shared_ptr<SpriteWithoutMask>& getScoreForPauseMenu();
    AppConsts::AI_STATES getState();
    bool isPucksStateIncoming();
    bool isPucksStateDistancing();

    // AIPlayer methods and members
    virtual void runAI();

protected:
    AppConsts::AI_STATES mAIState;
private:
    std::shared_ptr<SpriteWithoutMask>& _getScoreSprite();

    // AIPlayer methods and members
    virtual void doIdling();
    virtual void doDefending();
    virtual void doDistancing();
    virtual void doAttacking();
    virtual void doPositionResetting();
    virtual void changeAIStateAndStateFuncPtr(AppConsts::AI_STATES newAIState);
};
